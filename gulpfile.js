const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS = require('gulp-clean-css');
const del = require('del');
const gulpif = require('gulp-if');
const concat = require('gulp-concat');
const gcmq = require('gulp-group-css-media-queries');

const defaultTemplate = 'local/templates/.default/';
const mainTemplate = 'local/templates/kstart/';
const mobTemplate = 'local/templates/m_kstart/';

const path = {
	src: {
		sass: 'source/styles',
	},
	build: {
        js: 'build/js',
        css: 'build/css'
	},
	watch: {
		styles: 'source/styles/**/*.scss',
		scripts: 'source/scripts/**/*.js',
	}
};

const jsFiles = [
	defaultTemplate + 'source/scripts/jquery-3.4.1.min.js',
	defaultTemplate + 'source/libs/swiper/swiper.min.js',
	//defaultTemplate + 'source/scripts/modernizr-custom.js',
	defaultTemplate + 'source/libs/ui-slider/jquery-slider-ui.min.js',
	defaultTemplate + 'source/scripts/jquery.ui.touch-punch.min.js',
	defaultTemplate + 'source/scripts/jquery.inputmask.js',
	defaultTemplate + 'source/libs/fancybox/fancybox.js',
	mainTemplate + 'source/scripts/script.js'
];

const mobJsFiles = [
	defaultTemplate + 'source/scripts/jquery-3.4.1.min.js',
	defaultTemplate + 'source/libs/swiper/swiper.min.js',
	//defaultemplate + 'source/scripts/modernizr-custom.js',
	defaultTemplate + 'source/libs/ui-slider/jquery-slider-ui.min.js',
	defaultTemplate + 'source/scripts/jquery.ui.touch-punch.min.js',
	defaultTemplate + 'source/scripts/lazyestload.js',
	defaultTemplate + 'source/scripts/jquery.inputmask.js',
	mobTemplate + 'source/scripts/script.js',
];

const isDev = (process.argv.indexOf('--dev') !== -1);
const isProd = !isDev;

function styles() {
	return gulp.src(mainTemplate + path.src.sass + '/main.scss')
			.pipe(gulpif(isDev, sourcemaps.init()))
			.pipe(sass())
			.pipe(rename('main.min.css'))
			.pipe(autoprefixer({
		            browsers: ['> 0.1%'],
		            cascade: false
		        }))
			.pipe(gulpif(isProd, gcmq()))
			.pipe(cleanCSS({
			   		level: 2
			   }))
			.pipe(gulpif(isDev, sourcemaps.write()))
			.pipe(gulp.dest(mainTemplate + path.build.css))
			.pipe(browserSync.stream());
}

function scripts() {
	return gulp.src(jsFiles)
			.pipe(gulpif(isDev, sourcemaps.init()))
			.pipe(concat('script.min.js'))
			.pipe(uglify({
				toplevel: true
			}))
			.pipe(gulpif(isDev, sourcemaps.write()))
			.pipe(gulp.dest(mainTemplate + path.build.js))
			.pipe(browserSync.stream());
}

function mobStyles() {
	return gulp.src(mobTemplate + path.src.sass + '/main.scss')
			.pipe(gulpif(isDev, sourcemaps.init()))
			.pipe(sass())
			.pipe(rename('main.min.css'))
			.pipe(autoprefixer({
		            browsers: ['> 0.1%'],
		            cascade: false
		        }))
			.pipe(gulpif(isProd, gcmq()))
			.pipe(cleanCSS({
			   		level: 2
			   }))
			.pipe(gulpif(isDev, sourcemaps.write()))
			.pipe(gulp.dest(mobTemplate + path.build.css))
			.pipe(browserSync.stream());
}

function mobScripts() {
	return gulp.src(mobJsFiles)
			.pipe(gulpif(isDev, sourcemaps.init()))
			.pipe(concat('script.min.js'))
			.pipe(uglify({
				toplevel: true
			}))
			.pipe(gulpif(isDev, sourcemaps.write()))
			.pipe(gulp.dest(mobTemplate + path.build.js))
			.pipe(browserSync.stream());
}

function watch() {
   browserSync.init({
	proxy: "ex.ex",
	/*server: {
		baseDir: "./"
	}*/
  });

  gulp.watch(mainTemplate + path.watch.styles, styles)
  gulp.watch(mainTemplate + path.watch.scripts, scripts)
  gulp.watch("./*.php").on('change', browserSync.reload);

  gulp.watch(mobTemplate + path.watch.styles, mobStyles)
  gulp.watch(mobTemplate + path.watch.scripts, mobScripts)
  gulp.watch("./mobile/*.php").on('change', browserSync.reload);

  gulp.watch("./catalog/**/*.php").on('change', browserSync.reload);
  gulp.watch("./mobile/catalog/**/*.php").on('change', browserSync.reload);

  gulp.watch("./news/**/*.php").on('change', browserSync.reload);
  gulp.watch("./mobile/news/**/*.php").on('change', browserSync.reload);

  gulp.watch("./company/**/*.php").on('change', browserSync.reload);
  gulp.watch("./mobile/company/**/*.php").on('change', browserSync.reload);

  gulp.watch("./portfolio/**/*.php").on('change', browserSync.reload);
  gulp.watch("./mobile/portfolio/**/*.php").on('change', browserSync.reload);

  gulp.watch("./templates/kstart/*.php").on('change', browserSync.reload);
}

function clean() {
   return del([mainTemplate + 'build/*', mobTemplate + 'mobile/build/*']);
}

gulp.task('styles', styles);
gulp.task('scripts', scripts);

gulp.task('del', clean);
gulp.task('build', gulp.series(clean, gulp.parallel(styles,scripts,mobStyles,mobScripts)));

gulp.task('watch', gulp.series('build', watch));
