<? include 'paths.php'; ?>
<? include MAIN_TEMPLATE.'header.php'; ?>

		<section class="benefits-block container">
			<div class="benefits">
				<div class="benefits__item benefit benefit--one">
					<svg class="benefit__img">
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#benefit_01"></use>
		            </svg>
		            <p class="benefit__desc">
		            	Товар от производителя
		            </p>
				</div>
				<div class="benefits__item benefit benefit--two">
					<svg class="benefit__img">
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#benefit_02"></use>
		            </svg>
		            <p class="benefit__desc">
		            	Товар от производителя
		            </p>
				</div>
				<div class="benefits__item benefit benefit--tree">
					<svg class="benefit__img">
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#benefit_03"></use>
		            </svg>
		            <p class="benefit__desc">
		            	Товар от производителя
		            </p>
				</div>
				<div class="benefits__item benefit benefit--four">
					<svg class="benefit__img">
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#benefit_04"></use>
		            </svg>
		            <p class="benefit__desc">
		            	Товар от производителя
		            </p>
				</div>
			</div>
		</section>

		<section class="recommended-block">
			<div class="container">
				<div class="tabs-block">
					<div class="swiper-buttons">
	    				<div class="prev-product prev-slide">
	    					<i class="fa fa-angle-left" aria-hidden="true"></i>
	    				</div>
						<div class="next-product next-slide">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</div>
					</div>
					<ul class="catalog-tabs">
						<li class="catalog-tab active" data-code="discount">
							<span>Акции</span>
						</li>
						<li class="catalog-tab" data-code="saleleader">
							<span>Хит продаж</span>
						</li>
						<li class="catalog-tab" data-code="new">
							<span>Новинки</span>
						</li>
					</ul>
				</div>
			</div>
			<div class="tabs_content">
				<div class="container">
					<div class="catalog-block swiper-container" data-target="discount">
						<div class="swiper-wrapper">

							<div class="catalog-item swiper-slide catalog-item--absent">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_01.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный уголок для дома «Kind-Start»</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 149 грн.
										</span>
										<span class="catalog-item__discount">
										</span>
										<span class="catalog-item__old-price">
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--discount">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_02.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_02.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный уголок «Эверест-2»</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 969 грн.
										</span>
										<span class="catalog-item__discount">
											-10%
										</span>
										<span class="catalog-item__old-price">
											3 869 грн.
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--absent">
								<div class="catalog-item-wrapper">
									<div class="catalog-item__stickers item-stickers">
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_03.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_03.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детская площадка Babyland-12</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											32 924 грн.
										</span>
										<span class="catalog-item__discount">

										</span>
										<span class="catalog-item__old-price">

										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<span>В корзину</span>
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide">
								<div class="catalog-item-wrapper">
									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_04.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_04.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Спортивный комплекс для дома Акварелька Plus 1-1</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 149 грн.
										</span>
										<span class="catalog-item__discount">
										</span>
										<span class="catalog-item__old-price">
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--discount">
								<div class="catalog-item-wrapper">
									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_05.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_05.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный комплекс BambinoWood Color Plus 1-1</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											4 480 грн.
										</span>
										<span class="catalog-item__discount">
											-20%
										</span>
										<span class="catalog-item__old-price">
											5 600 грн.
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--absent">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_01.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный уголок для дома «Kind-Start»</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 149 грн.
										</span>
										<span class="catalog-item__discount">
										</span>
										<span class="catalog-item__old-price">
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--discount">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_02.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_02.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный уголок «Эверест-2»</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 969 грн.
										</span>
										<span class="catalog-item__discount">
											-10%
										</span>
										<span class="catalog-item__old-price">
											3 869 грн.
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--absent">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_03.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_03.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детская площадка Babyland-12</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											32 924 грн.
										</span>
										<span class="catalog-item__discount">

										</span>
										<span class="catalog-item__old-price">

										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<span>В корзину</span>
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_04.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_04.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Спортивный комплекс для дома Акварелька Plus 1-1</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 149 грн.
										</span>
										<span class="catalog-item__discount">
										</span>
										<span class="catalog-item__old-price">
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--discount">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_05.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_05.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный комплекс BambinoWood Color Plus 1-1</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											4 480 грн.
										</span>
										<span class="catalog-item__discount">
											-20%
										</span>
										<span class="catalog-item__old-price">
											5 600 грн.
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-block swiper-container" data-target="saleleader">
						<div class="swiper-wrapper">

							<div class="catalog-item swiper-slide catalog-item--absent">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_01.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный уголок для дома «Kind-Start»</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 149 грн.
										</span>
										<span class="catalog-item__discount">
										</span>
										<span class="catalog-item__old-price">
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--discount">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_05.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_05.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный комплекс BambinoWood Color Plus 1-1</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											4 480 грн.
										</span>
										<span class="catalog-item__discount">
											-20%
										</span>
										<span class="catalog-item__old-price">
											5 600 грн.
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_04.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_04.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Спортивный комплекс для дома Акварелька Plus 1-1</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 149 грн.
										</span>
										<span class="catalog-item__discount">
										</span>
										<span class="catalog-item__old-price">
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--absent">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_03.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_03.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детская площадка Babyland-12</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											32 924 грн.
										</span>
										<span class="catalog-item__discount">

										</span>
										<span class="catalog-item__old-price">

										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<span>В корзину</span>
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-block swiper-container" data-target="new">
						<div class="swiper-wrapper">

							<div class="catalog-item swiper-slide">
								<div class="catalog-item-wrapper">
									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_04.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_04.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Спортивный комплекс для дома Акварелька Plus 1-1</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 149 грн.
										</span>
										<span class="catalog-item__discount">
										</span>
										<span class="catalog-item__old-price">
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--discount">
								<div class="catalog-item-wrapper">
									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_05.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_05.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный комплекс BambinoWood Color Plus 1-1</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											4 480 грн.
										</span>
										<span class="catalog-item__discount">
											-20%
										</span>
										<span class="catalog-item__old-price">
											5 600 грн.
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--absent">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_01.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный уголок для дома «Kind-Start»</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 149 грн.
										</span>
										<span class="catalog-item__discount">
										</span>
										<span class="catalog-item__old-price">
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="catalog-item swiper-slide catalog-item--discount">
								<div class="catalog-item-wrapper">

									<div class="catalog-item__stickers item-stickers">
										<!-- <div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--hit">
												Хит
											</span>
										</div>
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--new">
												Новинка
											</span>
										</div> -->
										<div class="sticker-wrapper">
											<span class="item-stickers__elem item-stickers__elem--action">
												Акция
											</span>
										</div>
									</div>

									<div class="catalog-item__img">
										<a href="<?=SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_02.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_02.jpg">
											</picture>
										</a>
									</div>
									<div class="catalog-item__title">
										<a href="<?=SITE_DIR?>catalog/element">Детский спортивный уголок «Эверест-2»</a>
									</div>
									<div class="catalog-item__available">
										<p class="present">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В наличии</span>
										</p>
										<p class="absent">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
								            </svg>
											<span>Нет в наличии</span>
										</p>
									</div>

									<div class="catalog-item__v-code">
										<span class="catalog-item__v-code-name">
											Артикул:
										</span>
										«<span class="catalog-item__v-code-value">
											SportWood
										</span>»
									</div>

									<div class="catalog-item__price-cont">
										<span class="catalog-item__price">
											3 969 грн.
										</span>
										<span class="catalog-item__discount">
											-10%
										</span>
										<span class="catalog-item__old-price">
											3 869 грн.
										</span>
									</div>
									<div class="catalog-item__buy-more">
										<div class="catalog-item__have">
											<div class="catalog-item__buy">
												<button class="a-style-btn">
													<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
													<span>В корзину</span>
													</p>
													<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
													<span>В корзине</span>
													</p>
												</button>
											</div>
											<div class="catalog-item__more">
												<button class="c-style-btn compare">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
										            </svg>
												</button>
												<button class="c-style-btn favorites">
													<svg class="to-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
										            </svg>
													<svg class="in-favorites">
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
										            </svg>
												</button>
											</div>
										</div>
										<div class="catalog-item__report">
											<button class="b-style-btn js-avail-report">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
									            </svg>
												<span>Сообщить о наличии</span>
											</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="categories-block">
			<div class="container">
				<h2 class="categories-header section-header">
					Категории товаров
				</h2>
				<div class="categories-items">
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_01.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_01.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Спортивные уголки
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_02.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_02.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Гимнастические маты
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_03.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_03.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Шведская стенка
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_04.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_04.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Детские прыгунки
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_05.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_05.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Детские игровые комплексы
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_06.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_06.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Детские площадки
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_07.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_07.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Парты
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_08.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_08.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Игровые палатки
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_09.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_09.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Турники и брусья
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_10.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_10.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Дополнительное оборудование
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_11.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_11.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Детские песочницы
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_12.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_12.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Детская мебель
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_13.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_13.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Детские домики
							</p>
						</a>
					</div>
					<div class="categories-item">
						<div class="categories-item__cnt">5</div>
						<a href="<?=SITE_DIR?>catalog">
							<div class="categories-item__img">
								<picture>
								    <source srcset="<?=IMGS_DIR?>cats/cat_img_14.webp"
								                    type="image/webp">
								    <img src="<?=IMGS_DIR?>cats/cat_img_14.jpg"
								            alt="category image">
								</picture>
							</div>
							<p class="categories-item__desc">
								Шезлонги
							</p>
						</a>
					</div>
				</div>
			</div>
		</section>

		<section class="portfolio-block">
			<div class="container">
				<div class="section-header">
					<h2 class="portfolio-header">
					Наши работы
					</h2>
					<div class="swiper-buttons">
	    				<div class="prev-portfolio prev-slide">
	    					<i class="fa fa-angle-left" aria-hidden="true"></i>
	    				</div>
						<div class="next-portfolio next-slide">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</div>
					</div>
				</div>

				<div class="portfolio-items swiper-container">
					<div class="swiper-wrapper">
						<div class="swiper-slide portfolio-item">
							<a href="<?=SITE_DIR?>portfolio/section/">
								<div class="portfolio-item__img">
									<picture>
										<source srcset="<?=IMGS_DIR?>portfolio/01.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>portfolio/01.jpg">
									</picture>
								</div>
								<p class="portfolio-item__desc">
									Детские игровые комплексы
								</p>
							</a>
						</div>
						<div class="swiper-slide portfolio-item">
							<a href="<?=SITE_DIR?>portfolio/section/">
								<div class="portfolio-item__img">
									<picture>
									    <source srcset="<?=IMGS_DIR?>portfolio/02.webp"
									                    type="image/webp">
									    <img src="<?=IMGS_DIR?>portfolio/02.jpg"
									            alt="category image">
									</picture>
								</div>
								<p class="portfolio-item__desc">
									Детские площадки
								</p>
							</a>
						</div>
						<div class="swiper-slide portfolio-item">
							<a href="<?=SITE_DIR?>portfolio/section/">
								<div class="portfolio-item__img">
									<picture>
									    <source srcset="<?=IMGS_DIR?>portfolio/03.webp"
									                    type="image/webp">
									    <img src="<?=IMGS_DIR?>portfolio/03.jpg"
									            alt="category image">
									</picture>
								</div>
								<p class="portfolio-item__desc">
									Спортивные уголки
								</p>
							</a>
						</div>
						<div class="swiper-slide portfolio-item">
							<a href="<?=SITE_DIR?>portfolio/section/">
								<div class="portfolio-item__img">
									<picture>
									    <source srcset="<?=IMGS_DIR?>portfolio/04.webp"
									                    type="image/webp">
									    <img src="<?=IMGS_DIR?>portfolio/04.jpg"
									            alt="category image">
									</picture>
								</div>
								<p class="portfolio-item__desc">
									Спортивные уголки
								</p>
							</a>
						</div>
						<div class="swiper-slide portfolio-item">
							<a href="<?=SITE_DIR?>portfolio/section/">
								<div class="portfolio-item__img">
									<picture>
									    <source srcset="<?=IMGS_DIR?>portfolio/04.webp"
									                    type="image/webp">
									    <img src="<?=IMGS_DIR?>portfolio/04.jpg"
									            alt="category image">
									</picture>								</div>
								<p class="portfolio-item__desc">
									Детские игровые комплексы
								</p>
							</a>
						</div>
						<div class="swiper-slide portfolio-item">
							<a href="<?=SITE_DIR?>portfolio/section/">
								<div class="portfolio-item__img">
									<picture>
									    <source srcset="<?=IMGS_DIR?>portfolio/03.webp"
									                    type="image/webp">
									    <img src="<?=IMGS_DIR?>portfolio/03.jpg"
									            alt="category image">
									</picture>
								</div>
								<p class="portfolio-item__desc">
									Детские игровые комплексы
								</p>
							</a>
						</div>
						<div class="swiper-slide portfolio-item">
							<a href="<?=SITE_DIR?>portfolio/section/">
								<div class="portfolio-item__img">
									<picture>
									    <source srcset="<?=IMGS_DIR?>portfolio/02.webp"
									                    type="image/webp">
									    <img src="<?=IMGS_DIR?>portfolio/02.jpg"
									            alt="category image">
									</picture>
								</div>
								<p class="portfolio-item__desc">
									Детские игровые комплексы
								</p>
							</a>
						</div>
						<div class="swiper-slide portfolio-item">
							<a href="<?=SITE_DIR?>portfolio/section/">
								<div class="portfolio-item__img">
									<picture>
									    <source srcset="<?=IMGS_DIR?>portfolio/01.webp"
									                    type="image/webp">
									    <img src="<?=IMGS_DIR?>portfolio/01.jpg"
									            alt="category image">
									</picture>
								</div>
								<p class="portfolio-item__desc">
									Детские игровые комплексы
								</p>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="our-soc-block">
			<div class="container">
				<h2 class="our-soc-header section-header">
					Мы в соцсетях
				</h2>
				<div class="our-soc-items">
					<div class="our-soc-item our-soc-item--yt-post">
						<div class="our-soc-item--bground">
							<picture>
							    <source srcset="<?=IMGS_DIR?>portfolio/01.webp"
							                    type="image/webp">
							    <img src="<?=IMGS_DIR?>portfolio/01.jpg"
							            alt="category image">
							</picture>
						</div>
						<a href="#" target="_blank">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#play"></use>
				            </svg>
				        </a>
					</div>
					<div class="our-soc-item our-soc-item--fb our-soc-item--subscr">
						<a href="#">
							<div class="our-soc-item__logo">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
					            </svg>
							</div>

				        	<div class="our-soc-item__link">
				        		Подписаться на Facebook
				        	</div>
				        </a>
					</div>
					<div class="our-soc-item our-soc-item--fb-post">
						<a href="#" target="_blank">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--twt-post">
						<a href="#" target="_blank">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--yt our-soc-item--subscr">
						<a href="#">
							<div class="our-soc-item__logo">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#yt"></use>
					            </svg>
					        </div>

				        	<div class="our-soc-item__link">
				        		Подписаться на Youtube
				        	</div>
				        </a>
					</div>
					<div class="our-soc-item our-soc-item--twt-post">
						<a href="#">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--twt-post">
						<a href="#" target="_blank">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--fb-post">
						<a href="#">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--twt our-soc-item--subscr">
						<a href="#">
							<div class="our-soc-item__logo">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
					            </svg>
							</div>

				        	<div class="our-soc-item__link">
				        		Подписаться на Twitter
				        	</div>
				        </a>
					</div>
					<div class="our-soc-item our-soc-item--fb-post">
						<a href="#">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--twt-post">
						<a href="#">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--twt-post">
						<a href="#">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--yt our-soc-item--subscr">
						<a href="#">
							<div class="our-soc-item__logo">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#yt"></use>
					            </svg>
					        </div>

				        	<div class="our-soc-item__link">
				        		Подписаться на Youtube
				        	</div>
				        </a>
					</div>
					<div class="our-soc-item our-soc-item--twt-post">
						<a href="#">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--twt-post">
						<a href="#">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--fb-post">
						<a href="#">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--twt our-soc-item--subscr">
						<a href="#">
							<div class="our-soc-item__logo">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
					            </svg>
							</div>

				        	<div class="our-soc-item__link">
				        		Подписаться на Twitter
				        	</div>
				        </a>
					</div>
					<div class="our-soc-item our-soc-item--fb-post">
						<a href="#">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
					<div class="our-soc-item our-soc-item--twt-post">
						<a href="#">
							<div class="our-soc-item__icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
					            </svg>
							</div>
							<p class="our-soc-item__date">
								10.01.2019
							</p>
							<p class="our-soc-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi ducimus, corporis nisi quas praesentium illum, veritatis, iure magni repellat, deleniti molestias sunt autem fuga obcaecati delectus magnam sed. Ipsam consectetur, voluptatem nisi sit. Voluptatibus exercitationem vitae labore, laudantium fuga libero officiis. Iure doloribus, libero exercitationem repellat veritatis, adipisci autem
							</p>
						</a>
					</div>
				</div>
				<div class="show-more-block">
					<button class="a-style-btn js-more-soc">Показать еще</button>
				</div>
			</div>
		</section>

		<section class="about-shop-block container">
			<h2 class="about-shop-header section-header">
				Интернет-магазин детских товаров Kinderstar
			</h2>
			<div class="about-shop-content">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam saepe distinctio magnam est fugit, exercitationem minima quam quisquam, dolores incidunt laborum placeat reprehenderit porro consequatur excepturi nam, nulla dicta consectetur necessitatibus et quidem ex blanditiis quo sunt assumenda. Nobis reprehenderit eaque laboriosam, aperiam dolor doloremque sequi quae saepe incidunt, voluptas.</p>
				<p>Molestiae suscipit, delectus est necessitatibus possimus, totam ipsa animi, expedita sint perferendis similique dignissimos dolorem, libero porro excepturi laborum nostrum ducimus quo ex. Animi facilis, rerum nemo iure consequuntur, odio perspiciatis. Similique recusandae pariatur dolorum ullam facere perferendis tempora, expedita consectetur earum quasi cumque rerum, aspernatur debitis officia laudantium doloremque.</p>
				<p>Quasi quas dolore quos quis exercitationem ullam perspiciatis. Dolorum libero, enim similique tempora quibusdam aliquam accusantium saepe optio nam hic earum, in eligendi minus dicta, cum vitae neque reiciendis illo voluptatem quae itaque at deleniti nihil. Nisi illum dolore, cum, molestiae harum repudiandae deleniti accusantium. Dicta, consequuntur repellendus ut sit.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam saepe distinctio magnam est fugit, exercitationem minima quam quisquam, dolores incidunt laborum placeat reprehenderit porro consequatur excepturi nam, nulla dicta consectetur necessitatibus et quidem ex blanditiis quo sunt assumenda. Nobis reprehenderit eaque laboriosam, aperiam dolor doloremque sequi quae saepe incidunt, voluptas.</p>
				<p>Molestiae suscipit, delectus est necessitatibus possimus, totam ipsa animi, expedita sint perferendis similique dignissimos dolorem, libero porro excepturi laborum nostrum ducimus quo ex. Animi facilis, rerum nemo iure consequuntur, odio perspiciatis. Similique recusandae pariatur dolorum ullam facere perferendis tempora, expedita consectetur earum quasi cumque rerum, aspernatur debitis officia laudantium doloremque.</p>
				<p>Quasi quas dolore quos quis exercitationem ullam perspiciatis. Dolorum libero, enim similique tempora quibusdam aliquam accusantium saepe optio nam hic earum, in eligendi minus dicta, cum vitae neque reiciendis illo voluptatem quae itaque at deleniti nihil. Nisi illum dolore, cum, molestiae harum repudiandae deleniti accusantium. Dicta, consequuntur repellendus ut sit.</p>
			</div>
			<div class="about-shop-more">
				<span>Читать полностью<i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</div>
		</section>

<? include MAIN_TEMPLATE.'footer.php'; ?>