<nav class="v-multy-menu">
	<ul class="v-multy-menu__content">

		<li class="v-multy-menu__item">
			<div class="v-multy-menu__item-cont opened">
				<a href="#" class="v-multy-menu__name active">Детские игровые комплексы</a>
				<span class="v-multy-menu__toggle js-multy-menu"></span>
			</div>
			<ul class="v-multy-menu__sub">
				<li class="v-multy-menu__sub-item active">
					<a href="<?=SITE_DIR?>portfolio/element/">Babyland-3</a>
				</li>
				<li class="v-multy-menu__sub-item">
					<a href="<?=SITE_DIR?>portfolio/element/">Babyland-7</a>
				</li>
			</ul>
		</li>

		<li class="v-multy-menu__item">
			<div class="v-multy-menu__item-cont">
				<a href="#" class="v-multy-menu__name">Детские площадки</a>
				<span class="v-multy-menu__toggle js-multy-menu"></span>
			</div>
			<ul class="v-multy-menu__sub">
				<li class="v-multy-menu__sub-item">
					<a href="#">Детский спортивный уголок</a>
				</li>
				<li class="v-multy-menu__sub-item">
					<a href="#">Спортивный уголок - Бук</a>
				</li>
			</ul>
		</li>

		<li class="v-multy-menu__item">
			<div class="v-multy-menu__item-cont">
				<a href="#" class="v-multy-menu__name">Спортивные уголки</a>
				<span class="v-multy-menu__toggle js-multy-menu"></span>
			</div>
			<ul class="v-multy-menu__sub">
				<li class="v-multy-menu__sub-item">
					<a href="#">Детский спортивный уголок</a>
				</li>
				<li class="v-multy-menu__sub-item">
					<a href="#">Спортивный уголок - Бук</a>
				</li>
			</ul>
		</li>

		<li class="v-multy-menu__item">
			<div class="v-multy-menu__item-cont">
				<a href="#" class="v-multy-menu__name">Спортивный уголок - Цветной</a>
				<span class="v-multy-menu__toggle js-multy-menu"></span>
			</div>
			<ul class="v-multy-menu__sub">
				<li class="v-multy-menu__sub-item">
					<a href="#">Детский спортивный уголок</a>
				</li>
				<li class="v-multy-menu__sub-item">
					<a href="#">Спортивный уголок - Бук</a>
				</li>
			</ul>
		</li>

	</ul>
</nav>