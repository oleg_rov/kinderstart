<?php
include '../../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>

<!--<div class="page-content">-->
	<div class="container">
		<div class="page-block">

			<div class="side-block shift-side-block">
				<button class="shift-side-block__close">
					<svg class="svg-opened">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
					</svg>
				</button>
				<div class="catalog-side-wrapper">
<?php  include '../sidebar.php'; ?>
				</div>
			</div>

			<div class="main-block">
				<div class="regular-block">
					<h1 class="regular-block__header">
						Детские игровые комплексы
					</h1>

					<div class="shift-menu-top">
						<button class="shift-menu-top__btn js-cats-filters">
							<svg>
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
							</svg>
						</button>
					</div>

					<div class="portfolio-list-items">
						<div class="portfolio-list-elem">
							<div class="portfolio-list-elem__img">
								<a href="<?=SITE_DIR?>portfolio/element/">
									<picture>
										<source srcset="<?=IMGS_DIR?>portfolio/01.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>portfolio/01.jpg">
									</picture>
								</a>
							</div>

							<p class="portfolio-list-elem__title">
								<a href="<?=SITE_DIR?>portfolio/element/">
									Babyland-3
								</a>
							</p>
						</div>

						<div class="portfolio-list-elem">
							<div class="portfolio-list-elem__img">
								<a href="<?=SITE_DIR?>portfolio/element/">
									<picture>
										<source srcset="<?=IMGS_DIR?>portfolio/02.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>portfolio/02.jpg">
									</picture>
								</a>
							</div>

							<p class="portfolio-list-elem__title">
								<a href="<?=SITE_DIR?>portfolio/element/">
									Babyland-7
								</a>
							</p>
						</div>

						<div class="portfolio-list-elem">
							<div class="portfolio-list-elem__img">
								<a href="<?=SITE_DIR?>portfolio/element/">
									<picture>
										<source srcset="<?=IMGS_DIR?>portfolio/03.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>portfolio/03.jpg">
									</picture>
								</a>
							</div>

							<p class="portfolio-list-elem__title">
								<a href="<?=SITE_DIR?>portfolio/element/">
									Babyland-5
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--</div>-->

<?php include MAIN_TEMPLATE.'footer.php';?>