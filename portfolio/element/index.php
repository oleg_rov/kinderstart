<?php
include '../../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>

<!--<div class="page-content">-->
	<div class="container">
		<div class="page-block">

			<div class="side-block shift-side-block">
				<button class="shift-side-block__close">
					<svg class="svg-opened">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
					</svg>
				</button>

				<div class="catalog-side-wrapper">
<?php  include '../sidebar.php'; ?>
				</div>
			</div>

			<div class="main-block">
				<div class="regular-block">
					<h1 class="regular-block__header">
						Babyland-3
					</h1>

					<div class="shift-menu-top">
						<button class="shift-menu-top__btn js-cats-filters">
							<svg>
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
							</svg>
						</button>
					</div>

					<div class="portfolio-elems">
						<div class="portfolio-elems__item">
							<a href="<?=IMGS_DIR?>portfolio/slider_01/p_01.jpg"
									data-fancybox="portfolio-gallery"
									data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
									data-srcset="<?=IMGS_DIR?>portfolio/slider_01/p_01.webp type='image/webp'"
									>
								<picture>
									<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_01_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>portfolio/slider_01/p_01_s.jpg">
								</picture>
							</a>
						</div>

						<div class="portfolio-elems__item">
							<a href="<?=IMGS_DIR?>portfolio/slider_01/p_02.jpg"
									data-fancybox="portfolio-gallery"
									data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
									data-srcset="<?=IMGS_DIR?>portfolio/slider_01/p_02.webp type='image/webp'"
									>
								<picture>
									<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_02_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>portfolio/slider_01/p_02_s.jpg">
								</picture>
							</a>
						</div>

						<div class="portfolio-elems__item">
							<a href="<?=IMGS_DIR?>portfolio/slider_01/p_03.jpg"
									data-fancybox="portfolio-gallery"
									data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
									data-srcset="<?=IMGS_DIR?>portfolio/slider_01/p_03.webp type='image/webp'"
									>
								<picture>
									<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_03_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>portfolio/slider_01/p_03_s.jpg">
								</picture>
							</a>
						</div>

						<div class="portfolio-elems__item">
							<a href="<?=IMGS_DIR?>portfolio/slider_01/p_04.jpg"
									data-fancybox="portfolio-gallery"
									data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
									data-srcset="<?=IMGS_DIR?>portfolio/slider_01/p_04.webp type='image/webp'"
									>
								<picture>
									<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_04_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>portfolio/slider_01/p_04_s.jpg">
								</picture>
							</a>
						</div>

						<div class="portfolio-elems__item">
							<a href="<?=IMGS_DIR?>portfolio/slider_01/p_05.jpg"
									data-fancybox="portfolio-gallery"
									data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
									data-srcset="<?=IMGS_DIR?>portfolio/slider_01/p_05.webp type='image/webp'"
									>
								<picture>
									<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_05_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>portfolio/slider_01/p_05_s.jpg">
								</picture>
							</a>
						</div>

						<div class="portfolio-elems__item">
							<a href="<?=IMGS_DIR?>portfolio/slider_01/p_06.jpg"
									data-fancybox="portfolio-gallery"
									data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
									data-srcset="<?=IMGS_DIR?>portfolio/slider_01/p_06.webp type='image/webp'"
									>
								<picture>
									<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_06_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>portfolio/slider_01/p_06_s.jpg">
								</picture>
							</a>
						</div>

						<div class="portfolio-elems__item">
							<a href="<?=IMGS_DIR?>portfolio/slider_01/p_07.jpg"
									data-fancybox="portfolio-gallery"
									data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
									data-srcset="<?=IMGS_DIR?>portfolio/slider_01/p_07.webp type='image/webp'"
									>
								<picture>
									<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_07_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>portfolio/slider_01/p_07_s.jpg">
								</picture>
							</a>
						</div>

						<div class="portfolio-elems__item">
							<a href="<?=IMGS_DIR?>portfolio/slider_01/p_08.jpg"
									data-fancybox="portfolio-gallery"
									data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
									data-srcset="<?=IMGS_DIR?>portfolio/slider_01/p_08.webp type='image/webp'"
									>
								<picture>
									<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_08_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>portfolio/slider_01/p_08_s.jpg">
								</picture>
							</a>
						</div>

						<div class="portfolio-elems__item">
							<a href="<?=IMGS_DIR?>portfolio/slider_01/p_09.jpg"
									data-fancybox="portfolio-gallery"
									data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
									data-srcset="<?=IMGS_DIR?>portfolio/slider_01/p_09.webp type='image/webp'"
									>
								<picture>
									<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_09_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>portfolio/slider_01/p_09_s.jpg">
								</picture>
							</a>
						</div>

						<div class="portfolio-elems__item">
							<a href="<?=IMGS_DIR?>portfolio/slider_01/p_10.jpg"
									data-fancybox="portfolio-gallery"
									data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
									data-srcset="<?=IMGS_DIR?>portfolio/slider_01/p_10.webp type='image/webp'"
									>
								<picture>
									<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_10_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>portfolio/slider_01/p_10_s.jpg">
								</picture>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--</div>-->

<?php include MAIN_TEMPLATE.'footer.php';?>