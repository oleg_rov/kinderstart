<?php
include '../../../paths.php';
?>

<?php include MOB_TEMPLATE.'header.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					Кредит
				</h1>
				<div class="regular-block__content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste ullam quae ad, laborum iusto blanditiis labore animi vitae voluptate, veniam nam quod deleniti, error placeat consequuntur! Dolore error quae tempore aperiam quisquam, sed, cumque quia sint illo tempora. Repellat veniam, repudiandae ut, consectetur cupiditate tempora vel possimus asperiores iure suscipit?</p>
					<p>Consequuntur beatae eius aperiam blanditiis enim, molestias cum sed, deserunt qui quisquam tempora. Quaerat blanditiis vitae architecto velit autem nemo a enim. Accusamus animi exercitationem cupiditate nemo nisi, optio quis facilis dolore odit amet laboriosam odio commodi labore excepturi laborum molestiae quasi iste sapiente quo. Nisi suscipit, consequuntur maxime sit!</p>
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, ratione.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic et cupiditate quam neque earum expedita.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem temporibus, esse corporis dolorum fugiat pariatur assumenda. Aut perspiciatis consequuntur, in accusamus rerum! Possimus, dolor, impedit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, ratione.</li>
					</ul>
					<p>Odit pariatur, aliquam dolorem molestias natus. Minima officiis quibusdam, soluta debitis numquam! Eligendi alias quae omnis, quasi, suscipit a delectus impedit quo veniam magni expedita libero ut mollitia nesciunt eos vel. Recusandae labore consectetur, dolor, aliquid a perferendis repudiandae officia. Repellendus recusandae ad, aliquam delectus soluta illo dolorem eligendi eius?</p>
					<p>Sequi sint in cupiditate, vitae rem dolorem excepturi repellendus, quos asperiores omnis quisquam possimus dolore ratione nostrum at esse doloribus laboriosam dolores molestias culpa, nihil eius architecto! Qui voluptate velit cupiditate quia. Officiis excepturi adipisci accusantium consequatur aspernatur aut non sit quibusdam amet. Iure laudantium ullam, perferendis libero aliquam. Vel!</p>
					<p>Laudantium doloremque, consectetur, neque et dolore, sit eaque deleniti vero adipisci natus asperiores autem, quasi quia! Fuga saepe adipisci aut voluptate esse? Dolores maiores assumenda velit provident architecto expedita molestias esse nihil omnis ullam deleniti non ipsum rerum voluptas facilis libero eaque magnam, explicabo a error eveniet eum sapiente quia.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include MOB_TEMPLATE.'footer.php';?>