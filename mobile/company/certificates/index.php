<?php
include '../../../paths.php';
?>

<?php include MOB_TEMPLATE.'header.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					Сертификаты
				</h1>

				<div class="certif__items">
					<div class="certif__item">
						<div class="certif__item-img">
							<picture>
								<source srcset="<?=IMGS_DIR?>certificates/c_01_s.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>certificates/c_01_s.jpg">
							</picture>
						</div>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_02_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_02_s.jpg">
								</picture>
						</div>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<div class="certif__item-img">
							<picture>
								<source srcset="<?=IMGS_DIR?>certificates/c_03_s.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>certificates/c_03_s.jpg">
							</picture>
						</div>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<div class="certif__item-img">
							<picture>
								<source srcset="<?=IMGS_DIR?>certificates/c_01_s.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>certificates/c_01_s.jpg">
							</picture>
						</div>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_02_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_02_s.jpg">
								</picture>
						</div>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<div class="certif__item-img">
							<picture>
								<source srcset="<?=IMGS_DIR?>certificates/c_03_s.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>certificates/c_03_s.jpg">
							</picture>
						</div>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<div class="certif__item-img">
							<picture>
								<source srcset="<?=IMGS_DIR?>certificates/c_01_s.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>certificates/c_01_s.jpg">
							</picture>
						</div>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_02_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_02_s.jpg">
								</picture>
						</div>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<div class="certif__item-img">
							<picture>
								<source srcset="<?=IMGS_DIR?>certificates/c_03_s.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>certificates/c_03_s.jpg">
							</picture>
						</div>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php include MOB_TEMPLATE.'footer.php';?>