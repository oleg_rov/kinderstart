<?php
include '../../../paths.php';
?>

<?php include MOB_TEMPLATE.'header.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					Наши партнеры
				</h1>
				<div class="partners__items">
					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include MOB_TEMPLATE.'footer.php';?>