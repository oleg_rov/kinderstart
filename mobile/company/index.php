<?php
include '../../paths.php';
?>

<?php include MOB_TEMPLATE.'header.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					О компании
				</h1>
				<div class="about-block">
					<div class="about-block__img">
						<picture>
							<source srcset="<?=IMGS_DIR?>about.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>about.jpg">
						</picture>
					</div>
					<p>Kinerstar— компания, анализирующая потребности и бизнес-процессы организаций. Работаем с 2003 года, занимаемся автоматизацией бизнеса, управлением активами и инвестиций, финансовым аудитом и оказываем услуги лизинга и юридической практики.</p>

					<div class="benefits-about">
						<div class="benefits-about__item">
							<div class="benefits-about__img">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#benefit_01"></use>
					            </svg>
							</div>
							<p class="benefits-about__txt">
								Товар от производителя
							</p>
						</div>

						<div class="benefits-about__item">
							<div class="benefits-about__img">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#benefit_02"></use>
					            </svg>
							</div>
							<p class="benefits-about__txt">
								Экологически чистый материалы
							</p>
						</div>

						<div class="benefits-about__item">
							<div class="benefits-about__img">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#benefit_03"></use>
					            </svg>
							</div>
							<p class="benefits-about__txt">
								Экологически чистый лак
							</p>
						</div>
					</div>

					<div class="about-block__desc">
						<h3>Чем мы можем быть вам полезны</h3>
						<ul>
							<li>Увеличиваем прибыль вашей организации. Проанализируем бизнес-процессы и найдем способы их оптимизации. Перестроим работу так, чтобы сократить издержки или дать бизнесу потенциал к ускоренному развитию.</li>

							<li>Автоматизируем процессы. Заменим ручной труд машинным или внедрим систему для эффективного управления бизнесом. Знаем все нюансы работы с BI- CRM- SCM- и прочими системами.</li>

							<li>Защищаем ваши права. Проконсультируем по юридическим вопросам, подготовим нужные документы для организации или защитим вашу позицию в суде.</li>

							<li>Дадим оценку организации. Проанализируем экономическое состояние вашей организации, оценим правильность и законность ведения бухгалтерского учета и выявим предпосылки мошенничества ваших сотрудников.</li>
						</ul>
						<h3>Чем мы можем быть вам полезны</h3>
						<p>Увеличиваем прибыль вашей организации. Проанализируем бизнес-процессы и найдем способы их оптимизации. Перестроим работу так, чтобы сократить издержки или дать бизнесу потенциал к ускоренному развитию.</p>
						<p>Автоматизируем процессы. Заменим ручной труд машинным или внедрим систему для эффективного управления бизнесом. Знаем все нюансы работы с BI- CRM- SCM- и прочими системами.</p>
						<p>Защищаем ваши права. Проконсультируем по юридическим вопросам, подготовим нужные документы для организации или защитим вашу позицию в суде.</p>
						<p>Дадим оценку организации. Проанализируем экономическое состояние вашей организации, оценим правильность и законность ведения бухгалтерского учета и выявим предпосылки мошенничества ваших сотрудников.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include MOB_TEMPLATE.'footer.php';?>