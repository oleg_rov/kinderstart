<?php
include '../../../paths.php';
?>

<?php include MOB_TEMPLATE.'header.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					Оплата и доставка
				</h1>

				<h2 class="regular-block__sub-header">1. Доставка до пункта выдачи</h2>
				<p>Заказ оплачивается транспортной компании в момент получения заказа в пункте самовывоза. Товар передается в доставку в течение двух дней с момента оформления заказа.</p>

				<h3 class="delivery-list-header">Список транспортных компаний:</h3>
				<div class="delivery-list">
					<div class="delivery-list__item">
						<img src="<?=IMGS_DIR?>nova-poshta.svg" alt="Новая почта">
					</div>
					<div class="delivery-list__item">
						<img src="<?=IMGS_DIR?>ukrposhta.svg" alt="Укр. почта">
					</div>
					<div class="delivery-list__item">
						<img src="<?=IMGS_DIR?>intime.svg" alt="Ин тайм">
					</div>
				</div>

				<h2 class="regular-block__sub-header">2. Курьером</h2>
				<p>Время доставки курьером зависит от транспортной компании. В день доставки курьер свяжется с вами по номеру телефона указанному в заказе.</p>

				<div class="delivery-detail-list">
					<div class="delivery-detail-item">
						<div class="delivery-detail-item__header">
							<div class="delivery-detail-item__logo">
								<img src="<?=IMGS_DIR?>nova-poshta.svg" alt="Новая почта">
							</div>
							<div class="delivery-detail-item__title">
								<h3 class="delivery-list-header">
									Новая почта
								</h3>
								<p>Срок: 5-6 дней<br> при заказе от 50 грн / при оплате онлайн - бесплатно<br> при заказе меньше 30 грн - 50 грн</p>
							</div>
						</div>

						<div class="delivery-detail-item__more">
							<span class="js-delivery-about">Узнать больше<i class="fa fa-angle-down" aria-hidden="true"></i></span>
						</div>

						<div class="delivery-detail-item__content">
							<h4>Доставка:</h4>
							<p>Экспресс-доставка EMS Почта России доставит Ваш заказ в самые короткие сроки по всей России (2-5 дня в центральные города области, 3-10 дней в остальные города)</p>
							<h4>Информирование:</h4>
							<p>Вам придет уведомление на sms, e-mail об отправке посылки, с указанием прогнозируемого срока доставки. Перед доставкой с Вами связываются телефону, указанному в заказе. </p>
							<h4>Возврат:</h4>
							<p>После получения можно вернуть товар следующими способами:</p>
							<ul>
								<li>
									Почтой России. Заполните заявление, отправьте почтой. Получите компенсацию по приходу возврата в магазин
								</li>
								<li>
									IM-Logistics (Курьер). Закажите приезд курьера для принятия возврата через службу поддержки и возвратите товар. Получите компенсацию по возврату товара в магазин.
								</li>
								<li>
									IM-Logistics (Пункт Выдачи). Закажите возврат посылки в пункт выдачи через службу поддержки и возвратите товар. Получите компенсацию по возврату товара в магазин.
								</li>
							</ul>
						</div>
					</div>

					<div class="delivery-detail-item">
						<div class="delivery-detail-item__header">
							<div class="delivery-detail-item__logo">
								<img src="<?=IMGS_DIR?>ukrposhta.svg" alt="Укр Почта">
							</div>
							<div class="delivery-detail-item__title">
								<h3 class="delivery-list-header">
									Укр Почта
								</h3>
								<p>Срок: 5-6 дней<br> при заказе от 50 грн / при оплате онлайн - бесплатно<br> при заказе меньше 30 грн - 50 грн</p>
							</div>
						</div>

						<div class="delivery-detail-item__more">
							<span class="js-delivery-about">Узнать больше<i class="fa fa-angle-down" aria-hidden="true"></i></span>
						</div>

						<div class="delivery-detail-item__content">
							<h4>Доставка:</h4>
							<p>Экспресс-доставка EMS Почта России доставит Ваш заказ в самые короткие сроки по всей России (2-5 дня в центральные города области, 3-10 дней в остальные города)</p>
							<h4>Информирование:</h4>
							<p>Вам придет уведомление на sms, e-mail об отправке посылки, с указанием прогнозируемого срока доставки. Перед доставкой с Вами связываются телефону, указанному в заказе. </p>
							<h4>Возврат:</h4>
							<p>После получения можно вернуть товар следующими способами:</p>
							<ul>
								<li>
									Почтой России. Заполните заявление, отправьте почтой. Получите компенсацию по приходу возврата в магазин
								</li>
								<li>
									IM-Logistics (Курьер). Закажите приезд курьера для принятия возврата через службу поддержки и возвратите товар. Получите компенсацию по возврату товара в магазин.
								</li>
								<li>
									IM-Logistics (Пункт Выдачи). Закажите возврат посылки в пункт выдачи через службу поддержки и возвратите товар. Получите компенсацию по возврату товара в магазин.
								</li>
							</ul>
						</div>
					</div>

					<div class="delivery-detail-item">
						<div class="delivery-detail-item__header">
							<div class="delivery-detail-item__logo">
								<img src="<?=IMGS_DIR?>intime.svg" alt="Ин тайм">
							</div>
							<div class="delivery-detail-item__title">
								<h3 class="delivery-list-header">
									Ин Тайм
								</h3>
								<p>Срок: 5-6 дней<br> при заказе от 50 грн / при оплате онлайн - бесплатно<br> при заказе меньше 30 грн - 50 грн</p>
							</div>
						</div>

						<div class="delivery-detail-item__more">
							<span class="js-delivery-about">Узнать больше<i class="fa fa-angle-down" aria-hidden="true"></i></span>
						</div>

						<div class="delivery-detail-item__content">
							<h4>Доставка:</h4>
							<p>Экспресс-доставка EMS Почта России доставит Ваш заказ в самые короткие сроки по всей России (2-5 дня в центральные города области, 3-10 дней в остальные города)</p>
							<h4>Информирование:</h4>
							<p>Вам придет уведомление на sms, e-mail об отправке посылки, с указанием прогнозируемого срока доставки. Перед доставкой с Вами связываются телефону, указанному в заказе. </p>
							<h4>Возврат:</h4>
							<p>После получения можно вернуть товар следующими способами:</p>
							<ul>
								<li>
									Почтой России. Заполните заявление, отправьте почтой. Получите компенсацию по приходу возврата в магазин
								</li>
								<li>
									IM-Logistics (Курьер). Закажите приезд курьера для принятия возврата через службу поддержки и возвратите товар. Получите компенсацию по возврату товара в магазин.
								</li>
								<li>
									IM-Logistics (Пункт Выдачи). Закажите возврат посылки в пункт выдачи через службу поддержки и возвратите товар. Получите компенсацию по возврату товара в магазин.
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include MOB_TEMPLATE.'footer.php';?>