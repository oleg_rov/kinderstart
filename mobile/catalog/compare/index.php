<?php
include '../../../paths.php';
?>

<?php include MOB_TEMPLATE.'header.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
			<div class="catalog-compare">
				<h1 class="catalog-compare__header">
					Сравнение товаров
				</h1>

				<div class="catalog-compare-container">
					<div class="catalog-compare__control">
						<button class="js-compare__clear ctrl-btn">
							<span class="ctrl-btn__img">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#thin-close"></use>
					            </svg>
							</span>
				            <span class="ctrl-btn__txt">Удалить список</span>
						</button>

						<div class="catalog-compare__control-group">
							<button class="ctrl-btn campare-show-all selected">
								<span class="ctrl-btn__img">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#thin-ok"></use>
						            </svg>
								</span>
					            <span class="ctrl-btn__txt">Только различающиеся</span>
							</button>

							<button class="ctrl-btn campare-show-diff">
								<span class="ctrl-btn__img">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#thin-ok"></use>
						            </svg>
								</span>
					            <span class="ctrl-btn__txt">Все параметры</span>
							</button>
						</div>
					</div>

					<div class="catalog-compare__products swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="catalog-compare-item">
									<button class="catalog-compare-item__del">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
									</button>
									<div class="catalog-compare-item__img">
										<a href="<?=MOB_SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_01.jpg">
											</picture>
										</a>
									</div>
									<p class="catalog-compare-item__title">
										<a href="<?=MOB_SITE_DIR?>catalog/element">Детский спортивный комплекс для дома SportWood  Plus</a>
									</p>
									<p class="catalog-compare-item__price">
										2 619 грн.
									</p>
									<div class="catalog-compare-item__buttons">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
												</p>
												<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
												</p>
											</button>
										</div>

										<div class="catalog-item__more">
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
								</div>

								<div class="catalog-compare-item__props">
									<div class="catalog-compare-item__prop"
										data-prop-id="1"
										data-prop-code="HEIGHT"
										data-prop-name="Высота">
										130 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="2"
										data-prop-code="WIDTH"
										data-prop-name="Ширина">
										85 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="3"
										data-prop-code="LENGTH"
										data-prop-name="Длинна">
										132 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="4"
										data-prop-code="MATERIAL"
										data-prop-name="Материал">
										Берёза
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="5"
										data-prop-code="CROSSBARS"
										data-prop-name="Перекладины">
										Бук
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="6"
										data-prop-code="PERMIS_LOAD"
										data-prop-name="Допустимые нагрузки">
										60 кг
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="7"
										data-prop-code="FAST_TYPE"
										data-prop-name="Вид крепления">
										Не требует крепления
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="8"
										data-prop-code="WEIGHT"
										data-prop-name="Вес товара">
										30 кг
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="9"
										data-prop-code="GUARANTEE"
										data-prop-name="Гарантия">
										12 месяцев
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="10"
										data-prop-code="FILES"
										data-prop-name="Файлы">
										Array
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="11"
										data-prop-code="COLOR"
										data-prop-name="Цвет">
										Цветной
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="12"
										data-prop-code="SLIDE_LENGTH"
										data-prop-name="Длинна горки">
										150 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="13"
										data-prop-code="MANUFACTURER"
										data-prop-name="Производитель">
										SportBaby
									</div>
								</div>
							</div>

							<div class="swiper-slide">
								<div class="catalog-compare-item">
									<button class="catalog-compare-item__del">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
									</button>
									<div class="catalog-compare-item__img">
										<a href="<?=MOB_SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_02.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_02.jpg">
											</picture>
										</a>
									</div>
									<p class="catalog-compare-item__title">
										<a href="<?=MOB_SITE_DIR?>catalog/element">Детский спортивный комплекс для дома SportWood  Plus</a>
									</p>
									<p class="catalog-compare-item__price">
										2 619 грн.
									</p>
									<div class="catalog-compare-item__buttons">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
												</p>
												<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
												</p>
											</button>
										</div>

										<div class="catalog-item__more">
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
								</div>

								<div class="catalog-compare-item__props">
									<div class="catalog-compare-item__prop"
										data-prop-id="1"
										data-prop-code="HEIGHT"
										data-prop-name="Высота">
										130 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="2"
										data-prop-code="WIDTH"
										data-prop-name="Ширина">
										85 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="3"
										data-prop-code="LENGTH"
										data-prop-name="Длинна">
										132 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="4"
										data-prop-code="MATERIAL"
										data-prop-name="Материал">
										Берёза
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="5"
										data-prop-code="CROSSBARS"
										data-prop-name="Перекладины">
										Бук
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="6"
										data-prop-code="PERMIS_LOAD"
										data-prop-name="Допустимые нагрузки">
										60 кг
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="7"
										data-prop-code="FAST_TYPE"
										data-prop-name="Вид крепления">
										Не требует крепления
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="8"
										data-prop-code="WEIGHT"
										data-prop-name="Вес товара">
										30 кг
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="9"
										data-prop-code="GUARANTEE"
										data-prop-name="Гарантия">
										12 месяцев
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="10"
										data-prop-code="FILES"
										data-prop-name="Файлы">
										Array
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="11"
										data-prop-code="COLOR"
										data-prop-name="Цвет">
										Цветной
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="12"
										data-prop-code="SLIDE_LENGTH"
										data-prop-name="Длинна горки">
										150 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="13"
										data-prop-code="MANUFACTURER"
										data-prop-name="Производитель">
										SportBaby
									</div>
								</div>
							</div>

							<div class="swiper-slide">
								<div class="catalog-compare-item">
									<button class="catalog-compare-item__del">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
									</button>
									<div class="catalog-compare-item__img">
										<a href="<?=MOB_SITE_DIR?>catalog/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>prod_03.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>prod_03.jpg">
											</picture>
										</a>
									</div>
									<p class="catalog-compare-item__title">
										<a href="<?=MOB_SITE_DIR?>catalog/element">Детский спортивный комплекс для дома SportWood  Plus</a>
									</p>
									<p class="catalog-compare-item__price">
										2 619 грн.
									</p>
									<div class="catalog-compare-item__buttons">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
										            </svg>
												</p>
												<p class="in-cart">
													<svg>
										                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
										            </svg>
												</p>
											</button>
										</div>

										<div class="catalog-item__more">
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
								</div>

								<div class="catalog-compare-item__props">
									<div class="catalog-compare-item__prop"
										data-prop-id="1"
										data-prop-code="HEIGHT"
										data-prop-name="Высота">
										130 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="2"
										data-prop-code="WIDTH"
										data-prop-name="Ширина">
										85 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="3"
										data-prop-code="LENGTH"
										data-prop-name="Длинна">
										132 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="4"
										data-prop-code="MATERIAL"
										data-prop-name="Материал">
										Берёза
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="5"
										data-prop-code="CROSSBARS"
										data-prop-name="Перекладины">
										Бук
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="6"
										data-prop-code="PERMIS_LOAD"
										data-prop-name="Допустимые нагрузки">
										60 кг
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="7"
										data-prop-code="FAST_TYPE"
										data-prop-name="Вид крепления">
										Не требует крепления
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="8"
										data-prop-code="WEIGHT"
										data-prop-name="Вес товара">
										30 кг
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="9"
										data-prop-code="GUARANTEE"
										data-prop-name="Гарантия">
										12 месяцев
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="10"
										data-prop-code="FILES"
										data-prop-name="Файлы">
										Array
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="11"
										data-prop-code="COLOR"
										data-prop-name="Цвет">
										Цветной
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="12"
										data-prop-code="SLIDE_LENGTH"
										data-prop-name="Длинна горки">
										150 см
									</div>
									<div class="catalog-compare-item__prop"
										data-prop-id="13"
										data-prop-code="MANUFACTURER"
										data-prop-name="Производитель">
										SportBaby
									</div>
								</div>
							</div>
						</div>

						<div class="swiper-scrollbar catalog-compare-scrollbar"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include MOB_TEMPLATE.'footer.php';?>