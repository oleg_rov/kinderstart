<?php
include '../../paths.php';
?>
<?php include MOB_TEMPLATE.'header.php';?>

<?php include 'sidebar.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
			<div class="regular-block">

			<h1 class="regular-block__header">
				Детские спортивные уголки
			</h1>
			<div class="catalog-section-top">
				<button class="catalog-section-tune-btn js-cats-filters">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#tune"></use>
		            </svg>
				</button>

<?php
$path = getPath();
$arQuery = [];
if (!empty($_GET)) {
	foreach ($_GET as $k=>$v) {
		if ($k !== 'sort' && $k !== 'order') {
			$arQuery[$k] = $v;
		}
	}
}
$path = $path . '?' . http_build_query($arQuery);

$sortName = 'По популярности';
switch($_GET['sort']) {
	case 'pop':
		$sortName = 'По популярности';
		break;
	case 'price':
		$sortName = 'По цене';
		break;
	case 'name':
		$sortName = 'По названию';
		break;
	default:
		$sortName = 'По популярности';
}
$sortOrder = 'desc';
if ($_GET['order'] == 'asc')
	$sortOrder = 'asc';
?>
				<div class="catalog-section-sort">
					<button class="catalog-section-sort__item js-sort-dir sort-<?=$sortOrder?>">
						<span class="catalog-section-sort__sort-name">
							<svg class="desc-dir">
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
				            </svg>
							<svg  class="asc-dir">
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
				            </svg>

							<span><?=$sortName?></span>
						</span>
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</button>

					<div class="catalog-section-sort__drop-box">
						<ul>
							<li class="catalog-section-sort__item sort-desc">
								<a href="<?=$path?>&sort=pop&order=desc">
									<div class="catalog-section-sort__sort-name">
										<svg class="desc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
							            </svg>
										<svg  class="asc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
							            </svg>
										<span>По популярности</span>
									</div>
								</a>
							</li>

							<li class="catalog-section-sort__item sort-asc">
								<a href="<?=$path?>&sort=pop&order=asc">
									<div class="catalog-section-sort__sort-name">
										<svg class="desc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
							            </svg>
										<svg  class="asc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
							            </svg>
										<span>По популярности</span>
									</div>
								</a>
							</li>

							<li class="catalog-section-sort__item sort-desc">
								<a href="<?=$path?>&sort=price&order=desc">
									<div class="catalog-section-sort__sort-name">
										<svg class="desc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
							            </svg>
										<svg  class="asc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
							            </svg>
										<span>По цене</span>
									</div>
								</a>
							</li>

							<li class="catalog-section-sort__item sort-asc">
								<a href="<?=$path?>&sort=price&order=asc">
									<div class="catalog-section-sort__sort-name">
										<svg class="desc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
							            </svg>
										<svg  class="asc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
							            </svg>
										<span>По цене</span>
									</div>
								</a>
							</li>

							<li class="catalog-section-sort__item sort-desc">
								<a href="<?=$path?>&sort=name&order=desc">
									<div class="catalog-section-sort__sort-name">
										<svg class="desc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
							            </svg>
										<svg  class="asc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
							            </svg>
										<span>По названию</span>
									</div>
								</a>
							</li>

							<li class="catalog-section-sort__item sort-asc">
								<a href="<?=$path?>&sort=name&order=asc">
									<div class="catalog-section-sort__sort-name">
										<svg class="desc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
							            </svg>
										<svg  class="asc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
							            </svg>
										<span>По названию</span>
									</div>
								</a>
							</li>
						</ul>
					</div>
				</div>

			</div>

			<div class="catalog-section">
				<div class="catalog-items">
					<div class="catalog-item">
						<div class="catalog-item__stickers item-stickers">
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div>
							<!-- <div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div> -->
						</div>

						<div class="catalog-item__img">
							<a href="<?=MOB_SITE_DIR?>catalog/element">
								<picture>
									<source
									srcset="<?=IMGS_DIR?>prod_01.webp"
									type="image/webp">

									<img
									src="<?=IMGS_DIR?>prod_01.jpg">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="<?=MOB_SITE_DIR?>catalog/element">Детский спортивный уголок для дома «Kind-Start»</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 149 грн.
								</span>
								<span class="catalog-item__discount">
								</span>
								<span class="catalog-item__old-price">
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>

									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>

								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-item catalog-item--absent">
						<div class="catalog-item__stickers item-stickers">
							<!-- <div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div> -->
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="catalog-item__img">
							<a href="<?=MOB_SITE_DIR?>catalog/element">
								<picture>
									<source
									srcset="<?=IMGS_DIR?>prod_02.webp"
									type="image/webp">

									<img
									src="<?=IMGS_DIR?>prod_02.jpg">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="<?=MOB_SITE_DIR?>catalog/element">Детский спортивный уголок «Эверест-2»</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-item">
						<div class="catalog-item__stickers item-stickers">
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div>
							<!-- <div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div> -->
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="catalog-item__img">
							<a href="<?=MOB_SITE_DIR?>catalog/element">
								<picture>
									<source
									srcset="<?=IMGS_DIR?>prod_04.webp"
									type="image/webp">

									<img
									src="<?=IMGS_DIR?>prod_04.jpg">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="<?=MOB_SITE_DIR?>catalog/element">Спортивный комплекс для дома Акварелька Plus 1-1</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-item catalog-item--discount">
						<div class="catalog-item__stickers item-stickers">
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="catalog-item__img">
							<a href="<?=MOB_SITE_DIR?>catalog/element">
								<picture>
									<source
									srcset="<?=IMGS_DIR?>prod_03.webp"
									type="image/webp">

									<img
									src="<?=IMGS_DIR?>prod_03.jpg">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="<?=MOB_SITE_DIR?>catalog/element">Детская площадка Babyland-12</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-item">
						<div class="catalog-item__stickers item-stickers">
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="catalog-item__img">
							<a href="<?=MOB_SITE_DIR?>catalog/element">
								<picture>
									<source
									srcset="<?=IMGS_DIR?>prod_05.webp"
									type="image/webp">

									<img
									src="<?=IMGS_DIR?>prod_05.jpg">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="<?=MOB_SITE_DIR?>catalog/element">Детский спортивный комплекс BambinoWood Color Plus 1-1</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-item catalog-item--absent">
						<div class="catalog-item__stickers item-stickers">
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="catalog-item__img">
							<a href="<?=MOB_SITE_DIR?>catalog/element">
								<picture>
									<source
									data-srcset="<?=IMGS_DIR?>prod_02.webp"
									type="image/webp">

									<source
									data-srcset="<?=IMGS_DIR?>prod_02.jpg"
									type="image/jpeg">

									<img
									src="<?=IMGS_DIR?>cats/noimg.png"
									class="lazyestload">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="<?=MOB_SITE_DIR?>catalog/element">Детский спортивный комплекс Bambino Wood Color Plus 1-1</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>
				</div>

				<div class="catalog-pagination pagination-section">
					<ul class="pagination-section__items">
						<li class="active">
							<span class="nav-current-page">1</span>
						</li>
						<li>
							<a href="#?PAGEN_2=2">2</a>
						</li>
						<li>
							<a href="#?PAGEN_2=3">3</a>
						</li>
						<li class="points"><span>...</span></li>
						<li>
							<a href="#?PAGEN_2=24">26</a>
						</li>
						<li>
							<a href="#?PAGEN_2=25">25</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="catalog-section-desc">
				<div class="catalog-section-desc__img">
					<picture>
						<source srcset="<?=IMGS_DIR?>section_img.webp" type="image/webp">
						<img src="<?=IMGS_DIR?>section_img.jpg">
					</picture>
				</div>
				<div class="catalog-section-desc__txt site-styles">
					<h2>Детский спортивный уголок для дома – место для веселых игр!</h2>
					<p>Значимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного развития. Равным образом консультация с широким активом играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Разнообразный и богатый опыт сложившаяся структура организации позволяет оценить значение дальнейших направлений развития. Равным образом консультация с широким активом представляет собой интересный эксперимент проверки новых предложений.</p>
					<p>Не следует, однако забывать, что укрепление и развитие структуры способствует подготовки и реализации системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что сложившаяся структура организации требуют определения и уточнения модели развития. Товарищи! сложившаяся структура организации способствует подготовки и реализации соответствующий условий активизации.</p>
					<h2>Как правильно выбрать спортивный детский уголок в комнату?</h2>
					<p>Ваш выбор должен основываться не столько на внешнем виде самого комплекса, сколько на потребностях вашего ребенка. Ниже приведен список того, что конкретно нужно учесть:</p>
					<ul>
						<li>Физические размеры – это важно не только в том плане, что комплекс должен «стать» в вашу квартиру, но и в том, что он должен соответствовать возрасту вашего малыша.</li>
						<li>Комплектация – в зависимости от того, для каких целей вы приобретаете спорт-уголок, нужно взвешенно подойти к выбору навесного оборудования, которое идет в комплекте, а также к возможности его докупить позднее.</li>
						<li>Материал – в нашем каталоге представлены модели из двух пород дерева: сосны и бука. Каждая из них имеет свои преимущества и недостатки, однако в любом случае это будет более безопасная, экологичная и надежная альтернатива металлическим конструкциям.</li>
					</ul>
					<p>Исходя из этих трех параметров, необходимо выбрать тот вариант комплекса, который решит список поставленных перед ним задач. Купить же детский спортивный уголок в Киеве, Харькове, Днепропетровске или любом другом городе Украины, вам помогут менеджеры интернет-магазина SportBaby.</p>
					<h2>Спортивный уголок от производителя – качество по доступной цене!</h2>
					<p>Наша компания занимает сегмент производителей недорогих, но неизменно качественных спортивных тренажеров для детей. Этот выбор мы сделали исходя из потребностей наших покупателей, с которыми имеем удовольствие работа</p>
					<p>«Просто, надежно, недорого», – вот что хотят наши клиенты! Добавив к этому «экологично», мы выпустили на рынок и предлагаем вашему вниманию недорогие спортивные уголки для детей по доступной цене. Звоните или заказывайте на сайте, мы готовы доставить вашу покупку в любую точку Украины в течение 1-2 рабочих дней! Днепропетровск, Одесса, Львов – сотни довольных клиентов уже приобрели себе товары с нашего сайта в этих городах.</p>
				</div>

				<div class="catalog-section-desc__more">
					<span>Читать полностью<i class="fa fa-angle-right" aria-hidden="true"></i></span>
				</div>
			</div>
		</div><? /* end of .regular-block */?>
		</div><? /* end of .page-block */?>
	</div><? /* end of .container */?>
</div><? /* end of .page-content */?>

<?php include MOB_TEMPLATE.'footer.php';?>