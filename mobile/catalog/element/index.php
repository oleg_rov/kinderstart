<? include '../../../paths.php'; ?>

<?php include MOB_TEMPLATE.'header.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
<!--
.product-item--lot - много
.product-item--few - мало
.product-item--absent - Нет в наличии
-->
			<div class="product-main product-section product-item--lot">
				<div class="product-gallery">
					<div class="swiper-container product-images">
						<div class="catalog-item__stickers item-stickers">
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<picture>
									<source srcset="<?=IMGS_DIR?>products/product_01.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>products/product_01.jpg">
								</picture>
							</div>
							<div class="swiper-slide" style="background-color: #ffffaa"></div>
							<div class="swiper-slide" style="background-color: #aaffaa"></div>
							<div class="swiper-slide" style="background-color: #ffcccc"></div>
							<div class="swiper-slide js-show-video video-slide" style="background-color: #f5f5f5" data-video="Db4K5V01q5c">
								<picture>
									<source srcset="https://i.ytimg.com/vi_webp/Db4K5V01q5c/sddefault.webp" type="image/webp">
									<img class="video__media" src="https://i.ytimg.com/vi/Db4K5V01q5c/sddefault.jpg" alt="">
								</picture>
							</div>
							<div class="swiper-slide js-show-video video-slide" style="background-color: #f5f5f5" data-video="ENrxWQKYI1I">
								<picture>
									<source srcset="https://i.ytimg.com/vi_webp/ENrxWQKYI1I/sddefault.webp" type="image/webp">
									<img class="video__media" src="https://i.ytimg.com/vi/ENrxWQKYI1I/sddefault.jpg" alt="">
								</picture>
							</div>
						</div>
					</div>
					<div class="product-gallery__pagination"></div>
				</div>

				<h1 class="product-title">
					Детский спортивный уголок «Эверест-2»
				</h1>

				<div class="product-v-code">
					<span>Артикул: «Kind-Start»</span>
				</div>

				<div class="product-excerpt">
					<div class="product-excerpt__txt">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ratione id officia voluptate soluta vel ipsum autem fugiat eum sapiente, natus dignissimos cupiditate voluptas quos asperiores repellat perferendis, enim aut eligendi quo doloremque nobis fuga labore, delectus reiciendis!
					</div>
					<div class="product-excerpt__more">
						<span>Подробнее<i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</div>
				</div>

				<div class="product-upload">
					<div class="product-upload__title">
						Инструкции
					</div>
					<a href="<?=IMGS_DIR?>prod_01.jpg" class="product-upload__link" download>
						<svg  class="asc-dir">
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#get-file"></use>
			            </svg>
			            <span>«Kind-Start»</span>
					</a>
				</div>

				<div class="product-sale">
					<div class="product-amount">
						<input type="text" class="product-amount__input" value="1">
						<button class="product-amount__btn js-amount-plus">+</button>
						<span class="product-amount__units">шт</span>
					</div>

					<div class="product-price">
						<span class="product-price__old-price">
							5 600 грн.
						</span>
						<span class="product-price__price">
							4 480 грн.
						</span>
					</div>
				</div>

				<div class="product__available">
					<p class="present">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
			            </svg>
						<span class="present__few">Мало: </span>
						<span class="present__lot">Много: </span>

						<!-- <span class="present__count">2 шт.</span> -->
						<span class="present__count">240 шт.</span>
					</p>
					<p class="absent">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
			            </svg>
						<span>Нет в наличии</span>
					</p>
				</div>

				<div class="product__buy-more">
					<div class="product__have">
						<div class="product__buy">
							<button class="a-style-btn">
								<span class="to-cart">
								<span>В корзину</span>
								</span>
								<span class="in-cart">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
					            </svg>
								<span>В корзине</span>
								</span>
							</button>
						</div>

						<div class="product__more">
							<button class="c-style-btn compare">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
					            </svg>
							</button>
							<button class="c-style-btn favorites">
								<svg class="to-favorites">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
					            </svg>
								<svg class="in-favorites">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
					            </svg>
							</button>
						</div>
					</div>

					<div class="product__report">
						<button class="b-style-btn js-avail-report">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
				            </svg>
							<span>Сообщить о наличии</span>
						</button>
					</div>
				</div>

				<div class="product-call">
					<div class="product-call__title">
						Быстрый заказ
					</div>
					<input type="text" class="product-call__input" id="quick-order" placeholder="Телефон">
					<button class="product-call__btn">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#phone"></use>
			            </svg>
					</button>
				</div>
			</div>

			<div class="pop-up gallery-window" id="gallery-modal">
				<div class="pop-up-bg"></div>
				<div class="pop-up-window gallery-modal-window"
				style="max-width: 480px">
					<button class="pop-up-window__close" id="gallery-modal-close">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
			            </svg>
					</button>
				</div>
			</div>

			<div class="product-detail product-section">
				<div class="product-detail__block">
					<div class="product-detail__block-header js-detail-show">
						<span>Описание</span>
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					<div class="product-detail__block-content">
						<h3 class="product-detail__block-title">
							<span class="content-title">Описание</span>  Детский спортивный комплекс для дома SportWood Plus
						</h3>
						<p>Если в вашем доме подрастает непоседливый активный малыш, детский спортивный комплекс для дома SportWood станет лучшим подарком, который вы можете для него сделать!</p>

						<p>Этот яркий, красочный, многофункциональный спортивный комплекс подарит вашему ребёнку море радости и удовольствия, сделает его досуг весёлым, интересным и захватывающим. К тому же, благодаря своей комплектации, SportWood сделает вашего малыша более сильным, ловким, гибким и смелым. Порадует родителей и оригинальная конструкция изделия, благодаря которой спортивный комплекс легко складывается и не занимает много места, так что он подойдёт и для небольшой квартиры. При этом его цена – более, чем демократичная!</p>
						<h4>Возрастная категория</h4>
						<p>Детский спортивный комплекс для дома SportWood рассчитан на детей возрастом от 1 года до 5-7 лет. Конструкция данного изделия создана с учётом всех физиологических особенностей развития детей раннего и дошкольного возраста и продумана до мелочей, включая расстояние между перекладинами. Соответствие ГОСТу Изделие разработано и выполнено ведущими инженерами компании «SportBaby» в соответствии с высокими современными европейскими стандартами и требованиям ГОСТа. Детский спортивный комплекс для дома SportWood прошёл все необходимые проверки качества и отвечает требованиям международной сертификации СЕ и TUV.</p>
						<h4>Допустимые нагрузки</h4>
						<p>Конструкция рассчитана на нагрузку до 60 килограмм.</p>
						<h4>Используемые материалы</h4>
						<p>Для изготовления детского спортивного комплекса для дома SportWood использованы только качественные сертифицированные материалы: натуральное дерево – бук, выращенный в экологически чистых областях Украины и тщательным образом обработанный, высушенный и отшлифованный на собственном производстве компании «SportBaby»; влагостойкая фанера.</p>
					</div>
				</div>

				<div class="product-detail__block">
					<div class="product-detail__block-header js-detail-show">
						<span>Характеристики</span>
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					<div class="product-detail__block-content">
						<h3 class="product-detail__block-title">
							<span class="content-title">Характеристики</span>  Детский спортивный комплекс для дома SportWood Plus
						</h3>
						<div class="prop-list">
							<div class="prop-item">
								<div class="prop-item__prop">
									Высота
								</div>
								<div class="prop-item__desc">
									130 см
								</div>
							</div>
							<div class="prop-item">
								<div class="prop-item__prop">
									Ширина
								</div>
								<div class="prop-item__desc">
									85 см
								</div>
							</div>
							<div class="prop-item">
								<div class="prop-item__prop">
									Материал
								</div>
								<div class="prop-item__desc">
									Берёза
								</div>
							</div>
							<div class="prop-item">
								<div class="prop-item__prop">
									Перекладины
								</div>
								<div class="prop-item__desc">
									Бук
								</div>
							</div>
							<div class="prop-item">
								<div class="prop-item__prop">
									Допустимые нагрузки на спортуголок
								</div>
								<div class="prop-item__desc">
									60 кг
								</div>
							</div>
							<div class="prop-item">
								<div class="prop-item__prop">
									Вид крепления
								</div>
								<div class="prop-item__desc">
									Не требует крепления
								</div>
							</div>
							<div class="prop-item">
								<div class="prop-item__prop">
									Вес товара
								</div>
								<div class="prop-item__desc">
									30 кг
								</div>
							</div>
							<div class="prop-item">
								<div class="prop-item__prop">
									Гарантия
								</div>
								<div class="prop-item__desc">
									12 месяцев
								</div>
							</div>
							<div class="prop-item">
								<div class="prop-item__prop">
									Файлы
								</div>
								<div class="prop-item__desc">
									Array
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="product-detail__block">
					<div class="product-detail__block-header js-detail-show">
						<span>Комплектация</span>
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					<div class="product-detail__block-content">
						<h3 class="product-detail__block-title">
							<span class="content-title">Комплектация</span>  Детский спортивный комплекс для дома SportWood Plus
						</h3>
						<p>Детский спортивный комплекс для дома SportWood отличается лёгкостью, компактностью, многофункциональностью и яркой расцветкой.</p>
						<h4>В его комплектацию входят такие спортивные элементы:</h4>
						<ol>
							<li>Шведская стенка</li>
							<li>Гладиаторская сетка</li>
							<li>Рукоход</li>
							<li>Детская горка</li>
							<li>Кольца</li>
						</ol>
						<p>Все эти спортивные атрибуты будут способствовать развитию координации движений малыша и его основным физическим навыкам. Кроме того, благодаря спортивным занятиям, гармонично будет развиваться нервная и сердечно-сосудистая система малыша. Обратите внимание, что ручки наверху изделия вы можете использовать как для его удобной переноски, так и для крепежа дополнительных спортивных элементов, которые вы можете приобрести отдельно.</p>
						<p><strong>Для  безопасности рекомендуем  купить мат гимнастический.</strong></p>
					</div>
				</div>

				<div class="product-detail__block">
					<div class="product-detail__block-header js-detail-show">
						<span>Видео</span>
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</div>
					<div class="product-detail__block-content">
						<h3 class="product-detail__block-title">
							<span class="content-title">Видео</span>  Детский спортивный комплекс для дома SportWood Plus
						</h3>
						<div class="gallery-bottom">
							<div class="product-detail__v-block">
								<div class="product-detail__v-content js-show-video" data-video="M661dWbPww8">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/M661dWbPww8/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/M661dWbPww8/maxresdefault.jpg" alt="">
									</picture>
								</div>
								<div class="product-detail__v-txt">
									<p>Детские площадки</p>
								</div>
							</div>
							<div class="product-detail__v-block">
								<div class="product-detail__v-content js-show-video" data-video="CITAADoErb8">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/CITAADoErb8/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/CITAADoErb8/maxresdefault.jpg" alt="">
									</picture>
								</div>
								<div class="product-detail__v-txt">
									<p>Детские площадки</p>
								</div>
							</div>
							<div class="product-detail__v-block">
								<div class="product-detail__v-content js-show-video" data-video="LQ7sHjq8rcY">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/LQ7sHjq8rcY/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/LQ7sHjq8rcY/maxresdefault.jpg" alt="">
									</picture>
								</div>
								<div class="product-detail__v-txt">
									<p>Детские площадки</p>
								</div>
							</div>
							<div class="product-detail__v-block">
								<div class="product-detail__v-content js-show-video" data-video="B6dXxqiSBSM">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/B6dXxqiSBSM/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/B6dXxqiSBSM/maxresdefault.jpg" alt="">
									</picture>
								</div>
								<div class="product-detail__v-txt">
									<p>Детские площадки</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="product-with product-section">
				<h2 class="product-section__title">С этим товаром покупают</h2>
				<div class="catalog-items">
					<div class="catalog-item catalog-item--absent">
						<div class="catalog-item__stickers item-stickers">
							<!-- <div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div> -->
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="catalog-item__img">
							<a href="#">
								<picture>
									<source
									srcset="<?=IMGS_DIR?>prod_02.webp"
									type="image/webp">

									<img
									src="<?=IMGS_DIR?>prod_02.jpg">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="#">Детский спортивный уголок «Эверест-2»</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>

								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-item">
						<div class="catalog-item__stickers item-stickers">
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div>
							<!-- <div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div> -->
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="catalog-item__img">
							<a href="#">
								<picture>
									<source
									srcset="<?=IMGS_DIR?>prod_04.webp"
									type="image/webp">

									<img
									src="<?=IMGS_DIR?>prod_04.jpg">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="#">Спортивный комплекс для дома Акварелька Plus 1-1</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-item catalog-item--discount">
						<div class="catalog-item__stickers item-stickers">
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="catalog-item__img">
							<a href="#">
								<picture>
									<source
									srcset="<?=IMGS_DIR?>prod_03.webp"
									type="image/webp">

									<img
									src="<?=IMGS_DIR?>prod_03.jpg">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="#">Детская площадка Babyland-12</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-item">
						<div class="catalog-item__stickers item-stickers">
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="catalog-item__img">
							<a href="#">
								<picture>
									<source
									srcset="<?=IMGS_DIR?>prod_05.webp"
									type="image/webp">

									<img
									src="<?=IMGS_DIR?>prod_05.jpg">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="#">Детский спортивный комплекс BambinoWood Color Plus 1-1</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-item catalog-item--absent">
						<div class="catalog-item__stickers item-stickers">
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div>
						</div>

						<div class="catalog-item__img">
							<a href="#">
								<picture>
									<source
									data-srcset="<?=IMGS_DIR?>prod_02.webp"
									type="image/webp">

									<source
									data-srcset="<?=IMGS_DIR?>prod_02.jpg"
									type="image/jpeg">

									<img
									src="<?=IMGS_DIR?>cats/noimg.png"
									class="lazyestload">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="#">Детский спортивный комплекс Bambino Wood Color Plus 1-1</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>

					<div class="catalog-item catalog-item--discount">
						<div class="catalog-item__stickers item-stickers">
							<!-- <div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--hit">
									Хит
								</span>
							</div> -->
							<div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--new">
									Новинка
								</span>
							</div>
							<!-- <div class="sticker-wrapper">
								<span class="item-stickers__elem item-stickers__elem--action">
									Акция
								</span>
							</div> -->
						</div>

						<div class="catalog-item__img">
							<a href="#">
								<picture>
									<source
									data-srcset="<?=IMGS_DIR?>prod_01.webp"
									type="image/webp">

									<source
									data-srcset="<?=IMGS_DIR?>prod_01.jpg"
									type="image/jpeg">

									<img
									src="<?=IMGS_DIR?>cats/noimg.png"
									class="lazyestload">
								</picture>
							</a>
						</div>

						<div class="catalog-item__info">
							<div class="catalog-item__title">
								<a href="#">Детский спортивный уголок для дома «Kind-Start»</a>
							</div>

							<div class="catalog-item__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span>В наличии</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="catalog-item__v-code">
								<span class="catalog-item__v-code-name">
									Артикул:
								</span>
								«<span class="catalog-item__v-code-value">
									SportWood
								</span>»
							</div>

							<div class="catalog-item__price-cont">
								<span class="catalog-item__price">
									3 969 грн.
								</span>
								<span class="catalog-item__discount">
									-10%
								</span>
								<span class="catalog-item__old-price">
									3 869 грн.
								</span>
							</div>

							<div class="catalog-item__buy-more">
								<div class="catalog-item__have">
									<div class="catalog-item__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
								            </svg>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											</p>
										</button>
									</div>
									<div class="catalog-item__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="catalog-item__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
									</button>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include MOB_TEMPLATE.'footer.php';?>