<div class="filter-block">
	<button class="filter-block__close">
		<svg class="svg-opened">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
        </svg>
	</button>
	<div class="container">

		<h4 class="filter-block__title">Фильтр</h4>
		<div class="cats-filter">
			<form action="#" id="cats-filter-form">
				<div class="cats-filter-prop cats-filter-price opened">
					<div class="cats-filter-prop__title js-cats-filter-toggler">
						<h4 class="cats-filter-prop__title-name">
							<i class="fa fa-angle-up" aria-hidden="true"></i>
							<span>Цена</span>
						</h4>
					</div>

					<div class="cats-filter-prop__cont cats-filter-price__cont">
						<div class="range-input-group">
							<input type="text" class="fx-range-from" id="range-from">
							<input type="text" class="fx-range-to" id="range-to">
							<span class="fx-range-sep">–</span>
						</div>

						<div class="fx-range-wrapper">
							<div id="slider-range"></div>
						</div>
					</div>
				</div>

				<div class="cats-filter-prop opened">
					<div class="cats-filter-prop__title js-cats-filter-toggler">
						<h4 class="cats-filter-prop__title-name">
							<i class="fa fa-angle-up" aria-hidden="true"></i>
							<span>Высота</span>
						</h4>
					</div>

					<div class="cats-filter-prop__cont">
						<div class="check-group">
							<label class="check cats-filter-check">
								<input type="checkbox" class="check__input" checked>
								<span class="check__box"></span>
								170 см
							</label>
							<label class="check cats-filter-check">
								<input type="checkbox" class="check__input">
								<span class="check__box"></span>
								150 см
							</label>
							<label class="check cats-filter-check">
								<input type="checkbox" class="check__input">
								<span class="check__box"></span>
								130 см
							</label>
						</div>
					</div>
				</div>

				<div class="cats-filter-buttons">
					<button class="a-style-btn js-do-filter" type="submit">Показать</button>
					<button class="d-style-btn js-cancel-filter" type="reset">Сбросить</button>
				</div>
			</form>
		</div>
	</div>
</div>