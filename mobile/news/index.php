<?php
include '../../paths.php';
?>

<?php include MOB_TEMPLATE.'header.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					Новости
				</h1>
				<div class="news-list__items">
					<div class="news-list-elem">
						<div class="news-list-elem__img">
							<a href="<?=MOB_SITE_DIR?>catalog">
								<picture>
									<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>news/news-img.jpg">
								</picture>
							</a>
						</div>
						<div class="news-list-elem__content">
							<p class="news-list-elem__date">01.Ноя.2018</p>
							<h2 class="news-list-elem__title">
								<a href="<?=MOB_SITE_DIR?>news/element">
									Кампания SportBaby поздравляет Вас с новым 2014 годом
								</a>
							</h2>
							<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
							<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
						</div>
					</div>

					<div class="news-list-elem">
						<div class="news-list-elem__img">
							<a href="<?=MOB_SITE_DIR?>catalog">
								<picture>
									<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>news/news-img.jpg">
								</picture>
							</a>
						</div>
						<div class="news-list-elem__content">
							<p class="news-list-elem__date">01.Ноя.2018</p>
							<h2 class="news-list-elem__title">
								<a href="<?=MOB_SITE_DIR?>news/element">
									Кампания SportBaby поздравляет Вас с новым 2014 годом
								</a>
							</h2>
							<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
							<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
						</div>
					</div>

					<div class="news-list-elem">
						<div class="news-list-elem__img">
							<a href="<?=MOB_SITE_DIR?>catalog">
								<picture>
									<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>news/news-img.jpg">
								</picture>
							</a>
						</div>
						<div class="news-list-elem__content">
							<p class="news-list-elem__date">01.Ноя.2018</p>
							<h2 class="news-list-elem__title">
								<a href="<?=MOB_SITE_DIR?>news/element">
									Кампания SportBaby поздравляет Вас с новым 2014 годом
								</a>
							</h2>
							<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
							<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
						</div>
					</div>

					<div class="news-list-elem">
						<div class="news-list-elem__img">
							<a href="<?=MOB_SITE_DIR?>catalog">
								<picture>
									<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>news/news-img.jpg">
								</picture>
							</a>
						</div>
						<div class="news-list-elem__content">
							<p class="news-list-elem__date">01.Ноя.2018</p>
							<h2 class="news-list-elem__title">
								<a href="<?=MOB_SITE_DIR?>news/element">
									Кампания SportBaby поздравляет Вас с новым 2014 годом
								</a>
							</h2>
							<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
							<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
						</div>
					</div>

					<div class="news-list-elem">
						<div class="news-list-elem__img">
							<a href="<?=MOB_SITE_DIR?>catalog">
								<picture>
									<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>news/news-img.jpg">
								</picture>
							</a>
						</div>
						<div class="news-list-elem__content">
							<p class="news-list-elem__date">01.Ноя.2018</p>
							<h2 class="news-list-elem__title">
								<a href="<?=MOB_SITE_DIR?>news/element">
									Кампания SportBaby поздравляет Вас с новым 2014 годом
								</a>
							</h2>
							<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
							<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
						</div>
					</div>

					<div class="news-list-elem">
						<div class="news-list-elem__img">
							<a href="<?=MOB_SITE_DIR?>catalog">
								<picture>
									<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>news/news-img.jpg">
								</picture>
							</a>
						</div>
						<div class="news-list-elem__content">
							<p class="news-list-elem__date">01.Ноя.2018</p>
							<h2 class="news-list-elem__title">
								<a href="<?=MOB_SITE_DIR?>news/element">
									Кампания SportBaby поздравляет Вас с новым 2014 годом
								</a>
							</h2>
							<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
							<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
						</div>
					</div>

					<div class="news-list-elem">
						<div class="news-list-elem__img">
							<a href="<?=MOB_SITE_DIR?>catalog">
								<picture>
									<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>news/news-img.jpg">
								</picture>
							</a>
						</div>
						<div class="news-list-elem__content">
							<p class="news-list-elem__date">01.Ноя.2018</p>
							<h2 class="news-list-elem__title">
								<a href="<?=MOB_SITE_DIR?>news/element">
									Кампания SportBaby поздравляет Вас с новым 2014 годом
								</a>
							</h2>
							<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
							<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
						</div>
					</div>

					<div class="news-list-elem">
						<div class="news-list-elem__img">
							<a href="<?=MOB_SITE_DIR?>catalog">
								<picture>
									<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>news/news-img.jpg">
								</picture>
							</a>
						</div>
						<div class="news-list-elem__content">
							<p class="news-list-elem__date">01.Ноя.2018</p>
							<h2 class="news-list-elem__title">
								<a href="<?=MOB_SITE_DIR?>news/element">
									Кампания SportBaby поздравляет Вас с новым 2014 годом
								</a>
							</h2>
							<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
							<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
						</div>
					</div>

					<div class="news-list-elem">
						<div class="news-list-elem__img">
							<a href="<?=MOB_SITE_DIR?>catalog">
								<picture>
									<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>news/news-img.jpg">
								</picture>
							</a>
						</div>
						<div class="news-list-elem__content">
							<p class="news-list-elem__date">01.Ноя.2018</p>
							<h2 class="news-list-elem__title">
								<a href="<?=MOB_SITE_DIR?>news/element">
									Кампания SportBaby поздравляет Вас с новым 2014 годом
								</a>
							</h2>
							<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
							<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
						</div>
					</div>

					<div class="news-list-elem">
						<div class="news-list-elem__img">
							<a href="<?=MOB_SITE_DIR?>catalog">
								<picture>
									<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>news/news-img.jpg">
								</picture>
							</a>
						</div>
						<div class="news-list-elem__content">
							<p class="news-list-elem__date">01.Ноя.2018</p>
							<h2 class="news-list-elem__title">
								<a href="<?=MOB_SITE_DIR?>news/element">
									Кампания SportBaby поздравляет Вас с новым 2014 годом
								</a>
							</h2>
							<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
							<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
						</div>
					</div>
				</div>

				<div class="news-pagination pagination-section">
					<ul class="pagination-section__items">
						<li class="active">
							<span class="nav-current-page">1</span>
						</li>
						<li>
							<a href="#?PAGEN_2=2">2</a>
						</li>
						<li>
							<a href="#?PAGEN_2=3">3</a>
						</li>
						<li class="points"><span>...</span></li>
						<li>
							<a href="#?PAGEN_2=24">26</a>
						</li>
						<li>
							<a href="#?PAGEN_2=25">25</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</div>
</div>

<?php include MOB_TEMPLATE.'footer.php';?>