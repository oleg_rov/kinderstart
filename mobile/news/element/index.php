<?php
include '../../../paths.php';
?>

<?php include MOB_TEMPLATE.'header.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					Кампания SportBaby поздравляет Вас с новым 2014 годом
				</h1>

				<div class="news-item__one">
					<div class="news-item__top">
						<span class="news-item__date">01.Ноя.2018</span>
					</div>

					<div class="news-item__img">
						<picture>
							<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>news/news-img.jpg">
						</picture>
					</div>

					<div class="news-item__content">
						<p>Дети очень подвижны и любознательны, поэтому особенно в раннем возрасте они проявляют любопытство, исследуя этот мир. Конечно же, родителей заставляет волноваться этот факт и они стараются опекать и обеспечить максимальную безопасность малышу, чтобы предотвратить негативные последствия от таких любознательных игр.</p>

						<p>Если малыша не увлечь полезной и развивающей игрушкой, он сможет самостоятельно найти себе игру или предмет, который может его очень заинтересовать, но при этом, он будет нести возможную опасность для здоровья малыша. Именно поэтому свободное время и игры ребенка необходимо тщательно спланировать и присматривать за ним, а также постараться обеспечить детям комфортную, удобную, интересную и безопасную игровую зону. Именно для этого и созданы детские игровые комплексы, которые предлагает наш интернет-магазин.</p>

						<p>Детские игровые комплексы представляют собой прочную и безопасную надежную конструкцию, которую можно установить на открытом воздухе, на дачном участке или во дворе частного или многоквартирного дома, а также на территории детских садов. Развлекательный спортивный комплекс для малышей может включать в себя множество разнообразных деталей. В зависимости от модели он может включать в себя веселые качели, на которых так любят раскачиваться малыши, некоторые конструкции детских игровых комплексов могут содержать одновременно несколько качелей, что очень удобно, если у Вас двое и больше детей захотят покататься и им не придется ждать своей очереди.</p>

						<p>Также, в комплектацию могут входить и детские песочницы, которые смогут развлекать малышей помладше, в то время как старшие детки смогут заниматься спортивными играми на детских тренажерах. Среди прочего детские игровые комплексы из натурального экологически чистого дерева могут быть дополнены альпинисткой горкой для скалолазания, гладиаторской сеткой и подвесными элементами, такими как веревка-канат, веревочная лесенка, гимнастические кольца.</p>

						<p>Также очень многие модели детских игровых комплексов от отечественного производителя торговой марки SportBaby оснащены самым любимым детским аттракционом – длинной горкой для веселых спусков. Прочность и надежность горок, которые входят в комплектацию детских комплексов, гарантирована производителем. Многие конструкции дополнены крышей с навесным тентом, который позволит создать тенек в жаркое время года и защитить малыша от воздействия прямых солнечных лучей.</p>

						<p>Детские комплексы, которые призваны развивать способности ребенка и развлекать его, изготовлены из натурального материала – древесины сосны, которая обработана антисептиком. Конструкции не содержат острых углов, заусениц в дереве и других вещей, которые могли бы представлять опасность для Вашего малыша. Покупая игровой комплекс от проверенного производителя с многолетним опытом, Вы получаете также и гарантию, которая предоставляется на двенадцать месяцев.</p>
					</div>

					<div class="news-item__author">
						Автор: «SportBaby»
					</div>
				</div>

				<div class="news-item__return">
					<a href="<?=MOB_SITE_DIR?>news/">
						<span><i class="fa fa-angle-left" aria-hidden="true"></i>Возврат к списку</span>
					</a>
				</div>

				<div class="news-item__list">
					<h2 class="news-item__list-title">
						Новости
					</h2>
					<div class="news-list__items">
						<div class="news-list-elem">
							<div class="news-list-elem__img">
								<a href="<?=MOB_SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>news/news-img.jpg">
									</picture>
								</a>
							</div>
							<div class="news-list-elem__content">
								<p class="news-list-elem__date">01.Ноя.2018</p>
								<h2 class="news-list-elem__title">
									<a href="<?=MOB_SITE_DIR?>news/element">
										Кампания SportBaby поздравляет Вас с новым 2014 годом
									</a>
								</h2>
								<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
								<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
							</div>
						</div>

						<div class="news-list-elem">
							<div class="news-list-elem__img">
								<a href="<?=MOB_SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>news/news-img.jpg">
									</picture>
								</a>
							</div>
							<div class="news-list-elem__content">
								<p class="news-list-elem__date">01.Ноя.2018</p>
								<h2 class="news-list-elem__title">
									<a href="<?=MOB_SITE_DIR?>news/element">
										Кампания SportBaby поздравляет Вас с новым 2014 годом
									</a>
								</h2>
								<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
								<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
							</div>
						</div>

						<div class="news-list-elem">
							<div class="news-list-elem__img">
								<a href="<?=MOB_SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>news/news-img.jpg">
									</picture>
								</a>
							</div>
							<div class="news-list-elem__content">
								<p class="news-list-elem__date">01.Ноя.2018</p>
								<h2 class="news-list-elem__title">
									<a href="<?=MOB_SITE_DIR?>news/element">
										Кампания SportBaby поздравляет Вас с новым 2014 годом
									</a>
								</h2>
								<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
								<a href="<?=MOB_SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
							</div>
						</div>
					</div>

					<div class="news-pagination pagination-section">
						<ul class="pagination-section__items">
							<li class="active">
								<span class="nav-current-page">1</span>
							</li>
							<li>
								<a href="#?PAGEN_2=2">2</a>
							</li>
							<li>
								<a href="#?PAGEN_2=3">3</a>
							</li>
							<li class="points"><span>...</span></li>
							<li>
								<a href="#?PAGEN_2=24">26</a>
							</li>
							<li>
								<a href="#?PAGEN_2=25">25</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include MOB_TEMPLATE.'footer.php';?>