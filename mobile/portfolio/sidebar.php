<div class="filter-block">
	<button class="filter-block__close">
		<svg class="svg-opened">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
        </svg>
	</button>
	<div class="container">
		<div class="side-menu-wrapper">
			<div class="side-menu">
				<h4 class="side-menu__title">Все категории</h4>
				<ul class="side-menu__content">

					<li class="side-menu__item side-menu__item--parent">
						<div class="side-menu__item-cont opened js-side-menu">
							<span class="side-menu__name">Спортивные уголки</span>
						</div>
						<ul class="side-menu__sub">
							<li class="side-menu__sub-item active">
								<a href="#">Детский спортивный уголок</a>
							</li>
							<li class="side-menu__sub-item">
								<a href="#">Спортивный уголок - Бук</a>
							</li>
						</ul>
					</li>

					<li class="side-menu__item side-menu__item--parent">
						<div class="side-menu__item-cont js-side-menu">
							<span class="side-menu__name">Спортивный уголок - Цветной</span>
						</div>
						<ul class="side-menu__sub">
							<li class="side-menu__sub-item">
								<a href="#">Детский спортивный уголок</a>
							</li>
							<li class="side-menu__sub-item">
								<a href="#">Спортивный уголок - Бук</a>
							</li>
						</ul>
					</li>

					<li class="side-menu__item side-menu__item--parent">
						<div class="side-menu__item-cont js-side-menu">
							<span class="side-menu__name">Спортивный уголок - Цветной</span>
						</div>
						<ul class="side-menu__sub">
							<li class="side-menu__sub-item">
								<a href="#">Детский спортивный уголок</a>
							</li>
							<li class="side-menu__sub-item">
								<a href="#">Спортивный уголок - Бук</a>
							</li>
						</ul>
					</li>

					<li class="side-menu__item side-menu__item--parent">
						<div class="side-menu__item-cont js-side-menu">
							<span class="side-menu__name">Спортивный уголок - Цветной</span>
						</div>
						<ul class="side-menu__sub">
							<li class="side-menu__sub-item">
								<a href="#">Детский спортивный уголок</a>
							</li>
							<li class="side-menu__sub-item">
								<a href="#">Спортивный уголок - Бук</a>
							</li>
						</ul>
					</li>

				</ul>
			</div>
		</div>
	</div>
</div>