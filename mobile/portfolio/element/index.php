<?php
include '../../../paths.php';
?>

<?php include MOB_TEMPLATE.'header.php';?>

<?php include '../sidebar.php';?>

<div class="page-content">
	<div class="container">
		<div class="page-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					Babyland-3
				</h1>
				<div class="side-menu-top">
					<button class="side-menu-top__btn js-cats-filters">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
			            </svg>
					</button>
				</div>
				<div class="portfolio-item__items">
					<div class="portfolio-item__item">
						<picture>
							<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_01_s.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>portfolio/slider_01/p_01_s.jpg">
						</picture>
					</div>

					<div class="portfolio-item__item">
						<picture>
							<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_02_s.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>portfolio/slider_01/p_02_s.jpg">
						</picture>
					</div>

					<div class="portfolio-item__item">
						<picture>
							<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_03_s.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>portfolio/slider_01/p_03_s.jpg">
						</picture>
					</div>

					<div class="portfolio-item__item">
						<picture>
							<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_04_s.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>portfolio/slider_01/p_04_s.jpg">
						</picture>
					</div>

					<div class="portfolio-item__item">
						<picture>
							<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_05_s.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>portfolio/slider_01/p_05_s.jpg">
						</picture>
					</div>

					<div class="portfolio-item__item">
						<picture>
							<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_06_s.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>portfolio/slider_01/p_06_s.jpg">
						</picture>
					</div>

					<div class="portfolio-item__item">
						<picture>
							<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_07_s.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>portfolio/slider_01/p_07_s.jpg">
						</picture>
					</div>

					<div class="portfolio-item__item">
						<picture>
							<source srcset="<?=IMGS_DIR?>portfolio/slider_01/p_08_s.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>portfolio/slider_01/p_08_s.jpg">
						</picture>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include MOB_TEMPLATE.'footer.php';?>