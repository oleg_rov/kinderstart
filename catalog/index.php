<?php
include '../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>

<!--<div class="page-content">-->
	<div class="container">
		<div class="page-block">

			<div class="side-block shift-side-block">
				<button class="shift-side-block__close">
					<svg class="svg-opened">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
					</svg>
				</button>

				<div class="catalog-side-wrapper">
<?php  include 'sidebar.php'; ?>
				</div>
			</div>

			<div class="main-block">
				<div class="regular-block">

				<h1 class="regular-block__header">
					Детские спортивные уголки
				</h1>
				<div class="catalog-section-top">
					<button class="catalog-section-tune-btn js-cats-filters">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#tune"></use>
			            </svg>
					</button>
<?php
$path = getPath();
$arQuery = [];
if (!empty($_GET)) {
	foreach ($_GET as $k=>$v) {
		if ($k !== 'sort' && $k !== 'order') {
			$arQuery[$k] = $v;
		}
	}
}
$path = $path . '?' . http_build_query($arQuery);

$sortName = 'По популярности';
switch($_GET['sort']) {
	case 'pop':
		$sortName = 'По популярности';
		break;
	case 'price':
		$sortName = 'По цене';
		break;
	case 'name':
		$sortName = 'По названию';
		break;
	default:
		$sortName = 'По популярности';
}
$sortOrder = 'desc';
if ($_GET['order'] == 'asc')
	$sortOrder = 'asc';
?>
					<div class="catalog-section-sort">

						<button class="catalog-section-sort__item js-sort-dir sort-<?=$sortOrder?>">
							<span class="catalog-section-sort__sort-name">
								<svg class="desc-dir">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
					            </svg>
								<svg  class="asc-dir">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
					            </svg>

								<span><?=$sortName?></span>
							</span>
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</button>

						<div class="catalog-section-sort__drop-box">
							<ul>
								<li class="catalog-section-sort__item sort-desc">
									<a href="<?=$path?>&sort=pop&order=desc">
										<div class="catalog-section-sort__sort-name">
											<svg class="desc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
								            </svg>
											<svg  class="asc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
								            </svg>
											<span>По популярности</span>
										</div>
									</a>
								</li>

								<li class="catalog-section-sort__item sort-asc">
									<a href="<?=$path?>&sort=pop&order=asc">
										<div class="catalog-section-sort__sort-name">
											<svg class="desc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
								            </svg>
											<svg  class="asc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
								            </svg>
											<span>По популярности</span>
										</div>
									</a>
								</li>

								<li class="catalog-section-sort__item sort-desc">
									<a href="<?=$path?>&sort=price&order=desc">
										<div class="catalog-section-sort__sort-name">
											<svg class="desc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
								            </svg>
											<svg  class="asc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
								            </svg>
											<span>По цене</span>
										</div>
									</a>
								</li>

								<li class="catalog-section-sort__item sort-asc">
									<a href="<?=$path?>&sort=price&order=asc">
										<div class="catalog-section-sort__sort-name">
											<svg class="desc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
								            </svg>
											<svg  class="asc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
								            </svg>
											<span>По цене</span>
										</div>
									</a>
								</li>

								<li class="catalog-section-sort__item sort-desc">
									<a href="<?=$path?>&sort=name&order=desc">
										<div class="catalog-section-sort__sort-name">
											<svg class="desc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
								            </svg>
											<svg  class="asc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
								            </svg>
											<span>По названию</span>
										</div>
									</a>
								</li>

								<li class="catalog-section-sort__item sort-asc">
									<a href="<?=$path?>&sort=name&order=asc">
										<div class="catalog-section-sort__sort-name">
											<svg class="desc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-desc"></use>
								            </svg>
											<svg  class="asc-dir">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#sort-asc"></use>
								            </svg>
											<span>По названию</span>
										</div>
									</a>
								</li>
							</ul>
						</div>
					</div>

<?php if (empty($_GET['display']) || $_GET['display'] == 'block') {
	$view = 'block';
} else {
	$view = 'list';
} ?>

					<div class="catalog-section-views">
						<a href="<?=SITE_DIR?>catalog/?display=list" class="list-view toggler-view<?if($view == 'list') echo ' active'?>">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
				            </svg>
						</a>
						<a href="<?=SITE_DIR?>catalog/?display=block" class="table-view toggler-view<?if($view == 'block') echo ' active'?>">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#table"></use>
				            </svg>
						</a>
					</div>
				</div>

				<div class="catalog-section">

<?php if ($view == 'block'): ?>
	<?php include 'views/block.php'; ?>
<?php else: ?>
	<?php include 'views/list.php'; ?>
<?php endif; ?>

					<div class="catalog-pagination pagination-section">

						<ul class="pagination-section__items">
							<li class="active">
								<span class="nav-current-page">1</span>
							</li>
							<li>
								<a href="#?PAGEN_2=2">2</a>
							</li>
							<li>
								<a href="#?PAGEN_2=3">3</a>
							</li>
							<li class="points"><span>...</span></li>
							<li>
								<a href="#?PAGEN_2=24">26</a>
							</li>
							<li>
								<a href="#?PAGEN_2=25">25</a>
							</li>
							<li>
								<a href="#?PAGEN_2=26">26</a>
							</li>
						</ul>
						<div class="pagination-section__nums">
							1 - 21 из 74
						</div>
					</div>

				</div>

				<div class="catalog-section-desc">
					<div class="catalog-section-desc__img">
						<picture>
							<source srcset="<?=IMGS_DIR?>section_img.webp" type="image/webp">
							<img src="<?=IMGS_DIR?>section_img.jpg">
						</picture>
					</div>

					<div class="catalog-section-desc__txt site-styles">
						<h2>Детский спортивный уголок для дома – место для веселых игр!</h2>
						<p>Значимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного развития. Равным образом консультация с широким активом играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Разнообразный и богатый опыт сложившаяся структура организации позволяет оценить значение дальнейших направлений развития. Равным образом консультация с широким активом представляет собой интересный эксперимент проверки новых предложений.</p>
						<p>Не следует, однако забывать, что укрепление и развитие структуры способствует подготовки и реализации системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что сложившаяся структура организации требуют определения и уточнения модели развития. Товарищи! сложившаяся структура организации способствует подготовки и реализации соответствующий условий активизации.</p>
						<h2>Как правильно выбрать спортивный детский уголок в комнату?</h2>
						<p>Ваш выбор должен основываться не столько на внешнем виде самого комплекса, сколько на потребностях вашего ребенка. Ниже приведен список того, что конкретно нужно учесть:</p>
						<ul>
							<li>Физические размеры – это важно не только в том плане, что комплекс должен «стать» в вашу квартиру, но и в том, что он должен соответствовать возрасту вашего малыша.</li>
							<li>Комплектация – в зависимости от того, для каких целей вы приобретаете спорт-уголок, нужно взвешенно подойти к выбору навесного оборудования, которое идет в комплекте, а также к возможности его докупить позднее.</li>
							<li>Материал – в нашем каталоге представлены модели из двух пород дерева: сосны и бука. Каждая из них имеет свои преимущества и недостатки, однако в любом случае это будет более безопасная, экологичная и надежная альтернатива металлическим конструкциям.</li>
						</ul>
						<p>Исходя из этих трех параметров, необходимо выбрать тот вариант комплекса, который решит список поставленных перед ним задач. Купить же детский спортивный уголок в Киеве, Харькове, Днепропетровске или любом другом городе Украины, вам помогут менеджеры интернет-магазина SportBaby.</p>
						<h2>Спортивный уголок от производителя – качество по доступной цене!</h2>
						<p>Наша компания занимает сегмент производителей недорогих, но неизменно качественных спортивных тренажеров для детей. Этот выбор мы сделали исходя из потребностей наших покупателей, с которыми имеем удовольствие работа</p>
						<p>«Просто, надежно, недорого», – вот что хотят наши клиенты! Добавив к этому «экологично», мы выпустили на рынок и предлагаем вашему вниманию недорогие спортивные уголки для детей по доступной цене. Звоните или заказывайте на сайте, мы готовы доставить вашу покупку в любую точку Украины в течение 1-2 рабочих дней! Днепропетровск, Одесса, Львов – сотни довольных клиентов уже приобрели себе товары с нашего сайта в этих городах.</p>
					</div>

					<div class="catalog-section-desc__more">
						<span>Читать полностью<i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</div>
				</div>
				</div>
			</div>

		</div>

	</div>
<!--</div>-->

<?php include MAIN_TEMPLATE.'footer.php';?>