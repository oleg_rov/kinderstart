<div class="catalog-items catalog-items--list">

	<div class="catalog-item catalog-item--discount catalog-item--few-amount">

		<div class="catalog-item__stickers item-stickers">
			<!-- <div class="sticker-wrapper">
				<span class="item-stickers__elem item-stickers__elem--hit">
					Хит
				</span>
			</div> -->
			<div class="sticker-wrapper">
				<span class="item-stickers__elem item-stickers__elem--new">
					Новинка
				</span>
			</div>
			<div class="sticker-wrapper">
				<span class="item-stickers__elem item-stickers__elem--action">
					Акция
				</span>
			</div>
		</div>

		<div class="catalog-item__img">
			<a href="<?=SITE_DIR?>catalog/element">
				<picture>
					<source srcset="<?=IMGS_DIR?>prod_02.webp" type="image/webp">
					<img src="<?=IMGS_DIR?>prod_02.jpg">
				</picture>
			</a>
		</div>

		<div class="catalog-item__col-wrapper">
			<div class="catalog-item__info">
				<div class="catalog-item__title">
					<a href="<?=SITE_DIR?>catalog/element">Детский спортивный уголок «Эверест-2»</a>
				</div>

				<div class="catalog-item__available">
					<p class="present">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
			            </svg>
						<span class="present__yes">В наличии</span>
						<span class="present__few">Мало</span>
						<span class="present__count">2 шт.</span>

						<span class="present__lot">Много</span>
						<span class="present__count">240 шт.</span>
					</p>
					<p class="absent">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
			            </svg>
						<span>Нет в наличии</span>
					</p>
				</div>

				<div class="catalog-item__v-code">
					<span class="catalog-item__v-code-name">
						Артикул:
					</span>
					«<span class="catalog-item__v-code-value">
						SportWood
					</span>»
				</div>

				<div class="catalog-item__props-list">
					<ul>
						<li class="catalog-item__props-item">
							Производитель:  <span>SportBaby</span>
						</li>
						<li class="catalog-item__props-item">
							Высота:  <span>130 см</span>
						</li>
						<li class="catalog-item__props-item">
							Ширина:  <span>85 см</span>
						</li>
						<li class="catalog-item__props-item">
							Перекладины:  <span>Бук</span>
						</li>
						<li class="catalog-item__props-item">
							Материал:  <span>Берёза</span>
						</li>
					</ul>
				</div>
			</div>

			<div class="catalog-item__sell">
				<div class="catalog-item__price-cont">
					<span class="catalog-item__price">
						3 969 грн.
					</span>
					<span class="catalog-item__discount">
						-10%
					</span>
					<span class="catalog-item__old-price">
						3 869 грн.
					</span>
				</div>

				<div class="catalog-item__buy-more">
					<div class="catalog-item__have">
						<div class="catalog-item__buy">
							<button class="a-style-btn">
								<span class="to-cart">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
					            </svg>
								<span>В корзину</span>
								</span>
								<span class="in-cart">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
					            </svg>
								<span>В корзине</span>
								</span>
							</button>
						</div>
						<div class="catalog-item__more">
							<button class="c-style-btn compare">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
					            </svg>
							</button>
							<button class="c-style-btn favorites">
								<svg class="to-favorites">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
					            </svg>
								<svg class="in-favorites">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
					            </svg>
							</button>
						</div>
					</div>
					<div class="catalog-item__report">
						<button class="b-style-btn js-avail-report">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
				            </svg>
							<span>Сообщить о наличии</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="catalog-item catalog-item--absent">
		<div class="catalog-item__stickers item-stickers">
			<div class="sticker-wrapper">
				<span class="item-stickers__elem item-stickers__elem--hit">
					Хит
				</span>
			</div>
			<div class="sticker-wrapper">
				<span class="item-stickers__elem item-stickers__elem--new">
					Новинка
				</span>
			</div>
			<div class="sticker-wrapper">
				<span class="item-stickers__elem item-stickers__elem--action">
					Акция
				</span>
			</div>
		</div>

		<div class="catalog-item__img">
			<a href="<?=SITE_DIR?>catalog/element">
				<picture>
					<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
					<img src="<?=IMGS_DIR?>prod_01.jpg">
				</picture>
			</a>
		</div>
		<div class="catalog-item__col-wrapper">
			<div class="catalog-item__info">
				<div class="catalog-item__title">
					<a href="<?=SITE_DIR?>catalog/element">Детский спортивный уголок для дома «Kind-Start»</a>
				</div>

				<div class="catalog-item__available">
					<p class="present">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
			            </svg>
						<span class="present__yes">В наличии</span>
						<span class="present__few">Мало</span>
						<span class="present__count">2 шт.</span>

						<span class="present__lot">Много</span>
						<span class="present__count">240 шт.</span>
					</p>
					<p class="absent">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
			            </svg>
						<span>Нет в наличии</span>
					</p>
				</div>

				<div class="catalog-item__v-code">
					<span class="catalog-item__v-code-name">
						Артикул:
					</span>
					«<span class="catalog-item__v-code-value">
						SportWood
					</span>»
				</div>

				<div class="catalog-item__props-list">
					<ul>
						<li class="catalog-item__props-item">
							Производитель:  <span>SportBaby</span>
						</li>
						<li class="catalog-item__props-item">
							Высота:  <span>130 см</span>
						</li>
						<li class="catalog-item__props-item">
							Ширина:  <span>85 см</span>
						</li>
						<li class="catalog-item__props-item">
							Перекладины:  <span>Бук</span>
						</li>
						<li class="catalog-item__props-item">
							Материал:  <span>Берёза</span>
						</li>
					</ul>
				</div>
			</div>

			<div class="catalog-item__sell">
				<div class="catalog-item__price-cont">
					<span class="catalog-item__price">
						3 149 грн.
					</span>
					<span class="catalog-item__discount">
					</span>
					<span class="catalog-item__old-price">
					</span>
				</div>

				<div class="catalog-item__buy-more">
					<div class="catalog-item__have">
						<div class="catalog-item__buy">
							<button class="a-style-btn">
								<span class="to-cart">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
					            </svg>
								<span>В корзину</span>
								</span>
								<span class="in-cart">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
					            </svg>
								<span>В корзине</span>
								</span>
							</button>
						</div>
						<div class="catalog-item__more">
							<button class="c-style-btn compare">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
					            </svg>
							</button>
							<button class="c-style-btn favorites">
								<svg class="to-favorites">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
					            </svg>
								<svg class="in-favorites">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
					            </svg>
							</button>
						</div>
					</div>
					<div class="catalog-item__report">
						<button class="b-style-btn js-avail-report">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
				            </svg>
							<span>Сообщить о наличии</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="catalog-item catalog-item--discount catalog-item--lot-amount">
		<div class="catalog-item__stickers item-stickers">
			<div class="sticker-wrapper">
				<span class="item-stickers__elem item-stickers__elem--hit">
					Хит
				</span>
			</div>
			<div class="sticker-wrapper">
				<span class="item-stickers__elem item-stickers__elem--new">
					Новинка
				</span>
			</div>
			<div class="sticker-wrapper">
				<span class="item-stickers__elem item-stickers__elem--action">
					Акция
				</span>
			</div>
		</div>

		<div class="catalog-item__img">
			<a href="<?=SITE_DIR?>catalog/element">
				<picture>
					<source srcset="<?=IMGS_DIR?>prod_03.webp" type="image/webp">
					<img src="<?=IMGS_DIR?>prod_01.jpg">
				</picture>
			</a>
		</div>
		<div class="catalog-item__col-wrapper">
			<div class="catalog-item__info">
				<div class="catalog-item__title">
					<a href="<?=SITE_DIR?>catalog/element">Спортивный комплекс для дома Акварелька Plus 1-1</a>
				</div>

				<div class="catalog-item__available">
					<p class="present">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
			            </svg>
						<span class="present__yes">В наличии</span>
						<span class="present__few">Мало</span>
						<span class="present__count">2 шт.</span>

						<span class="present__lot">Много</span>
						<span class="present__count">240 шт.</span>
					</p>
					<p class="absent">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
			            </svg>
						<span>Нет в наличии</span>
					</p>
				</div>

				<div class="catalog-item__v-code">
					<span class="catalog-item__v-code-name">
						Артикул:
					</span>
					«<span class="catalog-item__v-code-value">
						SportWood
					</span>»
				</div>

				<div class="catalog-item__props-list">
					<ul>
						<li class="catalog-item__props-item">
							Производитель:  <span>SportBaby</span>
						</li>
						<li class="catalog-item__props-item">
							Высота:  <span>130 см</span>
						</li>
						<li class="catalog-item__props-item">
							Ширина:  <span>85 см</span>
						</li>
						<li class="catalog-item__props-item">
							Перекладины:  <span>Бук</span>
						</li>
						<li class="catalog-item__props-item">
							Материал:  <span>Берёза</span>
						</li>
					</ul>
				</div>
			</div>

			<div class="catalog-item__sell">
				<div class="catalog-item__price-cont">
					<span class="catalog-item__price">
						4 480 грн.
					</span>
					<span class="catalog-item__discount">
						-20%
					</span>
					<span class="catalog-item__old-price">
						5 600 грн.
					</span>
				</div>

				<div class="catalog-item__buy-more">
					<div class="catalog-item__have">
						<div class="catalog-item__buy">
							<button class="a-style-btn">
								<span class="to-cart">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
					            </svg>
								<span>В корзину</span>
								</span>
								<span class="in-cart">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
					            </svg>
								<span>В корзине</span>
								</span>
							</button>
						</div>
						<div class="catalog-item__more">
							<button class="c-style-btn compare">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
					            </svg>
							</button>
							<button class="c-style-btn favorites">
								<svg class="to-favorites">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
					            </svg>
								<svg class="in-favorites">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
					            </svg>
							</button>
						</div>
					</div>
					<div class="catalog-item__report">
						<button class="b-style-btn js-avail-report">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
				            </svg>
							<span>Сообщить о наличии</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>