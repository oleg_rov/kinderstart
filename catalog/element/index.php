<?php
include '../../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>

<!--<div class="page-content">-->
	<div class="container">
		<div class="page-block">
<!--
.product-item--lot - много
.product-item--few - мало
.product-item--absent - Нет в наличии
-->
			<div class="product-main elem-block__section product-item--lot">

				<div class="product-gallery-wrapper">
					<div class="product-gallery">
						<div class="swiper-container gallery-thumbs">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<picture>
										<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>prod_01.jpg">
									</picture>
								</div>
								<div class="swiper-slide" style="background-color: #ffffaa"></div>
								<div class="swiper-slide" style="background-color: #aaffaa"></div>
								<div class="swiper-slide" style="background-color: #ffcccc"></div>
								<div class="swiper-slide video-slide" style="background-color: #f5f5f5"></div>
								<div class="swiper-slide video-slide" style="background-color: #f5f5f5"></div>
								<div class="swiper-slide video-slide" style="background-color: #f5f5f5"></div>
							</div>
						</div>

						<div class="product-gallery-next"></div>
						<div class="product-gallery-prev"></div>


						<div class="product-gallery__pagination"></div>

						<div class="swiper-container gallery-top">
							<div class="catalog-item__stickers item-stickers">
								<div class="sticker-wrapper">
									<span class="item-stickers__elem item-stickers__elem--hit">
										Хит
									</span>
								</div>
								<div class="sticker-wrapper">
									<span class="item-stickers__elem item-stickers__elem--new">
										Новинка
									</span>
								</div>
								<div class="sticker-wrapper">
									<span class="item-stickers__elem item-stickers__elem--action">
										Акция
									</span>
								</div>
							</div>

							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<picture>
										<source srcset="<?=IMGS_DIR?>products/product_01.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>products/product_01.jpg">
									</picture>
								</div>
								<div class="swiper-slide" style="background-color: #ffffaa"></div>
								<div class="swiper-slide" style="background-color: #aaffaa"></div>
								<div class="swiper-slide" style="background-color: #ffcccc"></div>
								<div class="swiper-slide js-show-video video-slide"
								style="background-color: #f5f5f5" data-video="Db4K5V01q5c">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/Db4K5V01q5c/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/Db4K5V01q5c/maxresdefault.jpg" alt="">
									</picture>
								</div>
								<div class="swiper-slide js-show-video video-slide"
								style="background-color: #f5f5f5" data-video="ENrxWQKYI1I">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/ENrxWQKYI1I/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/ENrxWQKYI1I/maxresdefault.jpg" alt="">
									</picture>
								</div>
								<div class="swiper-slide js-show-video video-slide" style="background-color: #bbbbff" data-video="ciK7p4khIIM">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/ciK7p4khIIM/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/ciK7p4khIIM/maxresdefault.jpg" alt="">
									</picture>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="pop-up gallery-window" id="gallery-modal">
					<div class="pop-up-bg"></div>
					<div class="pop-up-window gallery-modal-window">
						<button class="pop-up-window__close" id="gallery-modal-close">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
				            </svg>
						</button>

					</div>
				</div>

				<div class="outer-wrapper">
					<div class="title-wrapper">
						<h1 class="product-title">
							Детский спортивный уголок «Эверест-2»
						</h1>

						<div class="product-v-code">
							<span>Артикул: «Kind-Start»</span>
						</div>
					</div>

					<div class="description-wrapper">
						<div class="inner-wrapper inner-wrapper--top">
							<div class="contacts-block">
								<h4 class="contacts-block__header">
									Нужна помощь в выборе?
								</h4>
								<ul class="contacts-block__phones">
									<li class="contacts-block__phone">
										<span class="phone-icon">
											<svg class="phone-fill">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#phone"></use>
								            </svg>
							        	</span>
										<span class="phone-code">(044)</span> 360-64-56
									</li>
									<li class="contacts-block__phone">
										<span class="phone-icon">
											<svg class="kievstar-fill">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#kievstar"></use>
								            </svg>
							        	</span>
										<span class="phone-code">(067)</span> 506-42-09
									</li>
									<li class="contacts-block__phone">
										<span class="phone-icon">
											<svg class="vodafone-fill">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#vodafone"></use>
								            </svg>
							        	</span>
										<span class="phone-code">(050)</span> 666-02-12
									</li>
								</ul>
								<div class="contacts-block__schedule">
									<span>Пн — Пт с 9 до 21</span>
								</div>
							</div>

							<div class="main-desc-block">
								<div class="main-desc-block-inner">

									<div class="product-with series-block">
										<div class="section-header">
											<h2 class="product-with__title">
												Товары серии
											</h2>
											<div class="swiper-buttons">
							    				<div class="prev-series prev-slide">
							    					<i class="fa fa-angle-left" aria-hidden="true"></i>
							    				</div>

												<div class="next-series next-slide">
													<i class="fa fa-angle-right" aria-hidden="true"></i>
												</div>
											</div>
										</div>

										<div class="product-series swiper-container">
											<div class="swiper-wrapper">
												<div class="catalog-item swiper-slide">
													<div class="catalog-item-wrapper">
														<div class="catalog-item__img">
															<a href="#">
																<picture>
																	<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
																	<img src="<?=IMGS_DIR?>prod_01.jpg">
																</picture>
															</a>
														</div>

														<div class="catalog-item__title">
															<a href="#">Детский спортивный уголок для дома «Kind-Start»</a>
														</div>

														<div class="catalog-item__available">
															<p class="present">
																<svg>
													                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
													            </svg>
																<span>В наличии</span>
															</p>
															<p class="absent">
																<svg>
													                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
													            </svg>
																<span>Нет в наличии</span>
															</p>
														</div>

														<div class="catalog-item__price-cont">
															<span class="catalog-item__price">
																3 149 грн.
															</span>
														</div>
													</div>
												</div>
												<div class="catalog-item swiper-slide">
													<div class="catalog-item-wrapper">
														<div class="catalog-item__img">
															<a href="#">
																<picture>
																	<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
																	<img src="<?=IMGS_DIR?>prod_01.jpg">
																</picture>
															</a>
														</div>

														<div class="catalog-item__title">
															<a href="#">Детский спортивный уголок для дома «Kind-Start»</a>
														</div>

														<div class="catalog-item__available">
															<p class="present">
																<svg>
													                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
													            </svg>
																<span>В наличии</span>
															</p>
															<p class="absent">
																<svg>
													                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
													            </svg>
																<span>Нет в наличии</span>
															</p>
														</div>

														<div class="catalog-item__price-cont">
															<span class="catalog-item__price">
																3 149 грн.
															</span>
														</div>
													</div>
												</div>
												<div class="catalog-item swiper-slide">
													<div class="catalog-item-wrapper">
														<div class="catalog-item__img">
															<a href="#">
																<picture>
																	<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
																	<img src="<?=IMGS_DIR?>prod_01.jpg">
																</picture>
															</a>
														</div>

														<div class="catalog-item__title">
															<a href="#">Детский спортивный уголок для дома «Kind-Start»</a>
														</div>

														<div class="catalog-item__available">
															<p class="present">
																<svg>
													                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
													            </svg>
																<span>В наличии</span>
															</p>
															<p class="absent">
																<svg>
													                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
													            </svg>
																<span>Нет в наличии</span>
															</p>
														</div>

														<div class="catalog-item__price-cont">
															<span class="catalog-item__price">
																3 149 грн.
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--
								<div class="product-excerpt">
									<div class="product-excerpt__txt">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ratione id officia voluptate soluta vel ipsum autem fugiat eum sapiente, natus dignissimos cupiditate voluptas quos asperiores repellat perferendis, enim aut eligendi quo doloremque nobis fuga labore, delectus reiciendis!
									</div>
									<div class="product-excerpt__more">
										<span>Подробнее<i class="fa fa-angle-right" aria-hidden="true"></i></span>
									</div>
								</div>

								<div class="product-upload">
									<div class="product-upload__title">
										Инструкции
									</div>
									<a href="<?=IMGS_DIR?>prod_01.jpg" class="product-upload__link" download>
										<svg  class="asc-dir">
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#get-file"></use>
							            </svg>
							            <span>«Kind-Start»</span>
									</a>
								</div>
								-->
							</div>
						</div>

						<div class="inner-wrapper inner-wrapper--bottom">
							<div class="product-sale">
								<div class="product-amount">
									<input type="text" class="product-amount__input" value="1">
									<button class="product-amount__btn js-amount-plus">+</button>
									<span class="product-amount__units">шт</span>
								</div>

								<div class="product-price">
									<span class="product-price__old-price">
										5 600 грн.
									</span>
									<span class="product-price__price">
										4 480 грн.
									</span>
								</div>
							</div>

							<div class="product__available">
								<p class="present">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
						            </svg>
									<span class="present__few">Мало: </span>
									<span class="present__lot">Много: </span>

									<!-- <span class="present__count">2 шт.</span> -->
									<span class="present__count">240 шт.</span>
								</p>
								<p class="absent">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
						            </svg>
									<span>Нет в наличии</span>
								</p>
							</div>

							<div class="product__buy-more">
								<div class="product__have">
									<div class="product__buy">
										<button class="a-style-btn">
											<p class="to-cart">
											<span>В корзину</span>
											</p>
											<p class="in-cart">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
								            </svg>
											<span>В корзине</span>
											</p>
										</button>
									</div>
									<div class="product__more">
										<button class="c-style-btn compare">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
								            </svg>
										</button>
										<button class="c-style-btn favorites">
											<svg class="to-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
								            </svg>
											<svg class="in-favorites">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
								            </svg>
										</button>
									</div>
								</div>
								<div class="product__report">
									<button class="b-style-btn js-avail-report">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
							            </svg>
										<span>Сообщить о наличии</span>
									</button>
								</div>
							</div>

							<div class="product-call">
								<div class="product-call__title">
									Быстрый заказ
								</div>
								<input type="text" class="product-call__input" id="quick-order" placeholder="Телефон">
								<button class="product-call__btn">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#phone"></use>
						            </svg>
								</button>
							</div>

							<div class="contacts-block">
								<h4 class="contacts-block__header">
									Нужна помощь в выборе?
								</h4>
								<ul class="contacts-block__phones">
									<li class="contacts-block__phone">
										<span class="phone-icon">
											<svg class="phone-fill">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#phone"></use>
								            </svg>
							        	</span>
										<span class="phone-code">(044)</span> 360-64-56
									</li>
									<li class="contacts-block__phone">
										<span class="phone-icon">
											<svg class="kievstar-fill">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#kievstar"></use>
								            </svg>
							        	</span>
										<span class="phone-code">(067)</span> 506-42-09
									</li>
									<li class="contacts-block__phone">
										<span class="phone-icon">
											<svg class="vodafone-fill">
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#vodafone"></use>
								            </svg>
							        	</span>
										<span class="phone-code">(050)</span> 666-02-12
									</li>
								</ul>
								<div class="contacts-block__schedule">
									<span>Пн — Пт с 9 до 21</span>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="product-detail elem-block__section">
				<div class="product-tabs-block">
					<ul class="product-detail__tabs">
						<li class="product-detail__tab js-detail active" data-content="first-tab">
							<span>Описание</span>
						</li>
						<li class="product-detail__tab js-detail" data-content="second-tab">
							<span>Характеристики</span>
						</li>
						<li class="product-detail__tab js-detail" data-content="third-tab">
							<span>Комплектация</span>
						</li>
						<li class="product-detail__tab js-detail" data-content="forth-tab">
							<span>Видео</span>
						</li>
					</ul>
				</div>

				<div class="product-detail__content">
					<div id="first-tab" class="product-detail__elem shown">
						<h3 class="product-detail__title">
							<span class="tab-title">Описание</span>  Детский спортивный комплекс для дома SportWood Plus
						</h3>
						<p>Если в вашем доме подрастает непоседливый активный малыш, детский спортивный комплекс для дома SportWood станет лучшим подарком, который вы можете для него сделать!</p>

						<p>Этот яркий, красочный, многофункциональный спортивный комплекс подарит вашему ребёнку море радости и удовольствия, сделает его досуг весёлым, интересным и захватывающим. К тому же, благодаря своей комплектации, SportWood сделает вашего малыша более сильным, ловким, гибким и смелым. Порадует родителей и оригинальная конструкция изделия, благодаря которой спортивный комплекс легко складывается и не занимает много места, так что он подойдёт и для небольшой квартиры. При этом его цена – более, чем демократичная!</p>
						<h4>Возрастная категория</h4>
						<p>Детский спортивный комплекс для дома SportWood рассчитан на детей возрастом от 1 года до 5-7 лет. Конструкция данного изделия создана с учётом всех физиологических особенностей развития детей раннего и дошкольного возраста и продумана до мелочей, включая расстояние между перекладинами. Соответствие ГОСТу Изделие разработано и выполнено ведущими инженерами компании «SportBaby» в соответствии с высокими современными европейскими стандартами и требованиям ГОСТа. Детский спортивный комплекс для дома SportWood прошёл все необходимые проверки качества и отвечает требованиям международной сертификации СЕ и TUV.</p>
						<h4>Допустимые нагрузки</h4>
						<p>Конструкция рассчитана на нагрузку до 60 килограмм.</p>
						<h4>Используемые материалы</h4>
						<p>Для изготовления детского спортивного комплекса для дома SportWood использованы только качественные сертифицированные материалы: натуральное дерево – бук, выращенный в экологически чистых областях Украины и тщательным образом обработанный, высушенный и отшлифованный на собственном производстве компании «SportBaby»; влагостойкая фанера.</p>
					</div>

					<div id="second-tab" class="product-detail__elem">
						<h3 class="product-detail__title">
							<span class="tab-title">Характеристики</span>  Детский спортивный комплекс для дома SportWood Plus
						</h3>
						<table class="product-detail__table">
							<tr>
								<td>Высота</td>
								<td>130 см</td>
							</tr>
							<tr>
								<td>Ширина</td>
								<td>85 см</td>
							</tr>
							<tr>
								<td>Длинна</td>
								<td>132 см</td>
							</tr>
							<tr>
								<td>Материал</td>
								<td>Берёза</td>
							</tr>
							<tr>
								<td>Перекладины</td>
								<td>Бук</td>
							</tr>
							<tr>
								<td>Допустимые нагрузки на спортуголок</td>
								<td>60 кг</td>
							</tr>
							<tr>
								<td>Вид крепления</td>
								<td>Не требует крепления</td>
							</tr>
							<tr>
								<td>Вес товара</td>
								<td>30 кг</td>
							</tr>
							<tr>
								<td>Гарантия</td>
								<td>12 месяцев</td>
							</tr>
							<tr>
								<td>Файлы</td>
								<td>Array</td>
							</tr>
							<tr>
								<td>Цвет</td>
								<td>Цветной</td>
							</tr>
							<tr>
								<td>Длинна горки</td>
								<td>150 см</td>
							</tr>
							<tr>
								<td>Производитель</td>
								<td>SportBaby</td>
							</tr>
						</table>
					</div>

					<div id="third-tab" class="product-detail__elem">
						<h3 class="product-detail__title">
							<span class="tab-title">Комплектация</span>  Детский спортивный комплекс для дома SportWood Plus
						</h3>
						<p>Детский спортивный комплекс для дома SportWood отличается лёгкостью, компактностью, многофункциональностью и яркой расцветкой.</p>
						<h4>В его комплектацию входят такие спортивные элементы:</h4>
						<ol>
							<li>Шведская стенка</li>
							<li>Гладиаторская сетка</li>
							<li>Рукоход</li>
							<li>Детская горка</li>
							<li>Кольца</li>
						</ol>
						<p>Все эти спортивные атрибуты будут способствовать развитию координации движений малыша и его основным физическим навыкам. Кроме того, благодаря спортивным занятиям, гармонично будет развиваться нервная и сердечно-сосудистая система малыша. Обратите внимание, что ручки наверху изделия вы можете использовать как для его удобной переноски, так и для крепежа дополнительных спортивных элементов, которые вы можете приобрести отдельно.</p>
						<p><strong>Для  безопасности рекомендуем  купить мат гимнастический.</strong></p>

						<div class="product-upload">
							<div class="product-upload__title">
								Инструкции
							</div>
							<a href="<?=IMGS_DIR?>prod_01.jpg" class="product-upload__link" download>
								<svg  class="asc-dir">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#get-file"></use>
					            </svg>
					            <span>«Kind-Start»</span>
							</a>
						</div>
					</div>

					<div id="forth-tab" class="product-detail__elem">
						<h3 class="product-detail__title">
							<span class="tab-title">Видео</span>  Детский спортивный комплекс для дома SportWood Plus
						</h3>
						<div class="product-detail__wrapper gallery-bottom">
							<div class="product-detail__v-block">
								<div class="product-detail__v-content js-show-video" data-video="M661dWbPww8">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/M661dWbPww8/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/M661dWbPww8/maxresdefault.jpg" alt="">
									</picture>
								</div>
								<div class="product-detail__v-txt">
									<p>Детские площадки</p>
								</div>
							</div>

							<div class="product-detail__v-block">
								<div class="product-detail__v-content js-show-video" data-video="CITAADoErb8">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/CITAADoErb8/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/CITAADoErb8/maxresdefault.jpg" alt="">
									</picture>
								</div>
								<div class="product-detail__v-txt">
									<p>Детские площадки</p>
								</div>
							</div>

							<div class="product-detail__v-block">
								<div class="product-detail__v-content js-show-video" data-video="LQ7sHjq8rcY">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/LQ7sHjq8rcY/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/LQ7sHjq8rcY/maxresdefault.jpg" alt="">
									</picture>
								</div>
								<div class="product-detail__v-txt">
									<p>Детские площадки</p>
								</div>
							</div>

							<div class="product-detail__v-block">
								<div class="product-detail__v-content js-show-video" data-video="B6dXxqiSBSM">
									<picture>
										<source srcset="https://i.ytimg.com/vi_webp/B6dXxqiSBSM/maxresdefault.webp" type="image/webp">
										<img class="video__media" src="https://i.ytimg.com/vi/B6dXxqiSBSM/maxresdefault.jpg" alt="">
									</picture>
								</div>
								<div class="product-detail__v-txt">
									<p>Детские площадки</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="product-with elem-block__section">
				<div class="section-header">
					<h2 class="product-with__title">С этим товаром покупают</h2>
					<div class="swiper-buttons">
	    				<div class="prev-with prev-slide">
	    					<i class="fa fa-angle-left" aria-hidden="true"></i>
	    				</div>
						<div class="next-with next-slide">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</div>
					</div>
				</div>

				<div class="product-with-items swiper-container">
					<div class="swiper-wrapper">
						<div class="catalog-item swiper-slide catalog-item--absent">
							<div class="catalog-item-wrapper">

								<div class="catalog-item__stickers item-stickers">
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--hit">
											Хит
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--new">
											Новинка
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--action">
											Акция
										</span>
									</div>
								</div>

								<div class="catalog-item__img">
									<a href="#">
										<picture>
											<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>prod_01.jpg">
										</picture>
									</a>
								</div>
								<div class="catalog-item__title">
									<a href="#">Детский спортивный уголок для дома «Kind-Start»</a>
								</div>
								<div class="catalog-item__available">
									<p class="present">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
							            </svg>
										<span>В наличии</span>
									</p>
									<p class="absent">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
										<span>Нет в наличии</span>
									</p>
								</div>

								<div class="catalog-item__v-code">
									<span class="catalog-item__v-code-name">
										Артикул:
									</span>
									«<span class="catalog-item__v-code-value">
										SportWood
									</span>»
								</div>

								<div class="catalog-item__price-cont">
									<span class="catalog-item__price">
										3 149 грн.
									</span>
									<span class="catalog-item__discount">
									</span>
									<span class="catalog-item__old-price">
									</span>
								</div>
								<div class="catalog-item__buy-more">
									<div class="catalog-item__have">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
									            </svg>
												<span>В корзину</span>
												</p>
												<p class="in-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
									            </svg>
												<span>В корзине</span>
												</p>
											</button>
										</div>
										<div class="catalog-item__more">
											<button class="c-style-btn compare">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
									            </svg>
											</button>
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
									<div class="catalog-item__report">
										<button class="b-style-btn js-avail-report">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
								            </svg>
											<span>Сообщить о наличии</span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="catalog-item swiper-slide catalog-item--discount">
							<div class="catalog-item-wrapper">

								<div class="catalog-item__stickers item-stickers">
									<!-- <div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--hit">
											Хит
										</span>
									</div> -->
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--new">
											Новинка
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--action">
											Акция
										</span>
									</div>
								</div>

								<div class="catalog-item__img">
									<a href="#">
										<picture>
											<source srcset="<?=IMGS_DIR?>prod_02.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>prod_02.jpg">
										</picture>
									</a>
								</div>
								<div class="catalog-item__title">
									<a href="#">Детский спортивный уголок «Эверест-2»</a>
								</div>
								<div class="catalog-item__available">
									<p class="present">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
							            </svg>
										<span>В наличии</span>
									</p>
									<p class="absent">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
										<span>Нет в наличии</span>
									</p>
								</div>

								<div class="catalog-item__v-code">
									<span class="catalog-item__v-code-name">
										Артикул:
									</span>
									«<span class="catalog-item__v-code-value">
										SportWood
									</span>»
								</div>

								<div class="catalog-item__price-cont">
									<span class="catalog-item__price">
										3 969 грн.
									</span>
									<span class="catalog-item__discount">
										-10%
									</span>
									<span class="catalog-item__old-price">
										3 869 грн.
									</span>
								</div>
								<div class="catalog-item__buy-more">
									<div class="catalog-item__have">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
									            </svg>
												<span>В корзину</span>
												</p>
												<p class="in-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
									            </svg>
												<span>В корзине</span>
												</p>
											</button>
										</div>
										<div class="catalog-item__more">
											<button class="c-style-btn compare">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
									            </svg>
											</button>
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
									<div class="catalog-item__report">
										<button class="b-style-btn js-avail-report">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
								            </svg>
											<span>Сообщить о наличии</span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="catalog-item swiper-slide catalog-item--absent">
							<div class="catalog-item-wrapper">
								<div class="catalog-item__stickers item-stickers">
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--hit">
											Хит
										</span>
									</div>
									<!-- <div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--new">
											Новинка
										</span>
									</div> -->
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--action">
											Акция
										</span>
									</div>
								</div>

								<div class="catalog-item__img">
									<a href="#">
										<picture>
											<source srcset="<?=IMGS_DIR?>prod_03.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>prod_03.jpg">
										</picture>
									</a>
								</div>
								<div class="catalog-item__title">
									<a href="#">Детская площадка Babyland-12</a>
								</div>
								<div class="catalog-item__available">
									<p class="present">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
							            </svg>
										<span>В наличии</span>
									</p>
									<p class="absent">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
										<span>Нет в наличии</span>
									</p>
								</div>

								<div class="catalog-item__v-code">
									<span class="catalog-item__v-code-name">
										Артикул:
									</span>
									«<span class="catalog-item__v-code-value">
										SportWood
									</span>»
								</div>

								<div class="catalog-item__price-cont">
									<span class="catalog-item__price">
										32 924 грн.
									</span>
									<span class="catalog-item__discount">

									</span>
									<span class="catalog-item__old-price">

									</span>
								</div>
								<div class="catalog-item__buy-more">
									<div class="catalog-item__have">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<span>В корзину</span>
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
									            </svg>
											</button>
										</div>
										<div class="catalog-item__more">
											<button class="c-style-btn compare">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
									            </svg>
											</button>
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
									<div class="catalog-item__report">
										<button class="b-style-btn js-avail-report">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
								            </svg>
											<span>Сообщить о наличии</span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="catalog-item swiper-slide">
							<div class="catalog-item-wrapper">
								<div class="catalog-item__stickers item-stickers">
									<!-- <div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--hit">
											Хит
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--new">
											Новинка
										</span>
									</div> -->
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--action">
											Акция
										</span>
									</div>
								</div>

								<div class="catalog-item__img">
									<a href="#">
										<picture>
											<source srcset="<?=IMGS_DIR?>prod_04.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>prod_04.jpg">
										</picture>
									</a>
								</div>
								<div class="catalog-item__title">
									<a href="#">Спортивный комплекс для дома Акварелька Plus 1-1</a>
								</div>
								<div class="catalog-item__available">
									<p class="present">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
							            </svg>
										<span>В наличии</span>
									</p>
									<p class="absent">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
										<span>Нет в наличии</span>
									</p>
								</div>

								<div class="catalog-item__v-code">
									<span class="catalog-item__v-code-name">
										Артикул:
									</span>
									«<span class="catalog-item__v-code-value">
										SportWood
									</span>»
								</div>

								<div class="catalog-item__price-cont">
									<span class="catalog-item__price">
										3 149 грн.
									</span>
									<span class="catalog-item__discount">
									</span>
									<span class="catalog-item__old-price">
									</span>
								</div>
								<div class="catalog-item__buy-more">
									<div class="catalog-item__have">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
									            </svg>
												<span>В корзину</span>
												</p>
												<p class="in-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
									            </svg>
												<span>В корзине</span>
												</p>
											</button>
										</div>
										<div class="catalog-item__more">
											<button class="c-style-btn compare">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
									            </svg>
											</button>
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
									<div class="catalog-item__report">
										<button class="b-style-btn js-avail-report">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
								            </svg>
											<span>Сообщить о наличии</span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="catalog-item swiper-slide catalog-item--discount">
							<div class="catalog-item-wrapper">
								<div class="catalog-item__stickers item-stickers">
									<!-- <div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--hit">
											Хит
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--new">
											Новинка
										</span>
									</div> -->
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--action">
											Акция
										</span>
									</div>
								</div>

								<div class="catalog-item__img">
									<a href="#">
										<picture>
											<source srcset="<?=IMGS_DIR?>prod_05.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>prod_05.jpg">
										</picture>
									</a>
								</div>
								<div class="catalog-item__title">
									<a href="#">Детский спортивный комплекс BambinoWood Color Plus 1-1</a>
								</div>
								<div class="catalog-item__available">
									<p class="present">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
							            </svg>
										<span>В наличии</span>
									</p>
									<p class="absent">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
										<span>Нет в наличии</span>
									</p>
								</div>

								<div class="catalog-item__v-code">
									<span class="catalog-item__v-code-name">
										Артикул:
									</span>
									«<span class="catalog-item__v-code-value">
										SportWood
									</span>»
								</div>

								<div class="catalog-item__price-cont">
									<span class="catalog-item__price">
										4 480 грн.
									</span>
									<span class="catalog-item__discount">
										-20%
									</span>
									<span class="catalog-item__old-price">
										5 600 грн.
									</span>
								</div>
								<div class="catalog-item__buy-more">
									<div class="catalog-item__have">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
									            </svg>
												<span>В корзину</span>
												</p>
												<p class="in-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
									            </svg>
												<span>В корзине</span>
												</p>
											</button>
										</div>
										<div class="catalog-item__more">
											<button class="c-style-btn compare">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
									            </svg>
											</button>
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
									<div class="catalog-item__report">
										<button class="b-style-btn js-avail-report">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
								            </svg>
											<span>Сообщить о наличии</span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="catalog-item swiper-slide catalog-item--absent">
							<div class="catalog-item-wrapper">

								<div class="catalog-item__stickers item-stickers">
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--hit">
											Хит
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--new">
											Новинка
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--action">
											Акция
										</span>
									</div>
								</div>

								<div class="catalog-item__img">
									<a href="#">
										<picture>
											<source srcset="<?=IMGS_DIR?>prod_01.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>prod_01.jpg">
										</picture>
									</a>
								</div>
								<div class="catalog-item__title">
									<a href="#">Детский спортивный уголок для дома «Kind-Start»</a>
								</div>
								<div class="catalog-item__available">
									<p class="present">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
							            </svg>
										<span>В наличии</span>
									</p>
									<p class="absent">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
										<span>Нет в наличии</span>
									</p>
								</div>

								<div class="catalog-item__v-code">
									<span class="catalog-item__v-code-name">
										Артикул:
									</span>
									«<span class="catalog-item__v-code-value">
										SportWood
									</span>»
								</div>

								<div class="catalog-item__price-cont">
									<span class="catalog-item__price">
										3 149 грн.
									</span>
									<span class="catalog-item__discount">
									</span>
									<span class="catalog-item__old-price">
									</span>
								</div>
								<div class="catalog-item__buy-more">
									<div class="catalog-item__have">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
									            </svg>
												<span>В корзину</span>
												</p>
												<p class="in-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
									            </svg>
												<span>В корзине</span>
												</p>
											</button>
										</div>
										<div class="catalog-item__more">
											<button class="c-style-btn compare">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
									            </svg>
											</button>
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
									<div class="catalog-item__report">
										<button class="b-style-btn js-avail-report">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
								            </svg>
											<span>Сообщить о наличии</span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="catalog-item swiper-slide catalog-item--discount">
							<div class="catalog-item-wrapper">

								<div class="catalog-item__stickers item-stickers">
									<!-- <div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--hit">
											Хит
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--new">
											Новинка
										</span>
									</div> -->
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--action">
											Акция
										</span>
									</div>
								</div>

								<div class="catalog-item__img">
									<a href="#">
										<picture>
											<source srcset="<?=IMGS_DIR?>prod_02.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>prod_02.jpg">
										</picture>
									</a>
								</div>
								<div class="catalog-item__title">
									<a href="#">Детский спортивный уголок «Эверест-2»</a>
								</div>
								<div class="catalog-item__available">
									<p class="present">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
							            </svg>
										<span>В наличии</span>
									</p>
									<p class="absent">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
										<span>Нет в наличии</span>
									</p>
								</div>

								<div class="catalog-item__v-code">
									<span class="catalog-item__v-code-name">
										Артикул:
									</span>
									«<span class="catalog-item__v-code-value">
										SportWood
									</span>»
								</div>

								<div class="catalog-item__price-cont">
									<span class="catalog-item__price">
										3 969 грн.
									</span>
									<span class="catalog-item__discount">
										-10%
									</span>
									<span class="catalog-item__old-price">
										3 869 грн.
									</span>
								</div>
								<div class="catalog-item__buy-more">
									<div class="catalog-item__have">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
									            </svg>
												<span>В корзину</span>
												</p>
												<p class="in-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
									            </svg>
												<span>В корзине</span>
												</p>
											</button>
										</div>
										<div class="catalog-item__more">
											<button class="c-style-btn compare">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
									            </svg>
											</button>
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
									<div class="catalog-item__report">
										<button class="b-style-btn js-avail-report">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
								            </svg>
											<span>Сообщить о наличии</span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="catalog-item swiper-slide catalog-item--absent">
							<div class="catalog-item-wrapper">

								<div class="catalog-item__stickers item-stickers">
									<!-- <div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--hit">
											Хит
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--new">
											Новинка
										</span>
									</div> -->
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--action">
											Акция
										</span>
									</div>
								</div>

								<div class="catalog-item__img">
									<a href="#">
										<picture>
											<source srcset="<?=IMGS_DIR?>prod_03.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>prod_03.jpg">
										</picture>
									</a>
								</div>
								<div class="catalog-item__title">
									<a href="#">Детская площадка Babyland-12</a>
								</div>
								<div class="catalog-item__available">
									<p class="present">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
							            </svg>
										<span>В наличии</span>
									</p>
									<p class="absent">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
										<span>Нет в наличии</span>
									</p>
								</div>

								<div class="catalog-item__v-code">
									<span class="catalog-item__v-code-name">
										Артикул:
									</span>
									«<span class="catalog-item__v-code-value">
										SportWood
									</span>»
								</div>

								<div class="catalog-item__price-cont">
									<span class="catalog-item__price">
										32 924 грн.
									</span>
									<span class="catalog-item__discount">

									</span>
									<span class="catalog-item__old-price">

									</span>
								</div>
								<div class="catalog-item__buy-more">
									<div class="catalog-item__have">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<span>В корзину</span>
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
									            </svg>
											</button>
										</div>
										<div class="catalog-item__more">
											<button class="c-style-btn compare">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
									            </svg>
											</button>
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
									<div class="catalog-item__report">
										<button class="b-style-btn js-avail-report">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
								            </svg>
											<span>Сообщить о наличии</span>
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="catalog-item swiper-slide">
							<div class="catalog-item-wrapper">

								<div class="catalog-item__stickers item-stickers">
									<!-- <div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--hit">
											Хит
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--new">
											Новинка
										</span>
									</div> -->
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--action">
											Акция
										</span>
									</div>
								</div>

								<div class="catalog-item__img">
									<a href="#">
										<picture>
											<source srcset="<?=IMGS_DIR?>prod_04.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>prod_04.jpg">
										</picture>
									</a>
								</div>
								<div class="catalog-item__title">
									<a href="#">Спортивный комплекс для дома Акварелька Plus 1-1</a>
								</div>
								<div class="catalog-item__available">
									<p class="present">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
							            </svg>
										<span>В наличии</span>
									</p>
									<p class="absent">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
										<span>Нет в наличии</span>
									</p>
								</div>

								<div class="catalog-item__v-code">
									<span class="catalog-item__v-code-name">
										Артикул:
									</span>
									«<span class="catalog-item__v-code-value">
										SportWood
									</span>»
								</div>

								<div class="catalog-item__price-cont">
									<span class="catalog-item__price">
										3 149 грн.
									</span>
									<span class="catalog-item__discount">
									</span>
									<span class="catalog-item__old-price">
									</span>
								</div>
								<div class="catalog-item__buy-more">
									<div class="catalog-item__have">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
									            </svg>
												<span>В корзину</span>
												</p>
												<p class="in-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
									            </svg>
												<span>В корзине</span>
												</p>
											</button>
										</div>
										<div class="catalog-item__more">
											<button class="c-style-btn compare">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
									            </svg>
											</button>
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
									<div class="catalog-item__report">
										<button class="b-style-btn js-avail-report">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
								            </svg>
											<span>Сообщить о наличии</span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="catalog-item swiper-slide catalog-item--discount">
							<div class="catalog-item-wrapper">

								<div class="catalog-item__stickers item-stickers">
									<!-- <div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--hit">
											Хит
										</span>
									</div>
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--new">
											Новинка
										</span>
									</div> -->
									<div class="sticker-wrapper">
										<span class="item-stickers__elem item-stickers__elem--action">
											Акция
										</span>
									</div>
								</div>

								<div class="catalog-item__img">
									<a href="#">
										<picture>
											<source srcset="<?=IMGS_DIR?>prod_05.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>prod_05.jpg">
										</picture>
									</a>
								</div>
								<div class="catalog-item__title">
									<a href="#">Детский спортивный комплекс BambinoWood Color Plus 1-1</a>
								</div>
								<div class="catalog-item__available">
									<p class="present">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
							            </svg>
										<span>В наличии</span>
									</p>
									<p class="absent">
										<svg>
							                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
							            </svg>
										<span>Нет в наличии</span>
									</p>
								</div>

								<div class="catalog-item__v-code">
									<span class="catalog-item__v-code-name">
										Артикул:
									</span>
									«<span class="catalog-item__v-code-value">
										SportWood
									</span>»
								</div>

								<div class="catalog-item__price-cont">
									<span class="catalog-item__price">
										4 480 грн.
									</span>
									<span class="catalog-item__discount">
										-20%
									</span>
									<span class="catalog-item__old-price">
										5 600 грн.
									</span>
								</div>
								<div class="catalog-item__buy-more">
									<div class="catalog-item__have">
										<div class="catalog-item__buy">
											<button class="a-style-btn">
												<p class="to-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
									            </svg>
												<span>В корзину</span>
												</p>
												<p class="in-cart">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#ok"></use>
									            </svg>
												<span>В корзине</span>
												</p>
											</button>
										</div>
										<div class="catalog-item__more">
											<button class="c-style-btn compare">
												<svg>
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
									            </svg>
											</button>
											<button class="c-style-btn favorites">
												<svg class="to-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#empty-heart"></use>
									            </svg>
												<svg class="in-favorites">
									                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
									            </svg>
											</button>
										</div>
									</div>
									<div class="catalog-item__report">
										<button class="b-style-btn js-avail-report">
											<svg>
								                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#envelope"></use>
								            </svg>
											<span>Сообщить о наличии</span>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
<!--</div>-->

<?php include MAIN_TEMPLATE.'footer.php';?>