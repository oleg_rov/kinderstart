<?php
include '../../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>

<!--<div class="page-content">-->
	<div class="container">
		<div class="page-block">

			<div class="side-block shift-side-block">
				<button class="shift-side-block__close">
					<svg class="svg-opened">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
					</svg>
				</button>

				<div class="catalog-side-wrapper">
<?php  include '../sidebar.php'; ?>
				</div>
			</div>

			<div class="main-block">
				<div class="regular-block">
					<h1 class="regular-block__header">
						Каталог
					</h1>
					<div class="catalog-list-items">
						<div class="cat-list-elem js-equal-height">
							<div class="cat-list-elem__img">
								<a href="<?=SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>cats/cat_img_01.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>cats/cat_img_01.jpg">
									</picture>
								</a>
							</div>
							<div class="cat-list-elem__content">
								<h2 class="cat-list-elem__title">
									<a href="<?=SITE_DIR?>catalog">
										Спортивные уголки
									</a>
								</h2>
								<ul class="cat-list-elem__sub">
									<li class="cat-list-elem__sub-elem">
										<a href="<?=SITE_DIR?>catalog">
											Детский спортивный уголок
										</a>
									</li>
									<li class="cat-list-elem__sub-elem">
										<a href="<?=SITE_DIR?>catalog">
											Спортивный уголок - Сосна
										</a>
									</li>
									<li class="cat-list-elem__sub-elem">
										<a href="<?=SITE_DIR?>catalog">
											Спортивный уголок - Бук
										</a>
									</li>
									<li class="cat-list-elem__sub-elem">
										<a href="<?=SITE_DIR?>catalog">
											Спортивный уголок - Цветной
										</a>
									</li>
									<li class="cat-list-elem__sub-elem">
										<a href="<?=SITE_DIR?>catalog">
											Гладиаторская сетка
										</a>
									</li>
								</ul>
								<div class="cat-list-elem__more">
									<a href="<?=SITE_DIR?>catalog">
										<span>Смотреть все<i class="fa fa-angle-right" aria-hidden="true"></i></span>
									</a>
								</div>
							</div>
						</div>

						<div class="cat-list-elem js-equal-height">
							<div class="cat-list-elem__img">
								<a href="<?=SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>cats/cat_img_02.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>cats/cat_img_02.jpg">
									</picture>
								</a>
							</div>
							<div class="cat-list-elem__content">
								<h2 class="cat-list-elem__title">
									<a href="<?=SITE_DIR?>catalog">
										Гимнастические маты
									</a>
								</h2>
								<ul class="cat-list-elem__sub">
									<li class="cat-list-elem__sub-elem">
										<a href="<?=SITE_DIR?>catalog">
											Детский спортивный уголок
										</a>
									</li>
									<li class="cat-list-elem__sub-elem">
										<a href="<?=SITE_DIR?>catalog">
											Спортивный уголок - Сосна
										</a>
									</li>
									<li class="cat-list-elem__sub-elem">
										<a href="<?=SITE_DIR?>catalog">
											Спортивный уголок - Бук
										</a>
									</li>
								</ul>
								<div class="cat-list-elem__more">
									<a href="<?=SITE_DIR?>catalog">
										<span>Смотреть все<i class="fa fa-angle-right" aria-hidden="true"></i></span>
									</a>
								</div>
							</div>
						</div>

						<div class="cat-list-elem js-equal-height">
							<div class="cat-list-elem__img">
								<a href="<?=SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>cats/cat_img_03.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>cats/cat_img_03.jpg">
									</picture>
								</a>
							</div>
							<div class="cat-list-elem__content">
								<h2 class="cat-list-elem__title">
									<a href="<?=SITE_DIR?>catalog">
										Шведская стенка
									</a>
								</h2>
								<ul class="cat-list-elem__sub">
									<li class="cat-list-elem__sub-elem">
										<a href="<?=SITE_DIR?>catalog">
											Деревянные шведские стенки
										</a>
									</li>
									<li class="cat-list-elem__sub-elem">
										<a href="<?=SITE_DIR?>catalog">
											Металлические шведские стенки
										</a>
									</li>
								</ul>
								<div class="cat-list-elem__more">
									<a href="<?=SITE_DIR?>catalog">
										<span>Смотреть все<i class="fa fa-angle-right" aria-hidden="true"></i></span>
									</a>
								</div>
							</div>
						</div>

						<div class="cat-list-elem js-equal-height">
							<div class="cat-list-elem__img">
								<a href="<?=SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>cats/cat_img_04.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>cats/cat_img_04.jpg">
									</picture>
								</a>
							</div>
							<div class="cat-list-elem__content">
								<h2 class="cat-list-elem__title">
									<a href="<?=SITE_DIR?>catalog">
										Детские прыгунки
									</a>
								</h2>

								<div class="cat-list-elem__more">
									<a href="<?=SITE_DIR?>catalog">
										<span>Смотреть все<i class="fa fa-angle-right" aria-hidden="true"></i></span>
									</a>
								</div>
							</div>
						</div>

						<div class="cat-list-elem js-equal-height">
							<div class="cat-list-elem__img">
								<a href="<?=SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>cats/cat_img_05.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>cats/cat_img_05.jpg">
									</picture>
								</a>
							</div>
							<div class="cat-list-elem__content">
								<h2 class="cat-list-elem__title">
									<a href="<?=SITE_DIR?>catalog">
										Детские игровые комплексы
									</a>
								</h2>

								<div class="cat-list-elem__more">
									<a href="<?=SITE_DIR?>catalog">
										<span>Смотреть все<i class="fa fa-angle-right" aria-hidden="true"></i></span>
									</a>
								</div>
							</div>
						</div>

						<div class="cat-list-elem js-equal-height">
							<div class="cat-list-elem__img">
								<a href="<?=SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>cats/cat_img_06.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>cats/cat_img_06.jpg">
									</picture>
								</a>
							</div>
							<div class="cat-list-elem__content">
								<h2 class="cat-list-elem__title">
									<a href="<?=SITE_DIR?>catalog">
										Детские площадки
									</a>
								</h2>

								<div class="cat-list-elem__more">
									<a href="<?=SITE_DIR?>catalog">
										<span>Смотреть все<i class="fa fa-angle-right" aria-hidden="true"></i></span>
									</a>
								</div>
							</div>
						</div>

						<div class="cat-list-elem js-equal-height">
							<div class="cat-list-elem__img">
								<a href="<?=SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>cats/cat_img_07.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>cats/cat_img_07.jpg">
									</picture>
								</a>
							</div>
							<div class="cat-list-elem__content">
								<h2 class="cat-list-elem__title">
									<a href="<?=SITE_DIR?>catalog">
										Парты
									</a>
								</h2>

								<div class="cat-list-elem__more">
									<a href="<?=SITE_DIR?>catalog">
										<span>Смотреть все<i class="fa fa-angle-right" aria-hidden="true"></i></span>
									</a>
								</div>
							</div>
						</div>

						<div class="cat-list-elem js-equal-height">
							<div class="cat-list-elem__img">
								<a href="<?=SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>cats/cat_img_08.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>cats/cat_img_08.jpg">
									</picture>
								</a>
							</div>
							<div class="cat-list-elem__content">
								<h2 class="cat-list-elem__title">
									<a href="<?=SITE_DIR?>catalog">
										Игровые палатки
									</a>
								</h2>

								<div class="cat-list-elem__more">
									<a href="<?=SITE_DIR?>catalog">
										<span>Смотреть все<i class="fa fa-angle-right" aria-hidden="true"></i></span>
									</a>
								</div>
							</div>
						</div>

						<div class="cat-list-elem js-equal-height">
							<div class="cat-list-elem__img">
								<a href="<?=SITE_DIR?>catalog">
									<picture>
										<source srcset="<?=IMGS_DIR?>cats/cat_img_09.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>cats/cat_img_09.jpg">
									</picture>
								</a>
							</div>
							<div class="cat-list-elem__content">
								<h2 class="cat-list-elem__title">
									<a href="<?=SITE_DIR?>catalog">
										Дополнительное оборудование
									</a>
								</h2>

								<div class="cat-list-elem__more">
									<a href="<?=SITE_DIR?>catalog">
										<span>Смотреть все<i class="fa fa-angle-right" aria-hidden="true"></i></span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--</div>-->

<?php include MAIN_TEMPLATE.'footer.php';?>