	<div class="v-multy-menu">
		<h4 class="v-multy-menu__title">Все категории</h4>

		<ul class="v-multy-menu__content">

			<li class="v-multy-menu__item">
				<div class="v-multy-menu__item-cont opened">
					<a href="#" class="v-multy-menu__name active">Спортивные уголки</a>
					<span class="v-multy-menu__toggle js-multy-menu"></span>
				</div>
				<ul class="v-multy-menu__sub">
					<li class="v-multy-menu__sub-item active">
						<a href="#">Детский спортивный уголок</a>
					</li>
					<li class="v-multy-menu__sub-item">
						<a href="#">Спортивный уголок - Бук</a>
					</li>
				</ul>
			</li>

			<li class="v-multy-menu__item">
				<div class="v-multy-menu__item-cont">
					<a href="#" class="v-multy-menu__name">Спортивный уголок - Цветной</a>
					<span class="v-multy-menu__toggle js-multy-menu"></span>
				</div>
				<ul class="v-multy-menu__sub">
					<li class="v-multy-menu__sub-item">
						<a href="#">Детский спортивный уголок</a>
					</li>
					<li class="v-multy-menu__sub-item">
						<a href="#">Спортивный уголок - Бук</a>
					</li>
				</ul>
			</li>

			<li class="v-multy-menu__item">
				<div class="v-multy-menu__item-cont">
					<a href="#" class="v-multy-menu__name">Спортивный уголок - Цветной</a>
					<span class="v-multy-menu__toggle js-multy-menu"></span>
				</div>
				<ul class="v-multy-menu__sub">
					<li class="v-multy-menu__sub-item">
						<a href="#">Детский спортивный уголок</a>
					</li>
					<li class="v-multy-menu__sub-item">
						<a href="#">Спортивный уголок - Бук</a>
					</li>
				</ul>
			</li>

			<li class="v-multy-menu__item">
				<div class="v-multy-menu__item-cont">
					<a href="#" class="v-multy-menu__name">Спортивный уголок - Цветной</a>
					<span class="v-multy-menu__toggle js-multy-menu"></span>
				</div>
				<ul class="v-multy-menu__sub">
					<li class="v-multy-menu__sub-item">
						<a href="#">Детский спортивный уголок</a>
					</li>
					<li class="v-multy-menu__sub-item">
						<a href="#">Спортивный уголок - Бук</a>
					</li>
				</ul>
			</li>

			<li class="v-multy-menu__item">
				<div class="v-multy-menu__item-cont">
					<a href="#" class="v-multy-menu__name">Спортивный уголок - Цветной</a>
					<span class="v-multy-menu__toggle js-multy-menu"></span>
				</div>
				<ul class="v-multy-menu__sub">
					<li class="v-multy-menu__sub-item">
						<a href="#">Детский спортивный уголок</a>
					</li>
					<li class="v-multy-menu__sub-item">
						<a href="#">Спортивный уголок - Бук</a>
					</li>
				</ul>
			</li>

			<li class="v-multy-menu__item">
				<div class="v-multy-menu__item-cont">
					<a href="#" class="v-multy-menu__name">Спортивный уголок - Цветной</a>
					<span class="v-multy-menu__toggle js-multy-menu"></span>
				</div>
				<ul class="v-multy-menu__sub">
					<li class="v-multy-menu__sub-item">
						<a href="#">Детский спортивный уголок</a>
					</li>
					<li class="v-multy-menu__sub-item">
						<a href="#">Спортивный уголок - Бук</a>
					</li>
				</ul>
			</li>

		</ul>
	</div>

<?if(getPath() == '/catalog/'):?>
		<div class="cats-filter">
			<form action="#" id="cats-filter-form">
				<div class="cats-filter-prop cats-filter-price opened">
					<div class="cats-filter-prop__title js-cats-filter-toggler">
						<h4 class="cats-filter-prop__title-name">
							<i class="fa fa-angle-up" aria-hidden="true"></i>
							<span>Цена</span>
						</h4>
					</div>

					<div class="cats-filter-prop__cont cats-filter-price__cont">
						<div class="range-input-group">
							<input type="text" class="fx-range-from" id="range-from">
							<input type="text" class="fx-range-to" id="range-to">
							<span class="fx-range-sep">–</span>
						</div>

						<div class="fx-range-wrapper">
							<div id="slider-range"></div>
						</div>
					</div>
				</div>

				<div class="cats-filter-prop opened">
					<div class="cats-filter-prop__title js-cats-filter-toggler">
						<h4 class="cats-filter-prop__title-name">
							<i class="fa fa-angle-up" aria-hidden="true"></i>
							<span>Высота</span>
						</h4>
					</div>

					<div class="cats-filter-prop__cont">
						<div class="check-group">
							<label class="check cats-filter-check">
								<input type="checkbox" class="check__input" checked>
								<span class="check__box"></span>
								170 см
							</label>
							<label class="check cats-filter-check">
								<input type="checkbox" class="check__input">
								<span class="check__box"></span>
								150 см
							</label>
							<label class="check cats-filter-check">
								<input type="checkbox" class="check__input">
								<span class="check__box"></span>
								130 см
							</label>
						</div>
					</div>
				</div>

				<div class="cats-filter-buttons">
					<button class="a-style-btn js-do-filter" type="submit">Показать</button>
					<button class="d-style-btn js-cancel-filter" type="reset">Сбросить</button>
				</div>
			</form>
		</div>
<?endif?>