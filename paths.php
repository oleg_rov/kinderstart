<?php
define('ROOT_PATH', dirname( __FILE__ ) . '/');
define('MAIN_TEMPLATE', ROOT_PATH.'local/templates/kstart/');
define('MOB_TEMPLATE', ROOT_PATH.'local/templates/m_kstart/');

define('SITE_DIR', '/');
define('MOB_SITE_DIR', SITE_DIR.'mobile/');
define('MAIN_ASSETS', SITE_DIR.'local/templates/kstart/');
define('MOB_ASSETS', SITE_DIR.'local/templates/m_kstart/');
define('DEFAULT_ASSETS', SITE_DIR.'local/templates/.default/');
//define('IMGS_DIR', SITE_DIR.'img/');
define('IMGS_DIR', DEFAULT_ASSETS.'images/');

function getPath() {
	$parts = explode('?', $_SERVER["REQUEST_URI"]);
	return $parts[0];
}