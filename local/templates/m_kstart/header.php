<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		img {width: 0; height: 0;}
		svg {fill: transparent;}
		button {background-color: transparent; border-color: transparent;}
	</style>

	<link media="none" onload="if(media!='all') media='all'" rel="stylesheet" type="text/css", href="<?=MOB_ASSETS?>build/css/main.min.css">
	<noscript>
		<link rel="stylesheet" href="<?=MOB_ASSETS?>build/css/main.min.css">
	</noscript>

	<script src="<?=MOB_ASSETS?>build/js/script.min.js" async defer></script>
	<!-- <script>
		document.addEventListener("DOMContentLoaded", function() {
			var myCSS = document.createElement( "link" );
			myCSS.rel = "stylesheet";
			myCSS.href = "/local/templates/m_kstart/build/css/main.min.css";
			document.head.insertBefore( myCSS, document.head.childNodes[ document.head.childNodes.length - 1 ].nextSibling );
	    });
	</script> -->
</head>
<body style="min-width: 350px; max-width: 480px; margin-left: auto; margin-right: auto;">
	<header class="header">
		<div class="top-header">
			<button class="catalog-menu-button">
				<svg class="svg-closed">
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#bars"></use>
	            </svg>
				<svg class="svg-opened">
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#close"></use>
	            </svg>
			</button>

			<div class="catalog-menu-block">
				<div class="additional-menu-block">
					<div class="additional-menu-user">
						<button class="additional-menu-user__btn js-auth-btn">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#user"></use>
				            </svg>
				            <span>Войти</span>
						</button>
					</div>

					<div class="header-login">
						<div class="header-login__to-back">
							<button class="header-login__to-back-btn js-login-back">
								<i class="fa fa-angle-left" aria-hidden="true"></i> <span>Назад</span>
							</button>
						</div>
						<div class="header-login__content">
							<div class="header-login__title">Войти</div>

							<form id="header-login-form">
								<div class="header-login__input-group">
									<input type="text" name="login" class="header-login__input-field" required>
									<span class="header-login__input-placeholder">Логин</span>
								</div>
								<div class="header-login__input-group">
									<input type="password" name="password" class="header-login__input-field" required>
									<span class="header-login__input-placeholder">Пароль</span>
								</div>

								<div class="header-login__btn-group">
									<div class="header-login__forgot-link">
										<a href="#">Забыли пароль?</a>
									</div>
									<div class="header-login__btn">
										<button class="a-style-btn">Войти</button>
									</div>
									<div class="header-login__reg-link">
										<p>Новый пользователь?</p>
										<a href="#">Зарегистрироваться</a>
									</div>
								</div>
							</form>
						</div>
					</div>

					<div class="additional-menu-lang">
						<button class="bottom-header-btn additional-menu-lang__btn active">
							<span>RU</span>
						</button>
						<button class="bottom-header-btn additional-menu-lang__btn">
							<span>UA</span>
						</button>
					</div>
				</div>
				<div class="submenu-block catalog-menu">
					<div class="submenu-block__title js-submenu-title">
						<span>Категории товара</span>
						<button class="submenu-block__btn">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</button>
					</div>
					<ul class="submenu-block__list catalog-menu__list js-submenu-content">

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_01.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_01.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Спортивные уголки
								</p>
								<div class="catalog-menu__counter">5</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_02.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_02.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Гимнастические маты
								</p>
								<div class="catalog-menu__counter">10</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_03.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_03.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Шведская стенка
								</p>
								<div class="catalog-menu__counter">15</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_04.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_04.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Детские прыгунки
								</p>
								<div class="catalog-menu__counter">5</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_05.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_05.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Детские игровые комплексы
								</p>
								<div class="catalog-menu__counter">100</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_06.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_06.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Детские площадки
								</p>
								<div class="catalog-menu__counter">0</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_07.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_07.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Парты
								</p>
								<div class="catalog-menu__counter">1</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_08.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_08.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Игровые палатки
								</p>
								<div class="catalog-menu__counter">0</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_09.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_09.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Турники и брусья
								</p>
								<div class="catalog-menu__counter">50</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_10.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_10.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Дополнительное оборудование
								</p>
								<div class="catalog-menu__counter">12</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_11.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_11.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Детские песочницы
								</p>
								<div class="catalog-menu__counter">20</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_12.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_12.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Детская мебель
								</p>
								<div class="catalog-menu__counter">5</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_13.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_13.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Детские домики
								</p>
								<div class="catalog-menu__counter">5</div>
							</a>
						</li>

						<li class="submenu-block__item catalog-menu__item">
							<a href="<?=MOB_SITE_DIR?>catalog/list">
								<div class="catalog-menu__img">
									<picture>
										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_14.webp"
										type="image/webp">

										<source
										data-srcset="<?=IMGS_DIR?>
cats/cat_img_14.jpg"
										type="image/jpeg">

										<img
										src="<?=IMGS_DIR?>
cats/noimg.png"
										class="lazyestload">
									</picture>
								</div>
								<p class="catalog-menu__name">
									Шезлонги
								</p>
								<div class="catalog-menu__counter">5</div>
							</a>
						</li>

					</ul>
				</div>
				<div class="submenu-block customer-menu">
					<div class="submenu-block__title js-submenu-title">
						<span>Покупателям</span>
						<button class="submenu-block__btn">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</button>
					</div>
					<ul class="submenu-block__list js-submenu-content">
						<li class="submenu-block__item">
							<a href="<?=MOB_SITE_DIR?>company/faq/">Вопросы и ответы</a>
						</li>
						<li class="submenu-block__item">
							<a href="<?=MOB_SITE_DIR?>company/discounts/">Акции и скидки</a>
						</li>
						<li class="submenu-block__item">
							<a href="<?=MOB_SITE_DIR?>company/pay-and-delivery/">Оплата и доставка</a>
						</li>
						<li class="submenu-block__item">
							<a href="<?=MOB_SITE_DIR?>company/credit/">Кредит</a>
						</li>
						<li class="submenu-block__item">
							<a href="<?=MOB_SITE_DIR?>company/guarantee/">Гарантия</a>
						</li>
					</ul>
				</div>

			</div>

			<div class="header-logo">
				<?if(getPath() == MOB_SITE_DIR):?>
				<img src="<?=IMGS_DIR?>
header_logo.svg" alt="kinderstart">
				<?else:?>
				<a href="<?=MOB_SITE_DIR?>">
				<img src="<?=IMGS_DIR?>header_logo.svg" alt="kinderstart">
				</a>
				<?endif?>
			</div>
		</div>
		<div class="bottom-header">
			<button class="bottom-header-btn js-cont-btn
			bottom-cont-btn">
				<svg class="svg-closed">
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#phone"></use>
	            </svg>
	            <svg class="svg-opened">
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#close"></use>
	            </svg>
			</button>
			<div class="bottom-cont-block cont-block">
				<ul class="cont-block__phones">
					<li class="cont-block__phone">
						<span class="phone-icon phone-icon--phone">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#phone"></use>
				            </svg>
			        	</span>
						<span class="phone-code">(044)</span> 360-64-56
					</li>
					<li class="cont-block__phone">
						<span class="phone-icon phone-icon--kievstar">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#kievstar"></use>
				            </svg>
			        	</span>
						<span class="phone-code">(067)</span> 506-42-09
					</li>
					<li class="cont-block__phone">
						<span class="phone-icon phone-icon--vodafone">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#vodafone"></use>
				            </svg>
			        	</span>
						<span class="phone-code">(050)</span> 666-02-12
					</li>
				</ul>

				<div class="cont-block__schedule">
					<span>Пн — Пт с 9 до 21</span>
				</div>
			</div>
			<button class="bottom-header-btn bottom-search-btn js-search-btn">
				<svg class="svg-closed">
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#search"></use>
	            </svg>
	            <svg class="svg-opened">
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#close"></use>
	            </svg>
			</button>
			<button class="bottom-header-btn bottom-header-btn--added">
				<span class="bottom-header-btn__cnt">1</span>
				<svg>
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#heart"></use>
	            </svg>
	        </button>
			<a href="<?=MOB_SITE_DIR?>catalog/compare/" class="bottom-header-btn">
				<span class="bottom-header-btn__cnt">1</span>
				<svg>
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#compare"></use>
	            </svg>
	        </a>
			<button class="bottom-header-btn">
				<span class="bottom-header-btn__cnt">1</span>
				<svg>
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#cart"></use>
	            </svg>
	        </button>
		</div>
		<div class="header-search">
			<form class="header-search__form" id="header-search-form">
				<div class="header-search__input-group">
					<input type="text" name="q" class="header-search__input-field">
					<span class="header-search__input-placeholder">Поиск по товарам</span>
				</div>

				<button type="submit" name="s" class="header-search__btn">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>
sprites.svg#search"></use>
		            </svg>
				</button>
			</form>
		</div>
		<div class="catalog-menu-bg"></div>
<?
if(getPath() == MOB_SITE_DIR):
?>
<!-- start of slider on index page -->
		<div class="top-banner swiper-container">
			<div class="swiper-wrapper">

				<div class="swiper-slide top-banner-item">
					<div class="top-banner-item-bg"
					style="background-image: url(<?=IMGS_DIR?>top-banners/banner_bg_01_m.jpg)">
						<div class="container">
							<h3 class="top-banner-item__header">Спортивный комплекс</h3>
							<p class="top-banner-item__txt">
								Заказывали в «Олл Корп» систему безопасности для нашего центра.
							</p>
						</div>
					</div>
				</div>

				<div class="swiper-slide top-banner-item">
					<div class="top-banner-item-bg"
					style="background-image: url(<?=IMGS_DIR?>top-banners/banner_bg_01_m.jpg)">
						<div class="container">
							<h3 class="top-banner-item__header">Slide 2</h3>
							<p class="top-banner-item__txt">
								Заказывали в «Олл Корп» систему безопасности для нашего центра.
							</p>
						</div>
					</div>
				</div>

				<div class="swiper-slide top-banner-item">
					<div class="top-banner-item-bg"
					style="background-image: url(<?=IMGS_DIR?>top-banners/banner_bg_01_m.jpg)">
						<div class="container">
							<h3 class="top-banner-item__header">Slide 3</h3>
							<p class="top-banner-item__txt">
								Заказывали в «Олл Корп» систему безопасности для нашего центра.
							</p>
						</div>
					</div>
				</div>

			</div>
			<div class="container">
				<div class="swiper-pagination"></div>
			</div>
		</div>
<!-- end of slider on index page -->
<?else:?>
<div class="pages-nav">
	<div class="container">
		<div class="breadcrumbs">
			<div class="breadcrumbs-item">
				<a href="/" class="breadcrumbs-item__name">
					Главная
				</a>
				<span class="breadcrumbs-item__separator">></span>
			</div>
			<div class="breadcrumbs-item">
				<a href="#" class="breadcrumbs-item__name">
					Каталог
				</a>
				<span class="breadcrumbs-item__separator">></span>
			</div>
			<div class="breadcrumbs-item">
				<a href="#" class="breadcrumbs-item__name">
					Спортивный уголок для дома
				</a>
				<span class="breadcrumbs-item__separator">></span>
			</div>
			<div class="breadcrumbs-item">
				<span class="breadcrumbs-item__name">
					Детские спортивные уголки
				</span>
			</div>
		</div>

		<button class="share-btn js-share-btn">
			<svg>
	            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#share"></use>
	        </svg>
		</button>

		<div class="share-block">
			<div class="share-block__wrapper">
				<a href="#" class="share-block__ilem share-block__ilem--fb">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
		            </svg>
				</a>
				<a href="#" class="share-block__ilem share-block__ilem--twt">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
		            </svg>
				</a>
				<a href="#" class="share-block__ilem share-block__ilem--gplus">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#gplus"></use>
		            </svg>
				</a>
				<a href="#" class="share-block__ilem share-block__ilem--insta">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#insta"></use>
		            </svg>
				</a>
			</div>
		</div>

	</div>
</div>
<?endif?>
	</header>
	<main>
