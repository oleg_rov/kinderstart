	</main>

	<footer class="footer">
		<div class="container">
			<div class="footer-logo">
				<img src="<?=IMGS_DIR?>footer_logo.svg" alt="kinderstart">
			</div>
			<div class="footer-content">
				<div class="footer-menus">
					<div class="footer-catalog-menu footer-menu">
						<div class="footer-catalog-menu__header footer-menu__header js-foooter-menu">
							<span class="footer-menu__header-icon footer-catalog-menu__header-icon">
								<svg class="svg-closed">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
					            </svg>
								<svg class="svg-opened">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
					            </svg>
							</span>
							<spam class="footer-menu__header-txt">Каталог товаров</spam>
						</div>

						<ul class="footer-menu__list">
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Спортивные уголки</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Гимнастические маты</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Шведская стенка</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Детские прыгунки</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Детские игровые комплексы</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Детские площадки</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Парты</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Игровые палатки</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Турники и брусья</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Дополнительное оборудование</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Детские песочницы</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Детская мебель</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Детские домики</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>catalog/list">Шезлонги</a>
							</li>
						</ul>
					</div>

					<div class="footer-about-menu footer-menu">
						<div class="footer-about-menu__header footer-menu__header js-foooter-menu">
							<span class="footer-menu__header-icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#home"></use>
					            </svg>
							</span>
							<spam class="footer-menu__header-txt">Компания</spam>
							<spam class="footer-menu__arrow">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
							</spam>
						</div>

						<ul class="footer-menu__list">
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/certificates/">Сертификаты</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/awards/">Награды и достижения</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/partners/">Наши партнеры</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/reviews/">Отзывы</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/reviews/">Вакансии</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/contacts/">Реквизиты</a>
							</li>
						</ul>
					</div>

					<div class="footer-help-menu footer-menu">
						<div class="footer-help-menu__header footer-menu__header js-foooter-menu">
							<span class="footer-menu__header-icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#disc"></use>
					            </svg>
							</span>
							<spam class="footer-menu__header-txt">Помощь покупателю</spam>
							<spam class="footer-menu__arrow">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
							</spam>
						</div>

						<ul class="footer-menu__list">
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/faq/">Вопросы и ответы</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/discounts/">Акции и скидки</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/pay-and-delivery/">Оплата и доставка</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/credit/">Кредит</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/guarantee/">Гарантия</a>
							</li>
						</ul>
					</div>

					<div class="footer-hints-menu footer-menu">
						<div class="footer-hints-menu__header footer-menu__header js-foooter-menu">
							<span class="footer-menu__header-icon">
								<svg>
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#dialog"></use>
					            </svg>
							</span>
							<spam class="footer-menu__header-txt">Пресс-центр</spam>
							<spam class="footer-menu__arrow">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
							</spam>
						</div>

						<ul class="footer-menu__list">
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>news">Новости</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>news">Блог</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=MOB_SITE_DIR?>company/faq/">Вопросы и ответы</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="footer-contacts">
					<ul class="footer-phones">
						<li class="footer-phones__item">
							<a href="tel:0443606456">(044) 360-64-56</a>
						</li>
						<li class="footer-phones__item">
							<a href="tel:0675064209">(067) 506-42-09</a>
						</li>
						<li class="footer-phones__item">
							<a href="tel:0506660212">(050) 666-02-12</a>
						</li>
					</ul>

					<p class="footer-contacts__schedule">ПН — ПТ с 9 до 21</p>
					<p class="footer-contacts__mail"><a href="mailto:info@sportbaby.ua">info@sportbaby.ua</a></p>
				</div>

				<div class="footer-social-links">
					<button class="footer-social-link">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
			            </svg>
					</button>
					<button class="footer-social-link">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
			            </svg>
					</button>
					<button class="footer-social-link">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#gplus"></use>
			            </svg>
					</button>
					<button class="footer-social-link">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#insta"></use>
			            </svg>
					</button>
				</div>

			</div>
			<div class="copyrights">
				<span class="copyrights__txt">© 2019 «kinderstart» Все права защищены.</span><br>
				<span class="copyrights__link"><a href="<?=MOB_SITE_DIR?>company/">Пользовательское соглашение</a></span>
			</div>
		</div>
	</footer>

	<div class="pop-up avail-report" id="avail-report">
		<div class="pop-up-bg"></div>
		<div class="pop-up-window avail-report-window">
			<button class="pop-up-window__close" id="avail-report-close">
				<svg>
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
	            </svg>
			</button>
			<div class="pop-up-window__title">Сообщить о наличии</div>
			<form id="avail-report-form">
				<div class="pop-up-window__input-group">
					<input type="text" name="u-name" class="pop-up-window__input-field" required>
					<span class="pop-up-window__input-placeholder">Имя</span>
				</div>
				<div class="pop-up-window__input-group">
					<input type="text" name="u-phone" class="pop-up-window__input-field" required>
					<span class="pop-up-window__input-placeholder">Телефон</span>
				</div>
				<div class="pop-up-window__btn-group">
					<div class="avail-report__btn">
						<button class="a-style-btn">Отправить</button>
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>