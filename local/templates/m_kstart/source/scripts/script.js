$(function () {
// fixing height content (all)
	function fixContentHeight() {
		var w_h = $(window).height();
		var h_h = $('.header').outerHeight();
		var f_h = $('.footer').outerHeight();
		var m_h = $('main').removeAttr('style').height();
		var diff = w_h - (h_h + f_h + m_h);
		if(diff > 0) {
			$('main').height(m_h + diff);
		}
	}
	fixContentHeight();

	$(window).resize(fixContentHeight);
// fixing height content

// categories menu
	$('.catalog-menu-button').click(function() {
		$('.bottom-header').css('z-index', '');
		$(this).toggleClass('opened');
		if($(this).hasClass('opened')) {
			$('.catalog-menu-block').fadeIn();
			$('.catalog-menu-bg').show();
		} else {
			$('.catalog-menu-block').fadeOut();
			$('.catalog-menu-bg').hide();
		}
	});

	$(document).click(function(event) {
		if ($(event.target).closest('.catalog-menu-block').length ||
			$(event.target).closest('.catalog-menu-button').length) return;
	    $('.catalog-menu-block').fadeOut();
		$('.catalog-menu-button').removeClass('opened');
		//$('.catalog-menu-bg').hide();
	    event.stopPropagation();
	});
// categories menu

$(document).click(function(event) {
if ($(event.target).closest('.catalog-menu-block').length ||
	$(event.target).closest('.catalog-menu-button').length ||
	$(event.target).closest('.js-cont-btn').length ||
	$(event.target).closest('.bottom-cont-block').length ||
	$(event.target).closest('.js-search-btn').length ||
	$(event.target).closest('.header-search').length)
	return;

	$('.catalog-menu-bg').hide();
    event.stopPropagation();
});

// top-menu контакты (all)
	$('.js-cont-btn').click(function() {
		$('.bottom-header').css('z-index', '');
		$(this).toggleClass('opened');
		if($(this).hasClass('opened')) {
			$('.bottom-cont-block').fadeIn();
			$('.bottom-header').css('z-index', 3);
			$('.catalog-menu-bg').show();
		} else {
			$('.bottom-cont-block').fadeOut();
			$('.bottom-header').css('z-index', '');
			$('.catalog-menu-bg').hide();
		}
	});

	$(document).click(function(event) {
		if ($(event.target).closest('.js-cont-btn').length ||
			$(event.target).closest('.bottom-cont-block').length) return;
		$('.bottom-cont-block').fadeOut();
		$('.js-cont-btn').removeClass('opened');
		event.stopPropagation();
	});
// top-menu контакты

// top-menu search
	$('.js-search-btn').click(function() {
		$('.bottom-header').css('z-index', '');
		$(this).toggleClass('opened');
		if($(this).hasClass('opened')) {
			$('.header-search').fadeIn();
			$('.bottom-header').css('z-index', 3);
			$('.catalog-menu-bg').show();
		} else {
			$('.header-search').fadeOut();
			$('.bottom-header').css('z-index', '');
			$('.catalog-menu-bg').hide();
		}
	});

	$(document).click(function(event) {
		if ($(event.target).closest('.js-search-btn').length ||
			$(event.target).closest('.header-search').length) return;
		$('.header-search').slideUp();
		$('.js-search-btn').removeClass('opened');
		event.stopPropagation();
	});

	$('#header-search-form').submit(function(e) {
		e.preventDefault();
		$(this).find('input').val('');
		$('.header-search').slideUp();
		$('.catalog-menu-bg').hide();
		$('.js-search-btn').removeClass('opened');
	});
// top-menu search

// top-menu auth
	$('.js-auth-btn').click(function() {
		$('.header-login').fadeIn();
	});

	$('.js-login-back').click(function() {
		$('.header-login').fadeOut();
	});

	$('#header-login-form').submit(function(e) {
		e.preventDefault();
		$(this).find('input, textarea').each(function() {
			$(this).val('');
		});
		$('.header-login').fadeOut();
	});

	$('#header-login-form input').on('change', function() {
		if($(this).val() != '') {
			$(this).next('span').hide();
		} else {
			$(this).next('span').show();
		}
	});
// top-menu auth

// categories submenu
	$('.js-submenu-title').click(function() {
		var parent = $(this).closest('.submenu-block');
		parent.toggleClass('opened');
		if(parent.hasClass('opened')) {
			parent.find('.js-submenu-content').slideDown();
			$(this).find('.fa').removeClass('fa-angle-down');
			$(this).find('.fa').addClass('fa-angle-up');
		} else {
			parent.find('.js-submenu-content').slideUp();
			$(this).find('.fa').removeClass('fa-angle-up');
			$(this).find('.fa').addClass('fa-angle-down');
		}
	});
// categories submenu

// sort button (catalog-section)
	if ($('.js-sort-dir').length > 0) {
		$('.js-sort-dir').click(function(e) {
			var sortList = $(this).closest('.catalog-section-sort')
							.find('.catalog-section-sort__drop-box');
			console.log(sortList);
			if (sortList.hasClass('opened')) {
				sortList.fadeOut('fast', function() {
					sortList.removeClass('opened');
				});
			} else {
				sortList.fadeIn('fast', function() {
					sortList.addClass('opened');
				});
			}
		});

		$(document).click(function(event) {
		    if ($(event.target).closest('.catalog-section-sort__drop-box').length || $(event.target).closest('.js-sort-dir').length) return;
		    $('.catalog-section-sort__drop-box').fadeOut('fast', function() {
		    	$(this).removeClass('opened');
		    });
		    event.stopPropagation();
		});
	}
// sort button

// lang buttons
	$('.additional-menu-lang__btn').click(function() {
		$('.additional-menu-lang__btn').removeClass('active');
		$(this).addClass('active');
	});
// lang buttons

// top banner
	var topBannerSwiper = new Swiper('.top-banner', {
		pagination: {
	      el: '.swiper-pagination',
	      clickable: true
	    },
	    loop: true,
	    speed: 400,
	});
// top banner

// portfolio slider
	var portfolioSwiper = new Swiper('.portfolio-items', {
	    speed: 400,
	    navigation: {
			nextEl: '.swiper-buttons .next-portfolio',
			prevEl: '.swiper-buttons .prev-portfolio',
		}
	});
// portfolio slider

// categories block more button
	$('.js-more-cats').click(function() {
		$('.categories-item').slideDown();
		$('.categories-items__more').hide();
	});
// categories block more button

// recommended-block tabs
	$('.recommended-block .catalog-tab').click(function() {
		$('.recommended-block .catalog-tab').removeClass('active');
		$(this).addClass('active');
	});
// recommended-block tabs

// products cards buttons on index page
	$('.catalog-item__buy').click(function() {
		$(this).addClass("done").find('button').attr('disabled', 'disabled');
	});
	$('.catalog-item__more .favorites, .catalog-item__more .compare').click(function() {
		$(this).toggleClass("done");
	});
// products cards buttons on index page

// recommended block more button
	$('.js-more-prods').click(function() {
		$('.catalog-item').slideDown();
		$('.catalog-items__more').hide();
	});
// categories block more button

// show more on about shop section
	$('.about-shop-more span').click(function() {
		$('.about-shop-content').addClass('extended');
	});
// show more on about shop section

// social section on index page
	function showNLines(n) {
		var items = $('.our-soc-item');
		items.each(function(i) {
			if( i >= n) {
				$(items[i]).hide();
			}
		});
	}

	function addNLines(n) {
		var items = $('.our-soc-item');
		var hidden = $('.our-soc-item:hidden');
		hidden.each(function(i) {
			if( i < n) {
				$(hidden[i]).show();
			}
		});
		if ($('.our-soc-item:hidden').length < 1) {
			$('.js-more-soc').hide();
		}
	}

	$('.js-more-soc').click(function() {
		addNLines(2);
	});
	showNLines(2);
// social section on index page

// footer menues
	$('.js-foooter-menu').click(function() {
		var parent = $(this).parent('.footer-menu');
		parent.toggleClass('opened');
		if (parent.hasClass('opened')) {
			parent.find('.footer-menu__list').slideDown();
			if (parent.hasClass('footer-catalog-menu')) {

			} else {
				parent.find('.footer-menu__arrow .fa').removeClass('fa-angle-right');
				parent.find('.footer-menu__arrow .fa').addClass('fa-angle-down');
			}
		} else {
			parent.find('.footer-menu__list').slideUp();
			if (parent.hasClass('footer-catalog-menu')) {

			} else {
				parent.find('.footer-menu__arrow .fa').removeClass('fa-angle-down');
				parent.find('.footer-menu__arrow .fa').addClass('fa-angle-right');
			}
		}
	});
// footer menues

// pop-up (сообщить о наличии)
	$('.js-avail-report').click(function() {
		$('#avail-report').show();
	});

	$(document).click(function(event) {
	    if ($(event.target).closest('.js-avail-report').length ||
	    	$(event.target).closest('.avail-report-window').length) return;
	    $('#avail-report').hide();
	    event.stopPropagation();
	});

	$('#avail-report-form').submit(function(e) {
		e.preventDefault();
		$(this).find('input, textarea').each(function() {
			$(this).val('');
		});
		$('#avail-report').hide();
	});

	$('#avail-report-close').click(function() {
		$('#avail-report').hide();
	});

	$('#avail-report input').on('change', function() {
		if($(this).val() != '') {
			$(this).next('span').hide();
		} else {
			$(this).next('span').show();
		}
	});
// pop-up (сообщить о наличии)

// catalog filter (catalog-section)
	$('.js-cats-filter-toggler').click(function() {
		var parent = $(this).parent('.cats-filter-prop');
		if (parent.hasClass('opened')) {
			parent.find('.cats-filter-prop__cont').slideUp(200, function() {
				parent.removeClass('opened')
					.find('.fa')
					.removeClass('fa-angle-up')
					.addClass('fa-angle-down');
			});
		} else {
			parent.find('.cats-filter-prop__cont').slideDown(200, function() {
				parent.addClass('opened')
					.find('.fa')
					.removeClass('fa-angle-down')
					.addClass('fa-angle-up');
			});
		}
	});

	$('.js-cats-filters').click(function() {
		var filter = $('.filter-block');
		var filterHeight = $(document).height() - 70 + 'px';
		if (filter.hasClass('opened')) {
			filter.fadeOut(200, function() {
				filter.removeClass('opened');
			});
		} else {
			filter.css('height', filterHeight);
			filter.fadeIn(200, function() {
				filter.addClass('opened');
			});
		}
	});

	$('.filter-block__close').click(function() {
		$('.js-cats-filters').trigger('click');
	});

	$(document).click(function(event) {
		if ($(event.target).closest('.filter-block').length) return;
		if ($('.filter-block').hasClass('opened')) {
			$('.filter-block').fadeOut(200, function() {
				$(this).removeClass('opened');
			});
		}
	    event.stopPropagation();
	});

	$('#cats-filter-form').submit(function(e) {
		e.preventDefault();
		$('.js-cats-filters').trigger('click');
	});
// catalog filter

// catalog filter range slider (catalog-section)
	if ($("#slider-range").length > 0) {

		$("#slider-range").slider({
			range: true,
			min: 0,
			max: 500,
			values: [ 75, 300 ],
			slide: function( event, ui ) {
				$('#range-from').val(ui.values[0]);
				$('#range-to').val(ui.values[1]);
			}
		});

		$('#range-from').val($("#slider-range").slider("values", 0));
		$('#range-to').val($("#slider-range").slider("values", 1));

		$('#range-from').on('change', function() {
			var to = $( "#slider-range" ).slider('values', 1);
			var min = $( "#slider-range" ).slider("option", "min");
			var value = $(this).val();

			if (value > to) {
				$(this).val(to);
				value = to;
			} else if (value < min) {
				$(this).val(min);
				value = min;
			}

			$( "#slider-range" ).slider('values', 0, value);

		});

		$('#range-to').on('change', function() {
			var from = $( "#slider-range" ).slider('values', 0);
			var max = $( "#slider-range" ).slider("option", "max");
			var value = $(this).val();

			if (value < from) {
				$(this).val(from);
				value = from;
			} else if (value > max) {
				$(this).val(max);
				value = max;
			}

			$( "#slider-range" ).slider('values', 1, value);

		});
	}
// catalog filter range slider

// show more section-description (catalog-section)
	$('.catalog-section-desc__more span').click(function() {
		$('.catalog-section-desc__txt').addClass('extended');
	});
// show more section-description

// share block (catalog-section)
	$('.js-share-btn').click(function() {
		var parent = $(this).closest('.pages-nav');
		var shareBlock = parent.find('.share-block');

		if (shareBlock.hasClass('shown')) {
			shareBlock.fadeOut(200, function() {
				shareBlock.removeClass('shown');
			});

		} else {
			shareBlock.fadeIn(200, function() {
				shareBlock.addClass('shown');
			});
		}
	});

	$(document).click(function(event) {
	    if ($(event.target).closest('.js-share-btn').length ||
	    	$(event.target).closest('.share-block').length) return;
	    $('.share-block').fadeOut(200, function() {
	    	$('.share-block').removeClass('shown');
	    });
	    event.stopPropagation();
	});
// share block

// top slider (catalog-element)
	if ($('.product-images').length > 0) {
		var gallery = new Swiper('.product-images', {
			spaceBetween: 0,
			loop:true,
			pagination: {
				el: '.product-gallery__pagination',
				clickable: true
			}
		});
	}
// top slider

// video lazy load (catalog-element)
	var modalVideo = (function() {
		 var prepare = function(video) {
			var link = video.querySelector('.video__link');
			var media = video.querySelector('.video__media');
			var button = video.querySelector('.video__button');
			var id = parseMediaURL(media);

			video.addEventListener('click', function(event) {
				var iframe = createIframe(id);

				link.remove();
				button.remove();
				video.appendChild(iframe);
				event.stopPropagation();
			});

			link.removeAttribute('href');
			video.classList.add('video--enabled');
		}
		var parseMediaURL = function(media) {
			var regexp = /https:\/\/i\.ytimg\.com\/vi\/([a-zA-Z0-9_-]+)\/maxresdefault\.jpg/i;
			var url = media.src;
			var match = url.match(regexp);

			return match[1];
		}
		var createIframe = function(id) {
			var iframe = document.createElement('iframe');

			iframe.setAttribute('allowfullscreen', '');
			iframe.setAttribute('allow', 'autoplay');
			iframe.setAttribute('src', generateURL(id));
			iframe.classList.add('video__media');

			return iframe;
		}
		var generateURL = function(id) {
			var query = '?rel=0&showinfo=0&enablejsapi=1&autoplay=1';

			return 'https://www.youtube.com/embed/' + id + query;
		}
		return {
			prep: prepare
		}
	})();

	$('.js-show-video').click(function() {
		$('.gallery-modal-window .video').remove();
		var button = '<button class="video__button" area-label="Запустить видео">';
		button += '<svg width="68" height="48" viewBox="0 0 68 48">';
		button += '<path class="video__button-shape" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path class="video__button-icon" d="M 45,24 27,14 27,34"></path>';
		button += '</svg>';
		button += '</button>';

		var code = $(this).data('video');

		var picture = '<a class="video__link" href="https://youtu.be/'+code+'">';
		picture += '<picture>';
		picture += '<source srcset="https://i.ytimg.com/vi_webp/'+code+'/maxresdefault.webp" type="image/webp">';
		picture += '<img class="video__media" src="https://i.ytimg.com/vi/'+code+'/maxresdefault.jpg" alt="">';
		picture += '</picture>';
		var inner = '<div class="gallery-modal-video video">'+picture+button+'</div>';

		$('.gallery-modal-window').append(inner);

		modalVideo.prep(document.querySelector('.gallery-modal-window .video'));

		$('#gallery-modal').fadeIn();
	});
	$(document).click(function(event) {
	    if ($(event.target).closest('.js-show-video').length ||
	    	$(event.target).closest('.gallery-modal-window').length) return;
		$('.gallery-modal-window .video').remove();
	    $('#gallery-modal').fadeOut();
	    event.stopPropagation();
	});
	$('#gallery-modal-close').click(function() {
		$('.gallery-modal-window .video').remove();
	    $('#gallery-modal').fadeOut();
	});
// video lazy load

// show product detail (catalog-element)
	$('.js-detail-show').click(function() {
		var header = $(this);
		var parent = header.closest('.product-detail__block');
		var content = parent.find('.product-detail__block-content');

		$('.js-detail-show').not($(this))
							.find('.fa')
							.removeClass('fa-angle-up')
							.addClass('fa-angle-down')
							.closest('.product-detail__block')
							.removeClass('opened')
							.find('.product-detail__block-content')
							.slideUp();

		if (parent.hasClass('opened')) {
			content.slideUp(function() {
				parent.removeClass('opened');
				header.find('.fa').removeClass('fa-angle-up');
				header.find('.fa').addClass('fa-angle-down');
			});
		} else {
			content.slideDown(function() {
				parent.addClass('opened');
				header.find('.fa').removeClass('fa-angle-down');
				header.find('.fa').addClass('fa-angle-up');
				$('html,body').animate({
					scrollTop: $(this).offset().top - 50
				}, 500);
			});
		}

	});
// show product detail

// show more (catalog-element)
	$('.product-excerpt__more').click(function() {
		$('.product-detail__block')
			.removeClass('opened')
			.find('.product-detail__block-content')
			.css('display', '');

		var more = $('.product-detail__block').first();
		if (!more.hasClass('opened'))
			more.addClass('opened');

		$('html,body').animate({
			scrollTop: more.offset().top - 10
		}, 500);
	});
// show more

// products cards buttons (catalog-element)
	$('.product__buy').click(function() {
		$(this).addClass("done").find('button').attr('disabled', 'disabled');
	});
	$('.product__more .favorites, .product__more .compare').click(function() {
		$(this).toggleClass("done");
	});
// products cards buttons

// sell amount increment (catalog-element)
	$('.js-amount-plus').click(function() {
		var amount = $('.product-amount__input').val();
		$('.product-amount__input').val(+amount + 1);
	});
// sell amount increment

// quick order mask (catalog-element)
	$("#quick-order").inputmask("+38 (099) 999-99-99");
// quick order mask

// faq accordion (faq)
	$('.faq-item__question').click(function() {
		var parent = $(this).closest('.faq-item');
		if (parent.hasClass('opened')) {
			parent.find('.faq-item__answer').slideUp(function() {
				parent.removeClass ('opened')
						.find('.fa')
						.removeClass('fa-angle-up')
						.addClass('fa-angle-down');
			});
		} else {
			parent.find('.faq-item__answer').slideDown(function() {
				parent.addClass ('opened')
						.find('.fa')
						.removeClass('fa-angle-down')
						.addClass('fa-angle-up');
			});
		}
	});
// faq accordion

// pop-up (Задать вопрос) (faq)
	$('.js-ask-question').click(function() {
		$('#ask-question').show();
		$('#ask-question span').css('display', '');
		$('#ask-question').find('input, textarea').each(function() {
			$(this).val('');
		});
	});

	$(document).click(function(event) {
	    if ($(event.target).closest('.js-ask-question').length ||
	    	$(event.target).closest('.ask-question-window').length) return;
	    $('#ask-question').hide();
	    event.stopPropagation();
	});

	$('#ask-question-form').submit(function(e) {
		e.preventDefault();
		$(this).find('input, textarea').each(function() {
			$(this).val('');
		});
		$('#ask-question').hide();
	});

	$('#ask-question-close').click(function() {
		$('#ask-question').hide();
	});

	$('#ask-question input, #ask-question textarea').on('change', function() {
		if($(this).val() != '') {
			$(this).next('span').hide();
		} else {
			$(this).next('span').show();
		}
	});
// pop-up

// about delivery (pay-and-delivery)
	$('.js-delivery-about').click(function() {
		var parent = $(this).closest('.delivery-detail-item');
		parent.find('.delivery-detail-item__more').hide();
		parent.find('.delivery-detail-item__content').slideDown();
	});
// about delivery


// compare products (compare)
	if ($('.catalog-compare__products').length > 0) {
		var compareSwiper = new Swiper('.catalog-compare__products', {
			spaceBetween: 30,
			speed: 400,
			slidesPerView: 2,
			grabCursor: true,
			autoHeight: true,
			scrollbar: {
				el: '.swiper-scrollbar',
				hide: false,
				draggable: true
			},
		});

      	$('.catalog-compare-item__del').click(function() {
			compareSwiper.removeSlide(compareSwiper.clickedIndex);
		});

		$('.js-compare__clear').click(function() {
			compareSwiper.removeAllSlides();
			$('.catalog-compare-container').html('Нет продуктов, выбраных для сравенения');
		});

		$('.campare-show-all, .campare-show-diff').click(function() {
			var target = $(this);
			var parent = target.closest('.catalog-compare__control-group');

			$('.ctrl-btn', parent).removeClass('selected');
			target.addClass('selected');

			if (target.hasClass('campare-show-all')) {

			} else {

			}
		});
	}
// compare products


// side menu (...)
	$('.js-side-menu').click(function() {
		var el = $(this);
		$('.js-side-menu').not(el).next('.side-menu__sub').slideUp(200, function() {
			$('.js-side-menu').removeClass('opened');
			if (el.hasClass('opened')) {
				el.next('.side-menu__sub').slideUp(200, function() {
					el.removeClass('opened');
				});
			} else {
				el.next('.side-menu__sub').slideDown(200, function() {
					el.addClass('opened');
				});
			}
		});
	});
// side menu
});