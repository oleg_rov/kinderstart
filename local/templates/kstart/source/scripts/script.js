$(function () {
// fixing height content (all)
	function fixContentHeight() {
		var w_h = $(window).height();
		var h_h = $('.header').outerHeight();
		var f_h = $('.footer').outerHeight();
		var m_h = $('main').removeAttr('style').height();
		var diff = w_h - (h_h + f_h + m_h);
		if(diff > 0) {
			$('main').height(m_h + diff);
		}
	}
	fixContentHeight();

	$(window).resize(fixContentHeight);
// fixing height content

// categories menu (all)
	$('.catalog-menu-button').click(function() {
		$(this).toggleClass('opened');
		if($(this).hasClass('opened')) {
			$('.catalog-menu-block').slideDown();
			$('.catalog-menu-bg').show();
		} else {
			$('.catalog-menu-block').slideUp();
			$('.catalog-menu-bg').hide();
		}
	});

	$(document).click(function(event) {
		if ($(event.target).closest('.catalog-menu-block').length ||
			$(event.target).closest('.catalog-menu-button').length) return;
	    $('.catalog-menu-block').slideUp();
		$('.catalog-menu-button').removeClass('opened');
	    event.stopPropagation();
	});
// categories menu

	$(document).click(function(event) {
		if ($(event.target).closest('.catalog-menu-block').length ||
			$(event.target).closest('.catalog-menu-button').length ||
			$(event.target).closest('.js-cats-filters').length ||
			$(event.target).closest('.shift-side-block').length) return;

		$('.catalog-menu-bg').hide();
	    event.stopPropagation();
	});

// top banner (index)
	if ($('.top-banner').length > 0 ) {
		var topBannerSwiper = new Swiper('.top-banner', {
			pagination: {
		      el: '.swiper-pagination',
		      clickable: true
		    },
		    loop: true,
		    speed: 600,
			autoplay: {
				delay: 5000,
			},
		});
	}
// top banner

// portfolio slider (index)
	if ($('.portfolio-items').length > 0 ) {
		var portfolioSwiper = new Swiper('.portfolio-items', {
		    spaceBetween: 30,
		    speed: 400,
		    slidesPerView: 2,
		    breakpointsInverse: true,
		    navigation: {
				nextEl: '.swiper-buttons .next-portfolio',
				prevEl: '.swiper-buttons .prev-portfolio',
			},
			breakpoints: {
				768: {
					slidesPerView: 2,
				},
				1024: {
					slidesPerView: 3,
				},
				1440: {
					slidesPerView: 4,
				}
			}
		});
	}
// portfolio slider

// recommended-block slider (index)
function recommendedSlider(name) {
	return new Swiper('.catalog-block[data-target="'+name+'"]', {
	    spaceBetween: 30,
	    speed: 400,
	    slidesPerView: 3,
	    autoHeight: true,
	    breakpointsInverse: true,
	    navigation: {
	    	nextEl: '.swiper-buttons .next-product.'+name,
			prevEl: '.swiper-buttons .prev-product.'+name,
	    },
	    breakpoints: {
			768: {
				slidesPerView: 3,
			},
			1024: {
				slidesPerView: 4,
			},
			1500: {
				slidesPerView: 5,
			}
		}
	});
}
$('.recommended-block .swiper-buttons>div').addClass('discount');
recommendedSlider('discount');

if ($('.catalog-block').length > 0 ) {
	var recommendedSliders = {};
	$('.catalog-block').each(function() {
		recommendedSliders[$(this).data('target')] = null;
	});

	var target = $('.catalog-block:first').data('target');
	$('.recommended-block .swiper-buttons>div').addClass(target);
	recommendedSliders[target] = recommendedSlider(target);

	$('.recommended-block .catalog-tab').click(function() {
		$('.recommended-block .catalog-tab').removeClass('active');
		$(this).addClass('active');
		var code = $(this).data('code');
		var sliders = Object.keys(recommendedSliders).join(' ');
		$('.recommended-block .swiper-buttons>div').removeClass(sliders).addClass(code);
		$('.catalog-block').each(function() {
			if ($(this).is('[data-target="'+code+'"]')) {
				$(this).show();
				recommendedSliders[code] = recommendedSlider(code);
			} else {
				$(this).hide();
			}
		});
	});
}
// recommended-block slider

// user menu in heder catalog menu < 1440px (all)
	$('.js-customer-menu').click(function() {
		$('.customer-menu-block').toggleClass('opened');
		if($('.customer-menu-block').hasClass('opened')) {
			$('.customer-menu-block__content').slideDown();
			$(this).find('.fa').removeClass('fa-angle-down');
			$(this).find('.fa').addClass('fa-angle-up');
		} else {
			$('.customer-menu-block__content').slideUp();
			$(this).find('.fa').removeClass('fa-angle-up');
			$(this).find('.fa').addClass('fa-angle-down');
		}
	});
// user menu in heder catalog menu

// products cards buttons (index, catalog-section)
	$('.catalog-item__buy').click(function() {
		$(this).addClass("done").find('button').attr('disabled', 'disabled');
	});
	$('.catalog-item__more .favorites, .catalog-item__more .compare').click(function() {
		$(this).toggleClass("done");
	});
// products cards buttons

// header search (all)
	$('.js-search-btn').click(function() {
		$(this).toggleClass('opened');
		if($(this).hasClass('opened')) {
			$('.header-search').slideDown();
		} else {
			$('.header-search').slideUp();
		}
	});

	$(document).click(function(event) {
		if ($(event.target).closest('.js-search-btn').length ||
			$(event.target).closest('.header-search').length) return;
		$('.header-search').slideUp(200, function() {
			$('.top-header-btn').removeClass('opened');
		});

	});

	$('#header-search-form').submit(function(e) {
		e.preventDefault();
		$(this).find('input').val('');
		$('.header-search').slideUp();
		$('.js-search-btn').removeClass('opened');
		$('.header-search__input-placeholder').show();
	});

	$('#header-search-form input').on('change', function() {
		if($(this).val() != '') {
			$(this).next('span').hide();
		} else {
			$(this).next('span').show();
		}
	});
// header search

// top-menu (покупатели, контакты) (all)
	function toggleExtMenu() {
		var parent = $(this).closest('.ext-btn-block');
		parent.toggleClass('opened');
		if(parent.hasClass('opened')) {
			parent.find('.ext-dropdown').slideDown();
			parent.find('.brief-ext__btn i').removeClass('fa-angle-down');
			parent.find('.brief-ext__btn i').addClass('fa-angle-up');
		} else {
			parent.find('.ext-dropdown').slideUp();
			parent.find('.brief-ext__btn i').removeClass('fa-angle-up');
			parent.find('.brief-ext__btn i').addClass('fa-angle-down');
		}
	}
	$('.js-cont-btn').click(toggleExtMenu);
	$('.js-u-menu-btn').click(toggleExtMenu);

	$(document).click(function(event) {
		if ($(event.target).closest('.js-cont-btn').length ||
			$(event.target).closest('.ext-dropdown').length) return;
		var parent = $('.js-cont-btn').closest('.ext-btn-block').removeClass('opened');
	    parent.find('.ext-dropdown').slideUp();
	    parent.find('.brief-ext__btn i').removeClass('fa-angle-up');
		parent.find('.brief-ext__btn i').addClass('fa-angle-down');
	    event.stopPropagation();
	});

	$(document).click(function(event) {
		if ($(event.target).closest('.js-u-menu-btn').length ||
			$(event.target).closest('.ext-dropdown').length) return;
		var parent = $('.js-u-menu-btn').closest('.ext-btn-block').removeClass('opened');
	    parent.find('.ext-dropdown').slideUp();
	    parent.find('.brief-ext__btn i').removeClass('fa-angle-up');
		parent.find('.brief-ext__btn i').addClass('fa-angle-down');
	    event.stopPropagation();
	});
// top-menu (контакты)

// auth pop-up (all)
	$('.js-auth-btn').click(function() {
		$('#login-modal').show();
	});
	$(document).click(function(event) {
	    if ($(event.target).closest('.js-auth-btn').length ||
	    	$(event.target).closest('.login-modal-window').length) return;
	    $('#login-modal').hide();
	    event.stopPropagation();
	});

	$('#login-modal-form').submit(function(e) {
		e.preventDefault();
		$(this).find('input, textarea').each(function() {
			$(this).val('');
		});
		$('#login-modal').hide();
	});

	$('#login-modal-close').click(function() {
		$('#login-modal').hide();
	});

	$('#login-modal input').on('change', function() {
		if($(this).val() != '') {
			$(this).next('span').hide();
		} else {
			$(this).next('span').show();
		}
	});
// auth pop-up

// feedback-form (contacts)
	function ff() {
		$('.js-ff').each(function() {
			if($(this).val() != '') {
				$(this).next('span').hide();
			} else {
				$(this).next('span').show();
			}
		});
	}
	ff();
	$('.js-ff').on('change', ff);
// feedback-form

// pop-up (сообщить о наличии) (index catalog-section)
	$('.js-avail-report').click(function() {
		$('#avail-report').show();
	});

	$(document).click(function(event) {
	    if ($(event.target).closest('.js-avail-report').length ||
	    	$(event.target).closest('.avail-report-window').length) return;
	    $('#avail-report').hide();
	    event.stopPropagation();
	});

	$('#avail-report-form').submit(function(e) {
		e.preventDefault();
		$(this).find('input, textarea').each(function() {
			$(this).val('');
		});
		$('#avail-report').hide();
	});

	$('#avail-report-close').click(function() {
		$('#avail-report').hide();
	});

	$('#avail-report input').on('change', function() {
		if($(this).val() != '') {
			$(this).next('span').hide();
		} else {
			$(this).next('span').show();
		}
	});
// pop-up (сообщить о наличии)

// show more on about shop section (index)
	$('.about-shop-more span').click(function() {
		$('.about-shop-content').addClass('extended');
	});
// show more on about shop section

// lang buttons (all)
	$('.js-switch-lang a').click(function(e) {
		e.preventDefault();
		var href = $(this).attr('href');
		var lang = $(this).data('lang');
		document.cookie = "lang=" + lang;
		location.href = href;
	});
// lang buttons

// social section (index)
	function showTwoLines() {
		var items = $('.our-soc-item');
		items.show();
		var item = $('.our-soc-item').outerWidth();
		var container = $('.our-soc-block .container').width();
		var visibleCnt = Math.floor(container / item) * 2;
		items.each(function(i) {
			if(i >= visibleCnt) {
				$(items[i]).hide();
			}
		});
	}

	function showMoreLines() {
		var hidden = $('.our-soc-item:hidden');
		var item = $('.our-soc-item').outerWidth();
		var container = $('.our-soc-block .container').width();
		var addCnt = Math.floor(container / item) * 2;
		hidden.each(function(i) {
			if(i < addCnt) {
				$(hidden[i]).show();
			}
		});

		if ($('.our-soc-item:hidden').length < 1) {
			$('.js-more-soc').hide();
		}
	}

	$('.js-more-soc').click(showMoreLines);

	showTwoLines();
	$(window).resize(showTwoLines);
// social section

// do equal height (catalog-section)
	function doEqualHeight() {
		if ($('.js-equal-height').length > 0) {
			var height = 0;
			$('.js-equal-height').css('height', '');

			$('.js-equal-height').each(function(i, el) {

				var elemHeight = $(el).height();
				if(elemHeight>height) height = elemHeight;

			});
			$('.js-equal-height').height(height);
		}
	}

	doEqualHeight();
	$(window).resize(doEqualHeight);
// do equal height

// sort button (catalog-section)
	if ($('.js-sort-dir').length > 0) {

		$('.js-sort-dir').click(function(e) {
			var sortList = $(this).closest('.catalog-section-sort')
							.find('.catalog-section-sort__drop-box');
			console.log(sortList);
			if (sortList.hasClass('opened')) {
				sortList.fadeOut(200, function() {
					sortList.removeClass('opened');
				});
			} else {
				sortList.fadeIn(200, function() {
					sortList.addClass('opened');
				});
			}
		});

		$(document).click(function(event) {
		    if ($(event.target).closest('.catalog-section-sort__drop-box').length || $(event.target).closest('.js-sort-dir').length) return;
		    $('.catalog-section-sort__drop-box').fadeOut(200, function() {
		    	$(this).removeClass('opened');
		    });
		    event.stopPropagation();
		});
	}
// sort button

// categories menu (catalog-section)
	$('.js-multy-menu').click(function() {
		var el = $(this);

		if (el.closest('.v-multy-menu__item-cont').hasClass('opened')) {
			el.closest('.v-multy-menu__item')
				.find('.v-multy-menu__sub')
				.slideUp(200, function() {
					el.closest('.v-multy-menu__item-cont').removeClass('opened');
			});
		} else {
			el.closest('.v-multy-menu__item')
				.find('.v-multy-menu__sub')
				.slideDown(200, function() {
					el.closest('.v-multy-menu__item-cont').addClass('opened');
			});
		}

		// $('.js-multy-menu').not(el)
		// .each(function(i) {
		// 	$(this).closest('.v-multy-menu__item')
		// 		.find('.v-multy-menu__sub')
		// 		.slideUp(200)
		// 		.closest('.v-multy-menu__item-cont')
		// 		.removeClass('opened');
		// });

		var others = $('.js-multy-menu').not(el);
		others.each(function(i) {
			console.log($(others[i]));
			$(others[i]).closest('.v-multy-menu__item')
			.find('.v-multy-menu__sub')
			.slideUp(200);

			$(others[i]).closest('.v-multy-menu__item-cont')
			.removeClass('opened');
		});
	});
// categories menu

// catalog filter (catalog-section)
	$('.js-cats-filter-toggler').click(function() {
		var parent = $(this).parent('.cats-filter-prop');
		if (parent.hasClass('opened')) {
			parent.find('.cats-filter-prop__cont').slideUp(200, function() {
				parent.removeClass('opened')
					.find('.fa')
					.removeClass('fa-angle-up')
					.addClass('fa-angle-down');
			});
		} else {
			parent.find('.cats-filter-prop__cont').slideDown(200, function() {
				parent.addClass('opened')
					.find('.fa')
					.removeClass('fa-angle-down')
					.addClass('fa-angle-up');
			});
		}
	});

	$('.js-cats-filters').click(function() {
		var filter = $('.shift-side-block');
		var height = filter.height();
		var docHeight = $(document).height();

		if (filter.hasClass('opened')) {
			filter.slideUp(200, function() {
				filter.removeClass('opened');
				filter.css('height', '');
				$('.catalog-menu-bg').hide();
			});
		} else {
			filter.slideDown(200, function() {
				filter.addClass('opened');
				filter.css('height', docHeight - 80 + 'px');
				$('.catalog-menu-bg').show();
			});
		}
	});

	$('.shift-side-block__close').click(function() {
		$('.js-cats-filters').trigger('click');
	});

	$(document).click(function(event) {
		if ($(window).width() < 1200) {
			if ($(event.target).closest('.js-cats-filters').length ||
				$(event.target).closest('.shift-side-block').length) return;
		    $('.shift-side-block').css('height', '').fadeOut();
			$('.shift-side-block').removeClass('opened');
		    event.stopPropagation();
		}
	});

	$(window).resize(function() {
		if ($(window).width() >= 1200) {
			$('.shift-side-block').css('display', '').css('height', '');
			$('.shift-side-block').removeClass('opened');
			$('.catalog-menu-bg').css('display', '');
		}
	});

	$('#cats-filter-form').submit(function(e) {
		e.preventDefault();
		if ($(window).width() < 1200) {
			$('.js-cats-filters').trigger('click');
		}
	});

	$('#cats-filter-form').on('reset', function(e) {
		var min = $( "#slider-range" ).slider("option", "min");
		var max = $( "#slider-range" ).slider("option", "max");

		$( "#slider-range" ).slider('values', 0, min);
		$( "#slider-range" ).slider('values', 1, max);
	});
// catalog filter

// catalog filter range slider (catalog-section)
	if ($("#slider-range").length > 0) {

		$("#slider-range").slider({
			range: true,
			min: 0,
			max: 500,
			values: [ 75, 300 ],
			slide: function( event, ui ) {
				$('#range-from').val(ui.values[0]);
				$('#range-to').val(ui.values[1]);
			}
		});

		$('#range-from').val($("#slider-range").slider("values", 0));
		$('#range-to').val($("#slider-range").slider("values", 1));

		$('#range-from').on('change', function() {
			var to = $( "#slider-range" ).slider('values', 1);
			var min = $( "#slider-range" ).slider("option", "min");
			var value = $(this).val();

			if (value > to) {
				$(this).val(to);
				value = to;
			} else if (value < min) {
				$(this).val(min);
				value = min;
			}

			$( "#slider-range" ).slider('values', 0, value);

		});

		$('#range-to').on('change', function() {
			var from = $( "#slider-range" ).slider('values', 0);
			var max = $( "#slider-range" ).slider("option", "max");
			var value = $(this).val();

			if (value < from) {
				$(this).val(from);
				value = from;
			} else if (value > max) {
				$(this).val(max);
				value = max;
			}

			$( "#slider-range" ).slider('values', 1, value);

		});
	}
// catalog filter range slider

// show more section-description (catalog-section)
	$('.catalog-section-desc__more span').click(function() {
		$('.catalog-section-desc__txt').addClass('extended');
	});
// show more section-description

// share block (catalog-section)
	$('.js-share-btn').click(function() {
		var parent = $(this).closest('.pages-nav');
		var shareBlock = parent.find('.share-block');

		if (shareBlock.hasClass('shown')) {
			shareBlock.fadeOut(200, function() {
				shareBlock.removeClass('shown');
			});

		} else {
			shareBlock.fadeIn(200, function() {
				shareBlock.addClass('shown');
			});
		}
	});

	$(document).click(function(event) {
	    if ($(event.target).closest('.js-share-btn').length ||
	    	$(event.target).closest('.share-block').length) return;
	    $('.share-block').fadeOut(200, function() {
	    	$('.share-block').removeClass('shown');
	    });
	    event.stopPropagation();
	});
// share block


// top slider (catalog-element)
	if ($('.gallery-thumbs').length > 0 &&
		$('.gallery-top').length > 0) {
			var cnt = $('.gallery-top .swiper-slide').length;

			var galleryThumbs = new Swiper('.gallery-thumbs', {
				spaceBetween: 15,
				slidesPerView: (cnt >= 5) ? 5 : cnt,
				direction: 'vertical',
				loop: true,
				freeMode: true,
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
			});
			var galleryTop = new Swiper('.gallery-top', {
				spaceBetween: 0,
				loop:true,
				navigation: {
					nextEl: '.product-gallery-next',
					prevEl: '.product-gallery-prev',
				},
				pagination: {
					el: '.product-gallery__pagination',
					clickable: true
				},
				thumbs: {
					swiper: galleryThumbs,
				},
			});

			if(cnt < 3) {
				$('.swiper-container.gallery-thumbs').css('visibility', 'hidden');
			}
	}
// top slider

// video lazy load (catalog-element)
	var modalVideo = (function() {
		 var prepare = function(video) {
			var link = video.querySelector('.video__link');
			var media = video.querySelector('.video__media');
			var button = video.querySelector('.video__button');
			var id = parseMediaURL(media);

			video.addEventListener('click', function(event) {
				var iframe = createIframe(id);

				link.remove();
				button.remove();
				video.appendChild(iframe);
				event.stopPropagation();
			});

			link.removeAttribute('href');
			video.classList.add('video--enabled');
		}
		var parseMediaURL = function(media) {
			var regexp = /https:\/\/i\.ytimg\.com\/vi\/([a-zA-Z0-9_-]+)\/maxresdefault\.jpg/i;
			var url = media.src;
			var match = url.match(regexp);

			return match[1];
		}
		var createIframe = function(id) {
			var iframe = document.createElement('iframe');

			iframe.setAttribute('allowfullscreen', '');
			iframe.setAttribute('allow', 'autoplay');
			iframe.setAttribute('src', generateURL(id));
			iframe.classList.add('video__media');

			return iframe;
		}
		var generateURL = function(id) {
			var query = '?rel=0&showinfo=0&enablejsapi=1&autoplay=1';

			return 'https://www.youtube.com/embed/' + id + query;
		}
		return {
			prep: prepare
		}
	})();

	$('.js-show-video').click(function() {
		$('.gallery-modal-window .video').remove();
		var button = '<button class="video__button" area-label="Запустить видео">';
		button += '<svg width="68" height="48" viewBox="0 0 68 48">';
		button += '<path class="video__button-shape" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path class="video__button-icon" d="M 45,24 27,14 27,34"></path>';
		button += '</svg>';
		button += '</button>';

		var code = $(this).data('video');

		var picture = '<a class="video__link" href="https://youtu.be/'+code+'">';
		picture += '<picture>';
		picture += '<source srcset="https://i.ytimg.com/vi_webp/'+code+'/maxresdefault.webp" type="image/webp">';
		picture += '<img class="video__media" src="https://i.ytimg.com/vi/'+code+'/maxresdefault.jpg" alt="">';
		picture += '</picture>';
		var inner = '<div class="gallery-modal-video video">'+picture+button+'</div>';

		$('.gallery-modal-window').append(inner);

		modalVideo.prep(document.querySelector('.gallery-modal-window .video'));

		$('#gallery-modal').fadeIn();
	});
	$(document).click(function(event) {
	    if ($(event.target).closest('.js-show-video').length ||
	    	$(event.target).closest('.gallery-modal-window').length) return;
		$('.gallery-modal-window .video').remove();
	    $('#gallery-modal').fadeOut();
	    event.stopPropagation();
	});
	$('#gallery-modal-close').click(function() {
		$('.gallery-modal-window .video').remove();
	    $('#gallery-modal').fadeOut();
	});
// video lazy load

// sell amount increment (catalog-element)
	$('.js-amount-plus').click(function() {
		var amount = $('.product-amount__input').val();
		$('.product-amount__input').val(+amount + 1);
	});
// sell amount increment

// products cards buttons (catalog-element)
	$('.product__buy').click(function() {
		$(this).addClass("done").find('button').attr('disabled', 'disabled');
	});
	$('.product__more .favorites, .product__more .compare').click(function() {
		$(this).toggleClass("done");
	});
// products cards buttons

// datails product tabs (catalog-element)
	$('.product-detail .product-detail__tab').click(function() {
		var target = $(this);
		var content = target.data('content');

		$('.product-detail__elem').removeClass('shown');
		$('#'+content).addClass('shown');

		$('.product-detail .product-detail__tab').removeClass('active');
		target.addClass('active');
	});
// datails product tabs

// show more (catalog-element)
	$('.product-excerpt__more').click(function() {
		$('.product-detail .product-detail__tab').removeClass('active');
		$('.product-detail .product-detail__tab[data-content="first-tab"]').addClass('active');
		$('.product-detail__elem').removeClass('shown');
		var target = $('#first-tab');
		target.addClass('shown');
		$('html,body').animate({
			scrollTop: target.offset().top - 100
		}, 500);
	});
// show more

// buy with slider (catalog-element)
	if ($('.product-with-items').length > 0 ) {
		var buyWithSwiper = new Swiper('.product-with-items', {
		    spaceBetween: 30,
		    speed: 400,
		    slidesPerView: 3,
		    autoHeight: true,
		    breakpointsInverse: true,
		    navigation: {
		    	nextEl: '.swiper-buttons .next-with',
				prevEl: '.swiper-buttons .prev-with',
		    },
		    breakpoints: {
				768: {
					slidesPerView: 3,
				},
				1024: {
					slidesPerView: 4,
				},
				1500: {
					slidesPerView: 5,
				}
			}
		});
	}
// buy with slider

// series products slider
	if ($('.product-series').length > 0 ) {
		var seriesSwiper = new Swiper('.product-series', {
		    spaceBetween: 30,
		    speed: 400,
		    slidesPerView: 1,
		    autoHeight: true,
		    breakpointsInverse: true,
		    navigation: {
		    	nextEl: '.swiper-buttons .next-series',
				prevEl: '.swiper-buttons .prev-series',
		    },
		    breakpoints: {
				1060: {
					slidesPerView: 2,
				},
				1400: {
					slidesPerView: 1,
				},
				1440: {
					slidesPerView: 2,
				},
				1600: {
					slidesPerView: 3,
				}
			}
		});
	}
// /series products slider

// quick order mask (catalog-element)
	$("#quick-order").inputmask("+38 (099) 999-99-99");
// quick order mask


// news slider (news-element)
	if ($('.news-block').length > 0 ) {
		var newsSwiper = new Swiper('.news-block', {
		    spaceBetween: 30,
		    speed: 400,
		    slidesPerView: 2,
		    autoHeight: true,
		    breakpointsInverse: true,
		    navigation: {
		    	nextEl: '.swiper-buttons .next-news',
				prevEl: '.swiper-buttons .prev-news',
		    },
		    breakpoints: {
				768: {
					slidesPerView: 2,
				},
				1024: {
					slidesPerView: 3,
				}
			}
		});
	}
// news slider

// faq accordion (faq)
	$('.faq-item__question').click(function() {
		var parent = $(this).closest('.faq-item');
		if (parent.hasClass('opened')) {
			parent.find('.faq-item__answer').slideUp(function() {
				parent.removeClass ('opened')
						.find('.fa')
						.removeClass('fa-angle-up')
						.addClass('fa-angle-down');
			});
		} else {
			parent.find('.faq-item__answer').slideDown(function() {
				parent.addClass ('opened')
						.find('.fa')
						.removeClass('fa-angle-down')
						.addClass('fa-angle-up');
			});
		}
	});
// faq accordion

// pop-up (Задать вопрос) (faq)
	$('.js-ask-question').click(function() {
		$('#ask-question').show();
		$('#ask-question span').css('display', '');
		$('#ask-question').find('input, textarea').each(function() {
			$(this).val('');
		});
	});

	$(document).click(function(event) {
	    if ($(event.target).closest('.js-ask-question').length ||
	    	$(event.target).closest('.ask-question-window').length) return;
	    $('#ask-question').hide();
	    event.stopPropagation();
	});

	$('#ask-question-form').submit(function(e) {
		e.preventDefault();
		$(this).find('input, textarea').each(function() {
			$(this).val('');
		});
		$('#ask-question').hide();
	});

	$('#ask-question-close').click(function() {
		$('#ask-question').hide();
	});

	$('#ask-question input, #ask-question textarea').on('change', function() {
		if($(this).val() != '') {
			$(this).next('span').hide();
		} else {
			$(this).next('span').show();
		}
	});
// pop-up (сообщить о наличии)

// about delivery (pay-and-delivery)
	$('.js-delivery-about').click(function() {
		var parent = $(this).closest('.delivery-detail-item');
		parent.find('.delivery-detail-item__more').hide();
		parent.find('.delivery-detail-item__content').slideDown();
	});
// about delivery

// fancybox (certificates)
	if ($('[data-fancybox="certif-gallery"]').length > 0 ) {
		$('[data-fancybox="certif-gallery"]').fancybox();
	}
// fancybox

// compare products (compare)
	if ($('.catalog-compare__products').length > 0) {
		var compareSwiper = new Swiper('.catalog-compare__products', {
			spaceBetween: 30,
			speed: 400,
			slidesPerView: 2,
			breakpointsInverse: true,
			grabCursor: true,
			autoHeight: true,
			scrollbar: {
				el: '.swiper-scrollbar',
				hide: false,
				draggable: true
			},
			breakpoints: {
				768: {
					slidesPerView: 2,
				},
				1024: {
					slidesPerView: 3,
				}
			}
		});

      	$('.catalog-compare-item__del').click(function() {
			compareSwiper.removeSlide(compareSwiper.clickedIndex);
		});


		$('.js-compare__clear').click(function() {
			compareSwiper.removeAllSlides();
			$('.catalog-compare__products').html('Нет продуктов, выбраных для сравенения');
		});

		function getNamesProps() {
			if ($('.catalog-compare-item').length > 0) {
				var props = $('.catalog-compare-item__props').eq(0);
				var names = {};
				$('.catalog-compare-item__prop:visible', props).each(function(i, el) {
					names[$(el).data('prop-id')] = $(el).data('prop-name');
				});
				return names;
			}

			return false;
		}

		function showAllProps() {
			if ($('.catalog-compare-item').length > 0) {
				$('.catalog-compare-item__prop').css('display', 'block');

				fillNamesProps();
			}

			return false;
		}

		function isIdDiff() {
			var ids = {};
			$('[data-prop-id]').each(function(i, el) {
				var prodId = $(this).data('prop-id');
				if(ids[prodId] === undefined) {
					ids[prodId] = false;
				}
			});
			for(var k in ids) {
				var str = "";
				var lines = $('[data-prop-id='+k+']');
				for(var i=0; i<lines.length; i++) {
					if(i == 0) {
						str = lines[i].textContent;
					} else {
						if(str != lines[i].textContent) {
							ids[k] = true;
							break;
						}
					}
				}
			}
			return ids;
		}

		$('.campare-show-all').click(function() {
			$('.catalog-compare-item__prop').css({
				'display': '',
				'background-color': ''
			});
			showAllProps();
		});

		$('.campare-show-diff').click(function() {
			$ids = isIdDiff();
			for(var id in $ids) {
				if(!$ids[id]) {
					$('[data-prop-id='+id+']').hide();
				}else{
					$('[data-prop-id='+id+']').show();
				}
			}
			$('.catalog-compare-item__prop').css('background-color', '#fff');
			$('.catalog-compare-item__prop:visible:even').css('background-color', '#f5f5f5');
			fillNamesProps();
		});

		function fillNamesProps() {
			var names = getNamesProps();
			var namesBlock = $('.catalog-compare__prop-names');
			namesBlock.html('');
			for (var n in names) {
				$(namesBlock).append('<div class="catalog-compare__prop-name" data-name-id="'+n+'">'+ names[n] +'</div>');
			}
		}

		fillNamesProps();

		$('.campare-show-all, .campare-show-diff').click(function() {
			var target = $(this);
			var parent = target.closest('.catalog-compare__control-group');

			$('.ctrl-btn', parent).removeClass('selected');
			target.addClass('selected');
		});

	}
// compare products

// footer catalog menu (all)
	$('.footer-catalog-menu').click(function() {
		var btn = this;
		if ($(this).hasClass('opened')) {
			$('.footer-catalog-menu__list').slideUp(200, function() {
				$(btn).removeClass('opened')
			});
		} else {
			$('.footer-catalog-menu__list').slideDown(200, function() {
				$(btn).addClass('opened')
			});
		}
	});

	$(document).click(function() {
		if ($(event.target).closest('.footer-catalog-menu').length) return;
	    $('.footer-catalog-menu__list').slideUp(200);
		$('.footer-catalog-menu').removeClass('opened');
	});
// footer catalog menu

});