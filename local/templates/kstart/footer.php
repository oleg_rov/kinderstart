	</main>

	<footer class="footer">
		<div class="footer-wrapper container">
			<div class="footer-content">
				<div class="footer-main-block">
					<div class="footer-logo">
						<img src="<?=IMGS_DIR?>footer_logo.svg" alt="kinderstart">
					</div>

					<div class="footer-catalog-menu">
						<ul class="footer-catalog-menu__list">
							<li class="footer-catalog-menu__item">
								<a href="/mobile/catalog/list">Спортивные уголки</a>
							</li>
							<li class="footer-catalog-menu__item">
								<a href="/mobile/catalog/list">Спортивные уголки</a>
							</li>
							<li class="footer-catalog-menu__item">
								<a href="/mobile/catalog/list">Спортивные уголки</a>
							</li>
							<li class="footer-catalog-menu__item">
								<a href="/mobile/catalog/list">Спортивные уголки</a>
							</li>
						</ul>
						<div class="footer-catalog-menu__header">
							<span class="footer-catalog-menu__header-icon">
								<svg class="svg-closed">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
					            </svg>
								<svg class="svg-opened">
					                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
					            </svg>
							</span>
							<span class="footer-catalog-menu__header-txt">Каталог товаров</span>
						</div>
					</div>
				</div>
				<div class="footer-other-blocks">
					<div class="footer-section footer-menu">
						<h4 class="footer-menu__header">Компания</h4>
						<ul class="footer-menu__items">
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/certificates/">Сертификаты</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/awards/">Награды и достижения</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/partners/">Наши партнеры</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/reviews/">Отзывы</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/vacancy/">Вакансии</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/contacts/">Реквизиты</a>
							</li>
						</ul>
					</div>
					<div class="footer-section footer-menu">
						<h4 class="footer-menu__header">Помощь покупателю</h4>
						<ul class="footer-menu__items">
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/faq/">Вопросы и ответы</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/discounts/">Акции и скидки</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/pay-and-delivery/">Оплата и доставка</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/credit/">Кредит</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/guarantee/">Гарантия</a>
							</li>
						</ul>
					</div>
					<div class="footer-section footer-menu">
						<h4 class="footer-menu__header">Пресс-центр</h4>
						<ul class="footer-menu__items">
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>news">Новости</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>news">Блог</a>
							</li>
							<li class="footer-menu__item">
								<a href="<?=SITE_DIR?>company/faq/">Вопросы и ответы</a>
							</li>
						</ul>
					</div>
					<div class="footer-section footer-contacts">
						<ul class="footer-menu__items">
							<li class="footer-menu__item footer-menu__item--phone">
								<a href="tel:0443606456">(044) 360-64-56</a>
							</li>
							<li class="footer-menu__item footer-menu__item--phone">
								<a href="tel:0675064209">(067) 506-42-09</a>
							</li>
							<li class="footer-menu__item footer-menu__item--phone">
								<a href="tel:0506660212">(050) 666-02-12</a>
							</li>
						</ul>
						<p class="footer-menu__item footer-menu__item--schedule">ПН — ПТ с 9 до 21</p>
						<p class="footer-menu__item footer-menu__item--male"><a href="mailto:info@sportbaby.ua">info@sportbaby.ua</a></p>
					</div>
				</div>
			</div>

			<div class="footer-social-links">
				<a href="#" area-label="facebook" class="footer-social-link">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
		            </svg>
				</a>
				<a href="#" area-label="twitter" class="footer-social-link">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
		            </svg>
				</a>
				<a href="#" area-label="google plus" class="footer-social-link">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#yt"></use>
		            </svg>
				</a>
				<a href="#" area-label="instagram" class="footer-social-link">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#insta"></use>
		            </svg>
				</a>
			</div>

			<div class="copyrights">
				<span class="copyrights__txt">© 2019 «kinderstart» Все права защищены.</span>
				<span class="copyrights__link"><a href="#">Пользовательское соглашение</a></span>
			</div>
		</div>
	</footer>

	<div class="pop-up avail-report" id="avail-report">
		<div class="pop-up-bg"></div>
		<div class="pop-up-window avail-report-window">
			<button class="pop-up-window__close" id="avail-report-close">
				<svg>
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
	            </svg>
			</button>
			<div class="pop-up-window__title">Сообщить о наличии</div>
			<form id="avail-report-form">
				<div class="pop-up-window__input-group">
					<input type="text" name="u-name" class="pop-up-window__input-field" required>
					<span class="pop-up-window__input-placeholder">Имя</span>
				</div>
				<div class="pop-up-window__input-group">
					<input type="text" name="u-phone" class="pop-up-window__input-field" required>
					<span class="pop-up-window__input-placeholder">Телефон</span>
				</div>
				<div class="pop-up-window__btn-group">
					<div class="avail-report__btn">
						<button class="a-style-btn">Отправить</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="pop-up login-modal" id="login-modal">
		<div class="pop-up-bg"></div>
		<div class="pop-up-window login-modal-window">
			<button class="pop-up-window__close" id="login-modal-close">
				<svg>
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
	            </svg>
			</button>
			<div class="pop-up-window__title">Авторизация</div>
			<form id="login-modal-form">
				<div class="pop-up-window__input-group">
					<input type="text" name="login" class="pop-up-window__input-field" required>
					<span class="pop-up-window__input-placeholder">Логин</span>
				</div>
				<div class="pop-up-window__input-group">
					<input type="password" name="password" class="pop-up-window__input-field" required>
					<span class="pop-up-window__input-placeholder">Пароль</span>
				</div>
				<div class="pop-up-window__btn-group">
					<div class="login-modal__btn">
						<button class="a-style-btn">Войти</button>
					</div>
					<div class="login-modal__forgot-link">
						<a href="#">Забыли пароль?</a>
					</div>
					<div class="login-modal__reg-link">
						<a href="registration.html">Регистрация</a>
					</div>

				</div>
			</form>
		</div>
	</div>
	<script src="<?=MAIN_ASSETS?>build/js/script.min.js" async></script>
</body>
</html>