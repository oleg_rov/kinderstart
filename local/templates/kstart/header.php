<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="<?=MAIN_ASSETS?>build/css/main.min.css">
</head>
<body>
	<header class="header">
		<div class="top-header">
			<button class="catalog-menu-button">
				<svg class="svg-closed">
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
	            </svg>
				<svg class="svg-opened">
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
	            </svg>
			</button>

			<div class="catalog-menu-block">
				<div class="additional-menu-block">
					<div class="additional-menu-user">
						<button class="additional-menu-user__btn js-auth-btn">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#user"></use>
				            </svg>
				            <span>Войти</span>
						</button>
					</div>

					<?php $site_lang = 'RU';
					if (!empty($_COOKIE['lang']) && $_COOKIE['lang'] == 'ua') {
						$site_lang = 'UA';
					} ?>
					<div class="additional-menu-lang js-switch-lang">
						<?if($site_lang=='UA'):?>
						<a href="/" class="top-header-btn additional-menu-lang__btn" data-lang="ru">
							<span>RU</span>
						</a>
						<span class="top-header-btn additional-menu-lang__btn active">
							<span>UA</span>
						</span>
						<?else:?>
						<span class="top-header-btn additional-menu-lang__btn active">
							<span>RU</span>
						</span>
						<a href="/?lang=ua"  class="top-header-btn additional-menu-lang__btn" data-lang="ua">
							<span>UA</span>
						</a>
						<?endif?>
					</div>
				</div>
				<div class="catalog-menu-content">
					<div class="catalog-menu-block__title">
						Категории товара
					</div>
					<nav class="catalog-menu-block__content">
						<ul class="catalog-menu">

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_01.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_01.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Спортивные уголки
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_02.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_02.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Гимнастические маты
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_03.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_03.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Шведская стенка
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_04.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_04.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Детские прыгунки
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_05.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_05.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Детские игровые комплексы
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_06.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_06.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Детские площадки
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_07.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_07.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Парты
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_08.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_08.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Игровые палатки
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_09.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_09.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Турники и брусья
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_10.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_10.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Дополнительное оборудование
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_11.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_11.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Детские песочницы
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_12.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_12.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Детская мебель
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_13.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_13.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Детские домики
									</p>
								</a>
							</li>

							<li class="catalog-menu-item">
								<a href="<?=SITE_DIR?>catalog/list">
									<div class="catalog-menu-item__img">
										<picture>
											<source srcset="<?=IMGS_DIR?>cats/cat_img_14.webp" type="image/webp">
											<img src="<?=IMGS_DIR?>cats/cat_img_14.jpg">
										</picture>
									</div>
									<p class="catalog-menu-item__name">
										Шезлонги
									</p>
								</a>
							</li>
						</ul>
					</nav>
				</div>
				<div class="customer-menu-block">
					<div class="customer-menu-block__title js-customer-menu">
						<span>Покупателям</span>
						<button class="customer-menu-block__btn">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</button>
					</div>
					<div class="customer-menu-block__content">
						<ul class="customer-menu-block__list">
							<li class="customer-menu-block__item">
								<a href="<?=SITE_DIR?>company/faq/">Вопросы и ответы</a>
							</li>
							<li class="customer-menu-block__item">
								<a href="<?=SITE_DIR?>company/discounts/">Акции и скидки</a>
							</li>
							<li class="customer-menu-block__item">
								<a href="<?=SITE_DIR?>company/pay-and-delivery/">Оплата и доставка</a>
							</li>
							<li class="customer-menu-block__item">
								<a href="<?=SITE_DIR?>company/credit/">Кредит</a>
							</li>
							<li class="customer-menu-block__item">
								<a href="<?=SITE_DIR?>company/guarantee/">Гарантия</a>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="header-search">
				<div class="container">
					<p class="header-search__question">ЧТО ВЫ ИЩЕТЕ?</p>
					<form class="header-search__form" id="header-search-form">
						<div class="header-search__input-group">
							<input type="text" name="q" class="header-search__input-field">
							<span class="header-search__input-placeholder">Поиск по товарам, брендам, категориям...</span>
						</div>

						<button type="submit" name="s" class="header-search__btn">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#search"></use>
				            </svg>
						</button>
					</form>
				</div>
			</div>

			<div class="header-logo">
				<?if(getPath() == SITE_DIR):?>
				<img src="<?=IMGS_DIR?>header_logo.svg" alt="kinderstart">
				<?else:?>
				<a href="<?=SITE_DIR?>">
				<img src="<?=IMGS_DIR?>header_logo.svg" alt="kinderstart">
				</a>
				<?endif?>
			</div>

			<?/*?>
			<div class="customer-menu ext-btn-block">
				<div class="customer-menu-brief brief-ext">
					<div class="brief-ext__info">
						Покупателям
					</div>
					<button class="brief-ext__btn js-u-menu-btn">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
					</button>
				</div>
				<div class="customer-menu-ext ext-dropdown">
					<ul class="ext-dropdown__menu">
						<li class="ext-dropdown__menu-item">
							<a href="<?=SITE_DIR?>company/faq/">Вопросы и ответы</a>
						</li>
						<li class="ext-dropdown__menu-item">
							<a href="<?=SITE_DIR?>company/discounts/">Акции и скидки</a>
						</li>
						<li class="ext-dropdown__menu-item">
							<a href="<?=SITE_DIR?>company/pay-and-delivery/">Оплата и доставка</a>
						</li>
						<li class="ext-dropdown__menu-item">
							<a href="<?=SITE_DIR?>company/credit/">Кредит</a>
						</li>
						<li class="ext-dropdown__menu-item">
							<a href="<?=SITE_DIR?>company/guarantee/">Гарантия</a>
						</li>
					</ul>
				</div>
			</div>
			<?*/?>

			<div class="ext-btn-block top-lang-block" style="margin-left: 25px">
				<div class="top-lang-block__btn">
					<span class="lang-active"><?=$site_lang?></span>
					<button class="brief-ext__btn js-cont-btn">
						<i class="fa fa-angle-down" aria-hidden="true"></i>
					</button>
				</div>
				<div class="ext-dropdown top-lang-block__ext js-switch-lang">
					<?if($site_lang=='UA'):?>
					<a href="/" class="top-header-btn top-lang-btn" data-lang="ru">
						<span>RU</span>
					</a>
					<span class="top-header-btn top-lang-btn active" data-lang="ua">
						<span>UA</span>
					</span>
					<?else:?>
					<span class="top-header-btn top-lang-btn active" data-lang="ru">
						<span>RU</span>
					</span>
					<a href="/?lang=ua" class="top-header-btn top-lang-btn" data-lang="ua">
						<span>UA</span>
					</a>
					<?endif?>
				</div>
			</div>

			<div class="right-header-section">
				<button class="top-header-btn th-user js-auth-btn">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#user"></use>
		            </svg>
				</button>

				<div class="addr-n-phones--wide phones-block">
					<div class="phones-block__one-phone">
						<span class="phone-icon">
							<svg class="phone-fill">
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#phone"></use>
				            </svg>
			        	</span>
						<span class="phone-code">(044)</span> 360-64-56
					</div>
					<div class="phones-block__one-phone">
						<span class="phone-icon">
							<svg class="kievstar-fill">
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#kievstar"></use>
				            </svg>
			        	</span>
						<span class="phone-code">(067)</span> 506-42-09
					</div>
					<div class="phones-block__one-phone">
						<span class="phone-icon">
							<svg class="vodafone-fill">
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#vodafone"></use>
				            </svg>
			        	</span>
						<span class="phone-code">(050)</span> 666-02-12
					</div>
					<div class="phones-block__schedule">
						Пн - Пт: с 9 до 21
					</div>
				</div>

				<div class="ext-btn-block addr-n-phones">
					<div class="brief-ext">
						<div class="brief-ext__info">
							<p class="brief-ext__phone">
								(067) 506-42-09
							</p>
							<p class="brief-ext__schedule">
								Пн — Пт С 9 ДО 21
							</p>
						</div>

						<button class="brief-ext__btn js-cont-btn">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</button>
					</div>

					<button class="top-header-btn js-cont-btn
					top-cont-btn">
						<svg class="svg-closed">
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#phone"></use>
			            </svg>
			            <svg class="svg-opened">
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
			            </svg>
					</button>
					<div class="ext-dropdown ext-contacts-block">
						<ul class="ext-dropdown__phones">
							<li class="ext-dropdown__phone">
								<span class="phone-icon">
									<svg class="phone-fill">
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#phone"></use>
						            </svg>
					        	</span>
								<span class="phone-code">(044)</span> 360-64-56
							</li>
							<li class="ext-dropdown__phone">
								<span class="phone-icon">
									<svg class="kievstar-fill">
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#kievstar"></use>
						            </svg>
					        	</span>
								<span class="phone-code">(067)</span> 506-42-09
							</li>
							<li class="ext-dropdown__phone">
								<span class="phone-icon">
									<svg class="vodafone-fill">
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#vodafone"></use>
						            </svg>
					        	</span>
								<span class="phone-code">(050)</span> 666-02-12
							</li>
						</ul>
						<p class="ext-dropdown__mail">
							<a href="mailto:info@sportbaby.ua">info@sportbaby.ua</a>
						</p>
					</div>
				</div>
				<button class="top-header-btn js-search-btn">
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#search"></use>
		            </svg>
				</button>
				<button class="top-header-btn top-header-btn--added">
					<span class="top-header-btn__cnt">1</span>
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#heart"></use>
		            </svg>
		        </button>
				<a  href="<?=SITE_DIR?>catalog/compare/" class="top-header-btn">
					<span class="top-header-btn__cnt">1</span>
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#compare"></use>
		            </svg>
		        </a>
				<button class="top-header-btn">
					<span class="top-header-btn__cnt">1</span>
					<svg>
		                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#cart"></use>
		            </svg>
		        </button>
			</div>
		</div>
		<div class="catalog-menu-bg"></div>
<?
if(getPath() == SITE_DIR):
?>
<!-- start of slider on index page -->
		<div class="top-banner swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide top-banner-item">
					<div class="top-banner-item-bg"
					style="background-image: url(<?=IMGS_DIR?>banner_bg_01.png)">
						<div class="top-banner-item-bg__fg" style="background-image: url(<?=IMGS_DIR?>banner_bg_fg_01.png)">

						</div>
					</div>
					<div class="container">
						<div class="top-banner-item__desc">
							<h3 class="top-banner-item__header">Спортивный комплекс</h3>
							<p class="top-banner-item__txt">
								Заказывали в «Олл Корп» систему безопасности для нашего центра.
							</p>
							<div class="top-banner-item__btn">
								<a href="#" class="a-style-btn">Подробно</a>
							</div>
						</div>
					</div>
				</div>

				<div class="swiper-slide top-banner-item">
					<div class="top-banner-item-bg"
					style="background-image: url(<?=IMGS_DIR?>banner_bg_01.png)">
						<div class="top-banner-item-bg__fg" style="background-image: url(<?=IMGS_DIR?>banner_bg_fg_01.png)">

						</div>
					</div>
					<div class="container">
						<div class="top-banner-item__desc">
							<h3 class="top-banner-item__header">Slide 2</h3>
							<p class="top-banner-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit reiciendis nobis nesciunt iusto at saepe similique omnis, neque cum aliquam rem magni provident.
							</p>
							<div class="top-banner-item__btn">
								<a href="#" class="a-style-btn">Подробно</a>
							</div>
						</div>
					</div>
				</div>

				<div class="swiper-slide top-banner-item">
					<div class="container">
						<div class="top-banner-item__desc">
							<h3 class="top-banner-item__header">Slide 3</h3>
							<p class="top-banner-item__txt">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit reiciendis nobis nesciunt iusto at saepe similique omnis, neque cum aliquam rem magni provident.
							</p>
							<div class="top-banner-item__btn">
								<a href="#" class="a-style-btn">Подробно</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="swiper-pagination"></div>
			</div>
		</div>
<!-- end of slider on index page -->
<?else:?>
		<div class="pages-nav">
			<div class="container">
				<div class="breadcrumbs">
					<div class="breadcrumbs-item">
						<a href="#" class="breadcrumbs-item__name">
							Главная
						</a>
						<span class="breadcrumbs-item__separator">></span>
					</div>
					<div class="breadcrumbs-item">
						<a href="#" class="breadcrumbs-item__name">
							Каталог
						</a>
						<span class="breadcrumbs-item__separator">></span>
					</div>
					<div class="breadcrumbs-item">
						<a href="#" class="breadcrumbs-item__name">
							Спортивный уголок для дома
						</a>
						<span class="breadcrumbs-item__separator">></span>
					</div>
					<div class="breadcrumbs-item">
						<span class="breadcrumbs-item__name">
							Детские спортивные уголки
						</span>
					</div>
				</div>

				<button class="share-btn js-share-btn">
					<svg>
			            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#share"></use>
			        </svg>
				</button>

				<div class="share-block">
					<div class="share-block__wrapper">
						<a href="#" class="share-block__ilem share-block__ilem--fb">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
				            </svg>
						</a>
						<a href="#" class="share-block__ilem share-block__ilem--twt">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#twt"></use>
				            </svg>
						</a>
						<a href="#" class="share-block__ilem share-block__ilem--gplus">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#gplus"></use>
				            </svg>
						</a>
						<a href="#" class="share-block__ilem share-block__ilem--insta">
							<svg>
				                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#insta"></use>
				            </svg>
						</a>
					</div>
				</div>
			</div>
		</div>
<?endif?>
	</header>
	<main>