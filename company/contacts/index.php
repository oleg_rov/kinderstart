<?php
include '../../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>

<!--<div class="page-content">-->
	<div class="container">
		<div class="page-block">
			<div class="side-block shift-side-block">
				<button class="shift-side-block__close">
					<svg class="svg-opened">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
					</svg>
				</button>
				<div class="catalog-side-wrapper">
<?php  include '../sidebar.php'; ?>
				</div>
			</div>

			<div class="main-block">
				<div class="regular-block">
					<h1 class="regular-block__header">
						Контакты
					</h1>

					<div class="shift-menu-top">
						<button class="shift-menu-top__btn js-cats-filters">
							<svg>
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
							</svg>
						</button>
					</div>

					<div class="contacts-block">
						<div class="contacts-map">
							<div id="map">

							</div>
						</div>

						<div class="contacts-wrapper">
							<div class="contacts-requisites">
								<div class="requisites-elem">
									<h3 class="requisites-elem__title">
										Телефон:
									</h3>
									<div class="requisites-elem__content">
										<p class="requisites-elem__phone">
											<svg class="phone">
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#phone"></use>
											</svg>
											<span>(044)</span> 360-64-56
										</p>

										<p class="requisites-elem__phone">
											<svg class="kievstar">
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#kievstar"></use>
											</svg>
											<span>(067)</span> 506-42-09
										</p>

										<p class="requisites-elem__phone">
											<svg class="vodafone">
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#vodafone"></use>
											</svg>
											<span>(050)</span> 666-02-12
										</p>
									</div>
								</div>

								<div class="requisites-elem">
									<h3 class="requisites-elem__title">
										Адрес:
									</h3>
									<div class="requisites-elem__content">
										<p>ул. Каланчевская д.1</p>
									</div>
								</div>

								<div class="requisites-elem">
									<h3 class="requisites-elem__title">
										Email:
									</h3>
									<div class="requisites-elem__content">
										<p>info@sportbaby.ua</p>
									</div>
								</div>
							</div>

							<div class="contacts-feedback">
								<h2 class="contacts-feedback__title">Задать вопрос</h2>
								<form action="" method="POST" id="feedback-form">
									<div class="feedback-wrapper">
										<div class="feedback-column">
											<div class="pop-up-window__input-group">
												<input type="text" name="u-name" class="pop-up-window__input-field js-ff" required="">
												<span class="pop-up-window__input-placeholder">Имя</span>
											</div>

											<div class="pop-up-window__input-group">
												<input type="text" name="u-email" class="pop-up-window__input-field js-ff" required="">
												<span class="pop-up-window__input-placeholder">Email</span>
											</div>

											<div class="feedback-captcha">
												<div class="input-captcha pop-up-window__input-group">
													<input type="text" name="u-captcha" class="pop-up-window__input-field js-ff" required="">
													<span class="pop-up-window__input-placeholder">Код с картинки</span>
												</div>
												<img class="img-captcha" src="<?=IMGS_DIR?>captcha.jfif">
											</div>
										</div>

										<div class="feedback-column">
											<div class="pop-up-window__textarea-group">
												<textarea name="u-question" id="u-question" class="pop-up-window__textarea-field js-ff" required=""></textarea>
												<span class="pop-up-window__textarea-placeholder">Вопрос</span>
											</div>
										</div>
									</div>

									<div class="feedback-submit">
										<button class="a-style-btn js-ask-question">Отправить</button>
									</div>
								</form>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
<!--</div>-->

<script>
	/* google map */
	function initMap() {
		var uluru = {lat: 50.459120, lng: 30.510327};

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 17,
			center: uluru,
		});
		var marker = new google.maps.Marker({
			position: uluru,
			map: map,
			icon: {
				anchor: new google.maps.Point(0, 0),
				//size: new google.maps.Size(30,44),
				url: '<?=IMGS_DIR?>marker.svg'
			}
		});
	}

	document.addEventListener("DOMContentLoaded", function() {
		var script = document.createElement('script');
		script.type = 'text/javascript';
	    script.async = true;
	    script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCQ3_-a_EpvpzasPXUl8unsfmfD2BYe88U&callback=initMap";
	    document.body.appendChild(script);
	});
</script>

<?php include MAIN_TEMPLATE.'footer.php';?>