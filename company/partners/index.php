<?php
include '../../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>

<div class="container">
	<div class="page-block">

		<div class="side-block shift-side-block">
			<button class="shift-side-block__close">
				<svg class="svg-opened">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
				</svg>
			</button>
			<div class="catalog-side-wrapper">
<?php  include '../sidebar.php'; ?>
			</div>
		</div>

		<div class="main-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					Наши партнеры
				</h1>

				<div class="shift-menu-top">
					<button class="shift-menu-top__btn js-cats-filters">
						<svg>
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
						</svg>
					</button>
				</div>

				<div class="partners__items">
					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>

					<div class="partners__item">
						<div class="partners__item-img">
							<img src="<?=IMGS_DIR?>partner.png" alt="partner">
						</div>
						<p class="partners__item-txt">
							name
						</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php include MAIN_TEMPLATE.'footer.php';?>