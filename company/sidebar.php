<?
$path = getPath();
$menuItems = [
	[
		"name" => "Новости",
		"link" => SITE_DIR."news/"
	],
	[
		"name" => "Вопросы и ответы",
		"link" => SITE_DIR."company/faq/"
	],
	[
		"name" => "Акции и скидки",
		"link" => SITE_DIR."company/discounts/"
	],
	[
		"name" => "Оплата и доставка",
		"link" => SITE_DIR."company/pay-and-delivery/"
	],
	[
		"name" => "Кредит",
		"link" => SITE_DIR."company/credit/"
	],
	[
		"name" => "Гарантия",
		"link" => SITE_DIR."company/guarantee/"
	],
	[
		"name" => "Вакансии",
		"link" => SITE_DIR."company/vacancy/"
	],
	[
		"name" => "Сертификаты",
		"link" => SITE_DIR."company/certificates/"
	],
	[
		"name" => "Наши партнеры",
		"link" => SITE_DIR."company/partners/"
	],
	[
		"name" => "Отзывы",
		"link" => SITE_DIR."company/reviews/"
	],
	[
		"name" => "Награды и достижения",
		"link" => SITE_DIR."company/awards/"
	],
];

?>

<div class="pages-menu">
	<ul class="pages-menu__content">
		<?foreach($menuItems as $item):?>
		<?if(getPath() == $item["link"]):?>
		<li class="pages-menu__item active">
			<span><?=$item["name"]?></span>
		</li>
		<?else:?>
		<li class="pages-menu__item">
			<a href="<?=$item["link"]?>"><?=$item["name"]?></a>
		</li>
		<?endif?>
		<?endforeach?>
	</ul>
</div>