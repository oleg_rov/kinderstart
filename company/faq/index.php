<?php
include '../../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>

<div class="container">
	<div class="page-block">

		<div class="side-block shift-side-block">
			<button class="shift-side-block__close">
				<svg class="svg-opened">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
				</svg>
			</button>
			<div class="catalog-side-wrapper">
<?php  include '../sidebar.php'; ?>
			</div>
		</div>

		<div class="main-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					Вопросы и ответы
				</h1>

				<div class="shift-menu-top">
					<button class="shift-menu-top__btn js-cats-filters">
						<svg>
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
						</svg>
					</button>
				</div>

				<div class="faq-items">
					<div class="faq-item">
						<div class="faq-item__question">
							<span>
								Возможен ли самовывоз заказанного товара?
							</span>
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</div>
						<div class="faq-item__answer">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laboriosam distinctio, quaerat, ullam debitis omnis a provident maxime officia! Voluptate.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laboriosam distinctio, quaerat, ullam debitis omnis a provident maxime officia! Voluptate.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates consectetur deserunt atque nulla minus, illo reiciendis sequi eius suscipit pariatur quos nisi! Adipisci porro dolorum assumenda, sit modi sunt ex illo veritatis quia cum deleniti doloremque cumque veniam distinctio vel asperiores! Molestiae ipsum expedita dolores ab porro placeat quam debitis, est sequi atque. Aliquid molestias vel veritatis necessitatibus voluptatum nulla architecto, voluptates pariatur. Animi impedit, corrupti similique vitae culpa minus, in sit tempora quaerat laboriosam unde, at nulla eaque alias pariatur, officiis dolore voluptates! Reprehenderit laudantium recusandae sint nulla, suscipit, eos ut eum minus nobis veniam ipsa saepe. Enim, dicta.</p>
						</div>
					</div>

					<div class="faq-item">
						<div class="faq-item__question">
							<span>
								Как осуществляется доставка в регионы?
							</span>
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</div>
						<div class="faq-item__answer">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laboriosam distinctio, quaerat, ullam debitis omnis a provident maxime officia! Voluptate.
						</div>
					</div>

					<div class="faq-item">
						<div class="faq-item__question">
							<span>
								Как рассчитать стоимость доставки в Магадан?
							</span>
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</div>
						<div class="faq-item__answer">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet maiores, distinctio, officia maxime quis veniam id libero laboriosam necessitatibus eum? Porro veritatis ut accusamus ratione, numquam iusto repellendus id quaerat deleniti eum, sequi magni placeat assumenda voluptate. Fugiat numquam, facilis repellat tenetur illum dolores officiis doloribus aliquid ipsum! Eveniet, maiores.
						</div>
					</div>

					<div class="faq-item">
						<div class="faq-item__question">
							<span>
								Возможен ли самовывоз заказанного товара?
							</span>
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</div>
						<div class="faq-item__answer">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga rem numquam cupiditate nemo tenetur alias amet ex possimus officia accusantium error, quia assumenda deserunt porro culpa distinctio enim, sequi aperiam est vitae delectus autem illum totam? In doloribus, maxime iste totam quidem assumenda numquam sequi officia eligendi, necessitatibus labore, perspiciatis aspernatur illum repudiandae facere rem distinctio aliquid a reprehenderit? Neque.
						</div>
					</div>

					<div class="faq-item">
						<div class="faq-item__question">
							<span>
								Как осуществляется доставка в регионы?
							</span>
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</div>
						<div class="faq-item__answer">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laboriosam distinctio, quaerat, ullam debitis omnis a provident maxime officia! Voluptate.
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laboriosam distinctio, quaerat, ullam debitis omnis a provident maxime officia! Voluptate.
						</div>
					</div>
				</div>

				<div class="question-yet">
					<div class="question-yet__img">
						<svg>
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#notebook"></use>
						</svg>
					</div>
					<div class="question-yet__txt">
						<p class="first-sentence">Не нашли ответа на свой вопрос?</p>
						<p class="second-sentence">Свяжитесь с нами, и мы предоставим необходимую информацию.</p>
					</div>
					<div class="question-yet__btn">
						<button class="a-style-btn js-ask-question">Задать вопрос</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="pop-up ask-question" id="ask-question">
	<div class="pop-up-bg"></div>
	<div class="pop-up-window ask-question-window">
		<button class="pop-up-window__close" id="ask-question-close">
			<svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
            </svg>
		</button>
		<div class="pop-up-window__title">Задать вопрос</div>
		<form id="ask-question-form">
			<div class="pop-up-window__input-group">
				<input type="text" name="u-name" class="pop-up-window__input-field" required>
				<span class="pop-up-window__input-placeholder">Имя</span>
			</div>
			<div class="pop-up-window__input-group">
				<input type="text" name="u-phone" class="pop-up-window__input-field" required>
				<span class="pop-up-window__input-placeholder">Телефон</span>
			</div>
			<div class="pop-up-window__textarea-group">
				<textarea name="u-question" id="u-question" class="pop-up-window__textarea-field" required></textarea>
				<span class="pop-up-window__textarea-placeholder">Вопрос</span>
			</div>
			<div class="pop-up-window__btn-group">
				<div class="ask-question__btn">
					<button class="a-style-btn">Отправить</button>
				</div>
			</div>
		</form>
	</div>
</div>

<?php include MAIN_TEMPLATE.'footer.php';?>