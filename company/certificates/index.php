<?php
include '../../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>


<div class="container">
	<div class="page-block">

		<div class="side-block shift-side-block">
			<button class="shift-side-block__close">
				<svg class="svg-opened">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
				</svg>
			</button>
			<div class="catalog-side-wrapper">
<?php  include '../sidebar.php'; ?>
			</div>
		</div>

		<div class="main-block">
			<div class="regular-block">
				<h1 class="regular-block__header">
					Сертификаты
				</h1>

				<div class="shift-menu-top">
					<button class="shift-menu-top__btn js-cats-filters">
						<svg>
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
						</svg>
					</button>
				</div>

				<div class="certif__items">
					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_01.jpg"
								data-fancybox="certif-gallery"
								data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
								data-srcset="<?=IMGS_DIR?>certificates/c_01.webp type='image/webp'"
								>
							<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_01_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_01_s.jpg">
								</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_02.jpg"
							data-fancybox="certif-gallery"
							data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #2"
							data-srcset="<?=IMGS_DIR?>certificates/c_02.webp type='image/webp'"
							>
							<div class="certif__item-img">
									<picture>
										<source srcset="<?=IMGS_DIR?>certificates/c_02_s.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>certificates/c_02_s.jpg">
									</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_03.jpg"
							data-fancybox="certif-gallery"
							data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #3"
							data-srcset="<?=IMGS_DIR?>certificates/c_03.webp type='image/webp'"
							>
							<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_03_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_03_s.jpg">
								</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_01.jpg"
								data-fancybox="certif-gallery"
								data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
								data-srcset="<?=IMGS_DIR?>certificates/c_01.webp type='image/webp'"
								>
							<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_01_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_01_s.jpg">
								</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_02.jpg"
							data-fancybox="certif-gallery"
							data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #2"
							data-srcset="<?=IMGS_DIR?>certificates/c_02.webp type='image/webp'"
							>
							<div class="certif__item-img">
									<picture>
										<source srcset="<?=IMGS_DIR?>certificates/c_02_s.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>certificates/c_02_s.jpg">
									</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_03.jpg"
							data-fancybox="certif-gallery"
							data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #3"
							data-srcset="<?=IMGS_DIR?>certificates/c_03.webp type='image/webp'"
							>
							<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_03_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_03_s.jpg">
								</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_01.jpg"
								data-fancybox="certif-gallery"
								data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #1"
								data-srcset="<?=IMGS_DIR?>certificates/c_01.webp type='image/webp'"
								>
							<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_01_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_01_s.jpg">
								</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_02.jpg"
							data-fancybox="certif-gallery"
							data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #2"
							data-srcset="<?=IMGS_DIR?>certificates/c_02.webp type='image/webp'"
							>
							<div class="certif__item-img">
									<picture>
										<source srcset="<?=IMGS_DIR?>certificates/c_02_s.webp" type="image/webp">
										<img src="<?=IMGS_DIR?>certificates/c_02_s.jpg">
									</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_03.jpg"
							data-fancybox="certif-gallery"
							data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #3"
							data-srcset="<?=IMGS_DIR?>certificates/c_03.webp type='image/webp'"
							>
							<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_03_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_03_s.jpg">
								</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_03.jpg"
							data-fancybox="certif-gallery"
							data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #3"
							data-srcset="<?=IMGS_DIR?>certificates/c_03.webp type='image/webp'"
							>
							<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_03_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_03_s.jpg">
								</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>

					<div class="certif__item">
						<a href="<?=IMGS_DIR?>certificates/c_03.jpg"
							data-fancybox="certif-gallery"
							data-caption="Сертифицированный производитель спортивных детских комплексов в Украине #3"
							data-srcset="<?=IMGS_DIR?>certificates/c_03.webp type='image/webp'"
							>
							<div class="certif__item-img">
								<picture>
									<source srcset="<?=IMGS_DIR?>certificates/c_03_s.webp" type="image/webp">
									<img src="<?=IMGS_DIR?>certificates/c_03_s.jpg">
								</picture>
							</div>
						</a>
						<div class="certif__item-txt">
							Сертифицированный производитель спортивных детских комплексов в Украине
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php include MAIN_TEMPLATE.'footer.php';?>