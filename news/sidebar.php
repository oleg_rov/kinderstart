<?
$path = getPath();
$menuItems = [
	[
		"name" => "Новости",
		"link" => SITE_DIR."news/"
	],
	[
		"name" => "Вопросы и ответы",
		"link" => SITE_DIR."company/faq/"
	],
	[
		"name" => "Акции и скидки",
		"link" => SITE_DIR."company/discounts/"
	],
	[
		"name" => "Оплата и доставка",
		"link" => SITE_DIR."company/pay-and-delivery/"
	],
	[
		"name" => "Кредит",
		"link" => SITE_DIR."company/credit/"
	],
	[
		"name" => "Гарантия",
		"link" => SITE_DIR."company/guarantee/"
	],
];

?>

<div class="pages-menu">
	<ul class="pages-menu__content">
		<?foreach($menuItems as $item):?>
		<?if(getPath() == $item["link"]):?>
		<li class="pages-menu__item active">
			<span><?=$item["name"]?></span>
		</li>
		<?else:?>
		<li class="pages-menu__item">
			<a href="<?=$item["link"]?>"><?=$item["name"]?></a>
		</li>
		<?endif?>
		<?endforeach?>
	</ul>
</div>