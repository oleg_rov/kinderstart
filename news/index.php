<?php
include '../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>

<div class="container">
	<div class="page-block">
		<div class="side-block shift-side-block">
			<button class="shift-side-block__close">
				<svg class="svg-opened">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
				</svg>
			</button>

			<div class="catalog-side-wrapper">
<?php  include 'sidebar.php'; ?>
			</div>
		</div>

	<div class="main-block">
		<div class="regular-block">
			<h1 class="regular-block__header">
				Новости
			</h1>

			<div class="shift-menu-top">
				<button class="shift-menu-top__btn js-cats-filters">
					<svg>
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
					</svg>
				</button>
			</div>

			<div class="news-list-items">
				<div class="news-list-elem">
					<div class="news-list-elem__img">
						<a href="<?=SITE_DIR?>news/element">
							<picture>
								<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>news/news-img.jpg">
							</picture>
						</a>
					</div>
					<div class="news-list-elem__content">
						<p class="news-list-elem__date">01.Ноя.2018</p>
						<h2 class="news-list-elem__title">
							<a href="<?=SITE_DIR?>news/element">
								Кампания SportBaby поздравляет Вас с новым 2014 годом
							</a>
						</h2>
						<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
						<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
					</div>
				</div>

				<div class="news-list-elem">
					<div class="news-list-elem__img">
						<a href="<?=SITE_DIR?>news/element">
							<picture>
								<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>news/news-img.jpg">
							</picture>
						</a>
					</div>
					<div class="news-list-elem__content">
						<p class="news-list-elem__date">01.Ноя.2018</p>
						<h2 class="news-list-elem__title">
							<a href="<?=SITE_DIR?>news/element">
								Кампания SportBaby поздравляет Вас с новым 2014 годом
							</a>
						</h2>
						<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
						<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
					</div>
				</div>

				<div class="news-list-elem">
					<div class="news-list-elem__img">
						<a href="<?=SITE_DIR?>news/element">
							<picture>
								<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>news/news-img.jpg">
							</picture>
						</a>
					</div>
					<div class="news-list-elem__content">
						<p class="news-list-elem__date">01.Ноя.2018</p>
						<h2 class="news-list-elem__title">
							<a href="<?=SITE_DIR?>news/element">
								Кампания SportBaby поздравляет Вас с новым 2014 годом
							</a>
						</h2>
						<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
						<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
					</div>
				</div>

				<div class="news-list-elem">
					<div class="news-list-elem__img">
						<a href="<?=SITE_DIR?>news/element">
							<picture>
								<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>news/news-img.jpg">
							</picture>
						</a>
					</div>
					<div class="news-list-elem__content">
						<p class="news-list-elem__date">01.Ноя.2018</p>
						<h2 class="news-list-elem__title">
							<a href="<?=SITE_DIR?>news/element">
								Кампания SportBaby поздравляет Вас с новым 2014 годом
							</a>
						</h2>
						<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
						<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
					</div>
				</div>

				<div class="news-list-elem">
					<div class="news-list-elem__img">
						<a href="<?=SITE_DIR?>news/element">
							<picture>
								<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>news/news-img.jpg">
							</picture>
						</a>
					</div>
					<div class="news-list-elem__content">
						<p class="news-list-elem__date">01.Ноя.2018</p>
						<h2 class="news-list-elem__title">
							<a href="<?=SITE_DIR?>news/element">
								Кампания SportBaby поздравляет Вас с новым 2014 годом
							</a>
						</h2>
						<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
						<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
					</div>
				</div>

				<div class="news-list-elem">
					<div class="news-list-elem__img">
						<a href="<?=SITE_DIR?>news/element">
							<picture>
								<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>news/news-img.jpg">
							</picture>
						</a>
					</div>
					<div class="news-list-elem__content">
						<p class="news-list-elem__date">01.Ноя.2018</p>
						<h2 class="news-list-elem__title">
							<a href="<?=SITE_DIR?>news/element">
								Кампания SportBaby поздравляет Вас с новым 2014 годом
							</a>
						</h2>
						<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
						<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
					</div>
				</div>

				<div class="news-list-elem">
					<div class="news-list-elem__img">
						<a href="<?=SITE_DIR?>news/element">
							<picture>
								<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>news/news-img.jpg">
							</picture>
						</a>
					</div>
					<div class="news-list-elem__content">
						<p class="news-list-elem__date">01.Ноя.2018</p>
						<h2 class="news-list-elem__title">
							<a href="<?=SITE_DIR?>news/element">
								Кампания SportBaby поздравляет Вас с новым 2014 годом
							</a>
						</h2>
						<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
						<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
					</div>
				</div>

				<div class="news-list-elem">
					<div class="news-list-elem__img">
						<a href="<?=SITE_DIR?>news/element">
							<picture>
								<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>news/news-img.jpg">
							</picture>
						</a>
					</div>
					<div class="news-list-elem__content">
						<p class="news-list-elem__date">01.Ноя.2018</p>
						<h2 class="news-list-elem__title">
							<a href="<?=SITE_DIR?>news/element">
								Кампания SportBaby поздравляет Вас с новым 2014 годом
							</a>
						</h2>
						<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
						<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
					</div>
				</div>

				<div class="news-list-elem">
					<div class="news-list-elem__img">
						<a href="<?=SITE_DIR?>news/element">
							<picture>
								<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>news/news-img.jpg">
							</picture>
						</a>
					</div>
					<div class="news-list-elem__content">
						<p class="news-list-elem__date">01.Ноя.2018</p>
						<h2 class="news-list-elem__title">
							<a href="<?=SITE_DIR?>news/element">
								Кампания SportBaby поздравляет Вас с новым 2014 годом
							</a>
						</h2>
						<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
						<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
					</div>
				</div>

			</div>

			<div class="news-pagination pagination-section">

				<ul class="pagination-section__items">
					<li class="active">
						<span class="nav-current-page">1</span>
					</li>
					<li>
						<a href="#?PAGEN_2=2">2</a>
					</li>
					<li>
						<a href="#?PAGEN_2=3">3</a>
					</li>
					<li class="points"><span>...</span></li>
					<li>
						<a href="#?PAGEN_2=24">26</a>
					</li>
					<li>
						<a href="#?PAGEN_2=25">25</a>
					</li>
					<li>
						<a href="#?PAGEN_2=26">26</a>
					</li>
				</ul>
				<div class="pagination-section__nums">
					1 - 21 из 74
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

<?php include MAIN_TEMPLATE.'footer.php';?>