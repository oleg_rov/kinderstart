<?php
include '../../paths.php';
?>

<?php include MAIN_TEMPLATE.'header.php';?>

<!--<div class="page-content">-->
	<div class="container">
		<div class="page-block">

			<div class="side-block shift-side-block">
				<button class="shift-side-block__close">
					<svg class="svg-opened">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
					</svg>
				</button>

				<div class="catalog-side-wrapper">
<?php  include '../sidebar.php'; ?>
				</div>
			</div>

			<div class="main-block">
				<div class="regular-block">

					<h1 class="regular-block__header">
						Кампания SportBaby поздравляет Вас с новым 2014 годом
					</h1>

					<div class="shift-menu-top">
						<button class="shift-menu-top__btn js-cats-filters">
							<svg>
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#bars"></use>
							</svg>
						</button>
					</div>

					<div class="news-item">
						<div class="news-item__top">
							<div class="news-item__date"><span>01.Ноя.2018</span></div>
							<button class="news-item__btn">
								<span class="btn-icon rss-icon">
									<svg>
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#rss"></use>
									</svg>
								</span>
								<span class="btn-txt">RSS</span>
							</button>
							<button class="news-item__btn">
								<span class="btn-icon print-icon">
									<svg>
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#print"></use>
									</svg>
								</span>
								<span class="btn-txt">Распечатать</span>
							</button>
						</div>

						<div class="news-item__img">
							<picture>
								<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
								<img src="<?=IMGS_DIR?>news/news-img.jpg">
							</picture>
						</div>

						<div class="news-item__content">
							<p>Дети очень подвижны и любознательны, поэтому особенно в раннем возрасте они проявляют любопытство, исследуя этот мир. Конечно же, родителей заставляет волноваться этот факт и они стараются опекать и обеспечить максимальную безопасность малышу, чтобы предотвратить негативные последствия от таких любознательных игр.</p>

							<p>Если малыша не увлечь полезной и развивающей игрушкой, он сможет самостоятельно найти себе игру или предмет, который может его очень заинтересовать, но при этом, он будет нести возможную опасность для здоровья малыша. Именно поэтому свободное время и игры ребенка необходимо тщательно спланировать и присматривать за ним, а также постараться обеспечить детям комфортную, удобную, интересную и безопасную игровую зону. Именно для этого и созданы детские игровые комплексы, которые предлагает наш интернет-магазин.</p>

							<p>Детские игровые комплексы представляют собой прочную и безопасную надежную конструкцию, которую можно установить на открытом воздухе, на дачном участке или во дворе частного или многоквартирного дома, а также на территории детских садов. Развлекательный спортивный комплекс для малышей может включать в себя множество разнообразных деталей. В зависимости от модели он может включать в себя веселые качели, на которых так любят раскачиваться малыши, некоторые конструкции детских игровых комплексов могут содержать одновременно несколько качелей, что очень удобно, если у Вас двое и больше детей захотят покататься и им не придется ждать своей очереди.</p>

							<p>Также, в комплектацию могут входить и детские песочницы, которые смогут развлекать малышей помладше, в то время как старшие детки смогут заниматься спортивными играми на детских тренажерах. Среди прочего детские игровые комплексы из натурального экологически чистого дерева могут быть дополнены альпинисткой горкой для скалолазания, гладиаторской сеткой и подвесными элементами, такими как веревка-канат, веревочная лесенка, гимнастические кольца.</p>

							<p>Также очень многие модели детских игровых комплексов от отечественного производителя торговой марки SportBaby оснащены самым любимым детским аттракционом – длинной горкой для веселых спусков. Прочность и надежность горок, которые входят в комплектацию детских комплексов, гарантирована производителем. Многие конструкции дополнены крышей с навесным тентом, который позволит создать тенек в жаркое время года и защитить малыша от воздействия прямых солнечных лучей.</p>

							<p>Детские комплексы, которые призваны развивать способности ребенка и развлекать его, изготовлены из натурального материала – древесины сосны, которая обработана антисептиком. Конструкции не содержат острых углов, заусениц в дереве и других вещей, которые могли бы представлять опасность для Вашего малыша. Покупая игровой комплекс от проверенного производителя с многолетним опытом, Вы получаете также и гарантию, которая предоставляется на двенадцать месяцев.</p>
						</div>

						<div class="news-item__author">
							Автор: «SportBaby»
						</div>

						<div class="news-item__return">
							<a href="<?=SITE_DIR?>news/">
								<span><i class="fa fa-angle-left" aria-hidden="true"></i>Возврат к списку</span>
							</a>
						</div>
					</div>

					<div class="news-item-list">
						<div class="section-header">
							<h2 class="news-item__list-title">
								Новости
							</h2>
							<div class="swiper-buttons">
			    				<div class="prev-news prev-slide">
			    					<i class="fa fa-angle-left" aria-hidden="true"></i>
			    				</div>
								<div class="next-news next-slide">
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
							</div>
						</div>

						<div class="news-block news-list-items swiper-container">
							<div class="swiper-wrapper">
								<div class="news-list-elem swiper-slide">
									<div class="news-list-elem__img">
										<a href="<?=SITE_DIR?>news/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>news/news-img.jpg">
											</picture>
										</a>
									</div>
									<div class="news-list-elem__content">
										<p class="news-list-elem__date">01.Ноя.2018</p>
										<h2 class="news-list-elem__title">
											<a href="<?=SITE_DIR?>news/element">
												Кампания SportBaby поздравляет Вас с новым 2014 годом
											</a>
										</h2>
										<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
										<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
									</div>
								</div>

								<div class="news-list-elem swiper-slide">
									<div class="news-list-elem__img">
										<a href="<?=SITE_DIR?>news/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>news/news-img.jpg">
											</picture>
										</a>
									</div>
									<div class="news-list-elem__content">
										<p class="news-list-elem__date">01.Ноя.2018</p>
										<h2 class="news-list-elem__title">
											<a href="<?=SITE_DIR?>news/element">
												Кампания SportBaby поздравляет Вас с новым 2014 годом
											</a>
										</h2>
										<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
										<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
									</div>
								</div>

								<div class="news-list-elem swiper-slide">
									<div class="news-list-elem__img">
										<a href="<?=SITE_DIR?>news/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>news/news-img.jpg">
											</picture>
										</a>
									</div>
									<div class="news-list-elem__content">
										<p class="news-list-elem__date">01.Ноя.2018</p>
										<h2 class="news-list-elem__title">
											<a href="<?=SITE_DIR?>news/element">
												Кампания SportBaby поздравляет Вас с новым 2014 годом
											</a>
										</h2>
										<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
										<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
									</div>
								</div>

								<div class="news-list-elem swiper-slide">
									<div class="news-list-elem__img">
										<a href="<?=SITE_DIR?>news/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>news/news-img.jpg">
											</picture>
										</a>
									</div>
									<div class="news-list-elem__content">
										<p class="news-list-elem__date">01.Ноя.2018</p>
										<h2 class="news-list-elem__title">
											<a href="<?=SITE_DIR?>news/element">
												Кампания SportBaby поздравляет Вас с новым 2014 годом
											</a>
										</h2>
										<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
										<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
									</div>
								</div>

								<div class="news-list-elem swiper-slide">
									<div class="news-list-elem__img">
										<a href="<?=SITE_DIR?>news/element">
											<picture>
												<source srcset="<?=IMGS_DIR?>news/news-img.webp" type="image/webp">
												<img src="<?=IMGS_DIR?>news/news-img.jpg">
											</picture>
										</a>
									</div>
									<div class="news-list-elem__content">
										<p class="news-list-elem__date">01.Ноя.2018</p>
										<h2 class="news-list-elem__title">
											<a href="<?=SITE_DIR?>news/element">
												Кампания SportBaby поздравляет Вас с новым 2014 годом
											</a>
										</h2>
										<p class="news-list-elem__txt">Кампания SportBaby поздравляет Вас с новым 2014 годом и дарит  вам возможность получить скидку</p>
										<a href="<?=SITE_DIR?>news/element" class="news-list-elem__btn a-style-btn">Подробнее</a>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
<!--</div>-->

<?php include MAIN_TEMPLATE.'footer.php';?>